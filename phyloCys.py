#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  phyloCys.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>, <daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from ete2 import Tree
from sources.mwmatching import maxWeightMatching
verbosity = 0

def getCysPattern(nodeName, numCys, fakerootName):	
	if nodeName == fakerootName:
		return "0"*numCys
	name = nodeName[-numCys:]
	assert len(name) == numCys
	assert len(set(name)) <= 2	
	if name.count("1") % 2 != 0:
		print "WARNING "+nodeName
	return nodeName[-numCys:]
	
def buildName(node, numCys, fakerootName):	# recursive function that assigns the names to all the nodes in the tree
	names = []
	for i in node.children:
		#print i.name		
		if i.is_leaf():
			assert i.name != "NoName"
			names.append(getCysPattern(i.name, numCys, fakerootName))
		else:
			if i.name == "NoName":
				buildName(i, numCys, fakerootName)
			assert i.name != "NoName" and len(i.name) == numCys and len(set(i.name)) <= 2
			names.append(i.name)
	assert len(names) == 2
	finalName = ""
	i = 0
	#ORs the names
	while i < numCys:
		if names[0][i] == "1" or names[1][i] == "1":
			finalName += "1"
		else:
			finalName += "0"
		i+=1
	assert len(finalName) == numCys
	node.name = finalName	
	

def smartDiff(s1, s2): #function that applies the rules described in the paper
	if verbosity >= 4:
		print s1, s2
	if s1 == s2:
		if verbosity >= 4:
			print "No diff"		
		return {}
	assert len(s1) == len(s2)
	p1 = s1.count("1") % 2 == 0
	p2 = s2.count("1") % 2 == 0
	changeList = []
	s1List = []
	couples = {}
	"""
	Here the 4 possible parity changes are analyzed: Even -> Even, Even -> Odd, Odd -> Even and Odd -> Odd
	"""
	if p1 == p2: #  Even -> Even and Odd -> Odd
		changeList = []
		i = 0
		while i < len(s1):
			assert not (s1[i] == "1" and s2[i] == "0")
			if s1[i] == "1":
				s1List.append(i+1)
			if s1[i] == "0" and s2[i] == "1":
				changeList.append(i+1)
			i+=1
		assert len(changeList) % 2 == 0
		numChg = len(changeList)
		while len(changeList) > 0:
			c = changeList.pop(0)
			for i in changeList:    #these 3 rows represent RULE 1
				tmp = tuple(sorted([i,c]))
				couples[tmp] = couples.get(tmp, 0) + 1.0/float(((numChg-1)*numChg)/2.0)			
	''' #uncomment this to use more complex rules
	elif p1 == True and p2 == False: #  Even -> Odd
		changeList = []
		i = 0
		s1List = []
		while i < len(s1):
			assert not (s1[i] == "1" and s2[i] == "0")
			if s1[i] == "1":
				s1List.append(i+1)
			if s1[i] == "0" and s2[i] == "1":
				changeList.append(i+1)
			i+=1
		assert len(changeList) % 2 == 1		
		numChg = len(changeList)			
		while len(changeList) > 0:
			c = changeList.pop(0)			
			for i in changeList:  
				tmp = tuple(sorted([i,c]))
				couples[tmp] = couples.get(tmp, 0) + 1.0/float(((numChg-1)*numChg)/2.0)
			for i in s1List:      #these 3 rows represent 
				tmp = tuple(sorted([i,c]))
				couples[tmp] = couples.get(tmp, 0) - 1.0/float(len(s1List))
		
	elif p1 == False and p2 == True: # Odd -> Even 
		changeList = []
		i = 0
		s1List = []
		while i < len(s1):
			assert not (s1[i] == "1" and s2[i] == "0")
			if s1[i] == "1":
				s1List.append(i+1)
			if s1[i] == "0" and s2[i] == "1":
				changeList.append(i+1)
			i+=1
		assert len(changeList) % 2 == 1
		numChg = len(changeList)			
		while len(changeList) > 0:
			c = changeList.pop(0)
			for i in changeList:   
				tmp = tuple(sorted([i,c]))
				couples[tmp] = couples.get(tmp, 0) + 1.0/float(((numChg-1)*numChg)/2.0)
			for i in s1List:      #these 3 rows represent 
				tmp = tuple(sorted([i,c]))
				couples[tmp] = couples.get(tmp, 0) - 1.0/float(len(s1List))
	'''
	return couples
	
		
def preProcessing(path): #apply some processing to each extracted path
	np = sorted(list(set(path))) #remove duplicate
	i = 0
	flist= []
	
	while True:			
		if len(np) > i+1:
			p1 = np[i].count("1") % 2 == 0
			p2 = np[i+1].count("1") % 2 == 0
			if p1 == p2:
				flist.append(np[i])
				flist.append(np[i+1])
				i = i+1
			else:	
				if len(np) > i+2:
					p3 = np[i+2].count("1") % 2 == 0
					if p3 == p1:
						flist.append(np[i])
						flist.append(np[i+2])
						i = i+2
					else:
						flist.append(np[i+1])
						i = i+1
				else:
					break
		else:
			break	
	return sorted(list(set(flist)))
	

def phyloCys(phylTree, targetCys):
	#retrieve fakeroot for this tree
	
	fastaName = phylTree[:-3]
	ifp = open(fastaName,"r")
	l = ifp.readlines()
	#fakerootName = l[-2][1:].strip()	
	ifp.close()	
	
	try:
		t = Tree(phylTree)
		#t.describe()
		#t.show()		
		#print fakerootName
		#fakeroot = t.get_leaves_by_name(fakerootName)	
		fakeroot = t.get_leaves_by_name("FAKEROOT")
		#print fakeroot	
		fakerootName = "FAKEROOT"			
		assert len(fakeroot) == 1
		#print t.get_farthest_leaf()
		#t.set_outgroup(t.get_farthest_leaf()[0])
		t.set_outgroup(fakeroot[0])		
	except: #problem reading the tree
		print "\n"+phylTree+": NEWICK malformed, missing or empty! RANDOMGUESSING connectivity!!!"
		return sorted(targetCys)
	
	numCys = len(targetCys)
	t.name = "1" * numCys		
	leaves = t.get_leaves()	
	#populate the tree with cysteine pattern names	
	scores = {}
	count = 1
	for i in leaves:
		if verbosity > 3:
			print "\rProcessing sequence %d of %d (%3.2f%%)" % (count, len(leaves), (count/float(len(leaves)))*100),
		path = [len(targetCys)*"0", getCysPattern(i.name, numCys, fakerootName)]
		#path = [getCysPattern(i.name, numCys)]
		node = i.up
		while node.up != None:			
			if node.name == "NoName":
				buildName(node, numCys, fakerootName)
			assert node.name != "NoName"
			path.append(node.name)
			node = node.up		
		count += 1		
		path.append(t.name) 
		if verbosity > 3:		
			print	
		#for each sequence (leaf) retrieve the path to the root and infer connectivity	
		j = 1		
		while j < len(path):				
			
			couples = smartDiff(path[j-1], path[j])
			if verbosity >= 4:
				print couples
			#print diff
			for d in couples.items():
				scores[d[0]] = scores.get(d[0], 0.0) + 	d[1]			
			j+=1
	if verbosity >= 4:
		print scores
			
	ordList = sorted(scores.values())
	#post-processing
	eList = []
	for e in scores.items():
		eList.append((e[0][0],e[0][1],ordList.index(e[1])+1))
	if verbosity >= 3: 
		print "ELIST: ", eList
	
	#call the third-party python implementation of the Edmonds-Gabow algorithm
	matches = maxWeightMatching(eList, maxcardinality=True)[1:] 
	if verbosity >= 4: 
		print "matches: " , matches
	if len(matches) < len(targetCys):
		if verbosity >= 1:
			print "   ***   WARNING: unsufficient number of edges  ***   "		
		while len(matches) < len(targetCys):
			matches.append(-1)	
	if verbosity >= 3:
		print "Matches: ", matches
	assert len(targetCys) >= len(matches)
	edges = set()
	missing = []
	i = 0	
	while i < len(matches): #extract cys coupling
		if matches[i] == -1:
			missing.append(i)  #list of missing predictions
		else:
			edges.add(tuple(sorted([targetCys[i],targetCys[matches[i]-1]])))
		i +=1	
	if len(missing) != 0: 	#adds missing bonds by wildguessing them. some times they are "necessary" and everything's OK
		if verbosity >= 1:
			print " * Missing edges added!"
		assert len(missing) % 2 == 0
		while len(missing) > 0:
			edges.add(tuple(sorted([targetCys[missing.pop(0)],targetCys[missing.pop(0)]])))		
				
	edges = sorted(list(edges))	
	if verbosity >= 1:
		print "Edges: ", edges
	return edges	

	
	

def main():		
	print phyloCys("fakerootTrees/SStrees3iterE-5/A2AP35_even_.ph", sorted([1, 27, 43, 61])) #(1, 43) (27, 61)
	return 0

if __name__ == '__main__':
	main()

