#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
verbosity = 0

def positiveDoubleHammingDistance(s1, s2):
	#calculates a positive double sort-of Hamming distance between two strings
	#it defines all the s2 with two cysteines more with respect to s1
	h = 0
	i = 0
	assert len(s1) == len(s2)
	while i < len(s1):
		if s1[i] == 1 and s2[i] == 0: #inverse
			h+=1
		elif s1[i] == 0 and s2[i] == 1:
			return -1
		i +=1
	return h #positive double hamming distance

def writeFasta(msa, fileName):
	ofp = open(fileName,"w")
	for i in msa:
		ofp.write(">seq|"+str(count)+"|\n")
		ofp.write(i+"\n")
	ofp.close()

def readMSA(path):
	ifp = open(path, "rb")
	msa = ifp.readlines()
	#msa.pop(0) #drop the original seq?
	if verbosity >= 1:
		print "Read %d sequences." % len(msa)
	i = 0
	while i < len(msa):
		msa[i] = msa[i].strip()
		i += 1	
	return msa

def clusterize(MSAfile, targetCys):
	msa = readMSA(MSAfile)
	assert len(msa) >= 1 #ERROR: not enough sequences
	if verbosity >= 2:
		print "Target Cysteines: " ,targetCys
	clusters = {}
	for s in msa:
		name = getClusterName(s, targetCys)
		if not clusters.has_key(name):
			clusters[name] = set()
		clusters[name].add(s)	
	if verbosity >= 1:
		print "Number of sequences for each cluster:"
	totCount = 0
	for i in clusters.items():
		totCount += len(i[1])
		if verbosity >= 1:
			print "%s -> %d" % (i[0],len(i[1]))
	assert totCount == len(msa)
	return clusters #clusters = {name:set(seq1,seq2,...)}
	
def getClusterName(seq, targetCys):
	cluster = []
	for i in targetCys:
		if "C" in seq[i]: #allow some error
			cluster.append(1)
		else:
			cluster.append(0)
	assert len(targetCys) == len(cluster)
	return tuple(cluster)
	
def getClusterNameString(seq, targetCys):
	cluster = ""
	for i in targetCys:	
			
		if "C" in seq[i]: #allow some error
			cluster+="1"
		else:
			cluster+="0"
	assert len(targetCys) == len(cluster)
	return cluster

def getSeqIDsimilarity(s1,s2): #change it with your favourite similarity measure!
	score = 0
	i = 0 
	while i < min(len(s1), len(s2)):
		if s1[i] == s2[i]:
			score += 1
		i += 1
	#return score/float(max(len(s1),len(s2))) #if you want to consider also coverage?
	return score/float(len(s1))	
	#return score/float(min(len(s1),len(s2)-s2.count("-")))
	
def calculateSingleLinkageClusteringDistance(seq, adList, clusters): #calculates to which cluster seq is closer
	bestDist = -1 # Seq ID is strictly > 0, so always better than this
	bestClust = None
	assert len(adList) > 0
	if len(adList) == 1: #if the egde is obligatory, save computational time
		return list(adList)[0]
	for cl in adList:
		bestDistClust = 0
		for s in clusters[cl]:
			currDist = getSeqIDsimilarity(seq, s)
			bestDistClust = max(currDist, bestDistClust)
		if bestDistClust > bestDist:
			bestDist = bestDistClust
			bestClust = cl
	#the best cluster is the one that has the closest sequence (single linkage clustering)
	return bestClust

def clusterName2Edges(e):
	s = []
	assert len(e[0]) == len(e[1])
	i = 0
	while i < len(e[0]):
		if e[0][i] == 1 and e[1][i] == 0: #inverse
			s.append(i+1)
		i += 1
	assert len(s) == 2
	return tuple(s)

def main():	
	return 0

if __name__ == '__main__':
	main()

