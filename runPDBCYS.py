#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  phWrapper.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from wrapperSrc.wrapperUtils import *
from phyloCys import *
import sys, marshal, os
verbosity = 0

"""
This script iteratively runs and calculates predictions of phyloCys over PDBCYS dataset.


USAGE: python phWrapper.py database_path numBondsChosen TREES_dir_path [DUMP]
	database_path = path to the dataset containing cysteine annotations in the PDBCYS-like format
	numBondsChosen = numbers of bonds for which the predictions are needed, separed by ':'s . e.g.: 2:3:4:5
	TREES_dir_path = directory containing the NEWICK-formatted trees built by buildClustalWTrees.py
	[DUMP] = optional; write 'dump' to store the serialized predictions in python marshal form.
"""
def main():
	if (len(sys.argv) != 4 and len(sys.argv) != 5 )or "-h" in sys.argv[1]:
		print "\nUSAGE: python runPDBCYS.py database_path numBondsChosen TREES_dir_path [DUMP]"
		print "\nEXAMPLE: python runPDBCYS.py databases/PDBCYS 2:3:4:5 fakerootTrees/SStrees3iterE-2\n"
		exit(0)
	dataBase = readTrainFile(sys.argv[1])
	bonds =sys.argv[2]
	bondList = []
	DUMP = False
	if len(sys.argv) == 5 and sys.argv[4].lower() == "dump":
		DUMP = True
		os.system("mkdir -p PREDICTIONS")
	for i in bonds.split(":"):
		bondList.append(int(i))
	print "Calculating %s bonds!" % (bondList)
	TARGET_TREE_DIR=sys.argv[3]
	#TARGET_TREE_NAME=sys.argv[4]
		
	for numBonds in bondList:
		predictions = {}
		#########PREDICTION PERFORMANCES EVALUATION PARAMETERS
		Qprot = 0
		numProts = 0
		Rb = 0
		edgeTot = 0
		############
		
		for target in dataBase.items(): #iterates among all the proteins with N bonds
			targetCys = getBonded(target[1][2])		
			assert (len(targetCys) % 2) == 0 #just in case
			
			if len(targetCys) == numBonds*2 :
				numProts += 1
				assert not predictions.has_key(target[0])
				if verbosity >= 1:
					print "\nPredicting protein %s" % target[0]
				#predictedEdges = sephiroot2bootstrap(TARGET_TREE_DIR+"/"+target[0]+"_pari_.ph", targetCys, 30) #calculate predictions
				predictedEdges = phyloCys(TARGET_TREE_DIR+"/"+target[0]+"_even_.ph", targetCys)
				predictions[target[0]] = predictedEdges
				groundTruth = extractCouplingFromMeta(target[1][2])
				########## assessing results			
				if verbosity >= 1:
					print "Prediction: ", predictedEdges
					print "real      : ", groundTruth
				if predictedEdges == groundTruth:
					if verbosity > 0:
						print "CORRECT\n"
					Qprot += 1
				else:
					if verbosity > 0:
						print "WRONG!!!\n"
				for i in predictedEdges:
					if i in groundTruth:
						Rb += 1
				edgeTot += len(groundTruth)	
				###############
		if DUMP:	
			print "Dumping marshalled predictions in " + "PREDICTIONS/PhyloCys-"+str(numBonds)+"_"+TARGET_TREE_DIR.replace("/","_")+".m ...",
			marshal.dump(predictions, open("PREDICTIONS/PhyloCys-"+str(numBonds)+"_"+TARGET_TREE_DIR.replace("/","_")+".m","wb"))
			print "Done."
		print "*******************SCORES**********************"
		print "Num of proteins with %d bonds: %d" % (numBonds, numProts)
		print "Qp = %3.3f" % (Qprot/float(numProts))
		print "Rb = %3.3f" % (Rb/float(edgeTot))
		print "***********************************************"	
	
	return 0




if __name__ == '__main__':
	main()

