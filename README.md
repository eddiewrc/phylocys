# README #

PhyloCys is a disulfide connectivity pattern predictor based on evolutionary information retrieved from Multiple Sequence Alignments (MSAs).
The algorithm has been designed by Gabriele Orlando, Daniele Raimondi and Wim Vranken.

The python implementation of the prediction algorithm can be downloaded, along with PDBCYS and SPX datasets described in the paper and  the best performing alignments, as directory tree or compressed archive.
 

### Requirements and Disclaimer:

The code is released under GNU GPL. The current implementation requires ETE2 library and ClustalW.

* ETE2 (https://pypi.python.org/pypi/ete2/) can be installed on debian-like systems with the following commands:

$ sudo apt-get install python-setuptools python-numpy python-qt4 python-scipy python-mysqldb python-lxml

$ sudo easy_install -U ete2

* CLUSTALW binaries can be downloaded from here http://www.clustal.org/download/current/ and the executable file must be placed in "clustalBins" folder

* We strongly recommend to use HHblits alignments as input because building phylogenetic trees for bigger alignment is unfeasible. A local instance of HHblits can be downloaded and installed on your local machine from here. If the chosen parameters and the local HHblits executables' paths are properly set at rows 20-33 in  sephiroth_standalone.py, the script can build HHblits alignments on the fly and use them to predict the query sequence.
* In order to provide the best tool possible, we are very grateful for bug reports!

### How do I set it up? ###

* First clone the repository on your local machine with the command "git clone https://eddiewrc@bitbucket.org/eddiewrc/phylocys.git" or by downloading the latest compressed archive in the DOWNLOAD section
* The instance of the repository on your hard dirve contains a detailed README pdf file

### Who do I talk to? ###

* Daniele Raimondi: daniele.raimondi@vub.ac.be, draimondi@ulb.ac.be
* Wim Vranken: wvranken@vub.ac.be