LTSSERIDKQIRYILDGISALRKETCNKSNMCE-NLNLPKMAEKDGCFQSGFNEETCLVKIITGLLEFEVYLEYLQNRFESSEEQARAVQMSTKVLIQFLQKKAKNLDAITTPDPTTNASLLTKLQAQNQWLQDMTTHLILRSFKEFLQSSLRALRQM
LTSTERIDKHIRYILDGISALRKEICNKSNMCENNLNLPKMAEKDGCFQSGFNEETCLLKITTGLLEFEVYLEYLQNRFESSKEQAGAVQMSTKGLIQSLQKKAKNLSAIATPDPATNASLLTKLQAQDQWLQGVTTHLILRSFKDFLQNSLRALRAN
LTPPGRTIESIRSILETIKELRKEMCDHDVNCMVNLHLPRLIEEDGCFPPAVNNETCLLRITSGLMEFRMYLEHLQAKFRSDEENTRVVLKNIQHLIKTLRPKVKNLNEEATLKPAVAVSLMENLQQKNQWLKTTTIHFILRGLTNFLEFTLRAVDLM
TSNSQAF-RLFTLVLHDVQELKSETCKHNVNCLNNLNLPKIKIEDGCFYGGYNWETCHLKIITGLLKFQIYLQYMQNKLQSENEKAEKIYTSVKSLSLFMKAKVSNTEQTVFPSPTANATLLEELESQNETQKLLIVQIVLCSLEEFLQNSLRPIRKA
RASSGKTAGQISYLIKEVFEMRKELCKNDETCMNHLNLPKMTEKDGCFQTGYNRDNCLVRITSGLLEFQVYLRYILNKFQKNKDRAEHVQFISKALIEILKQEVKDPNKIVLPSPTANIHLLAKLASQNDWQKVMTMQLILSNFENFLQFTLRAVRKA
YTTSQQVEGLITRVLKEILQMSKELCNNNPDCMNNLDLPVIERNDGCFQTGYDRENCLLKITSGLLDYQIYLEFLKNNVQNKKERARVIQNSTKALNQILKQEVKDPGKTATPSPTSKVLLMEKLESQKDWPRTKTIQLILKALEEFLKITMRSTRQN
LTSPDKTEELIRFILAEISVLRKKMCDKYDKCENNLKLPQMTEEDGCFHSGFNKETCLMKIITGLSEFQIYLDYLQNKFEGSKANVIVVQNSTKALVQILKQKIKNPEDVTTPDPTANASLLSKLQLQTEWLKNTTINLILRSLDDFMQFSLRAVRIM
FTSPDKTEELIKYILGKISAMRKEMCEKYEKCENNLNLPKMAEKDGCFQSGFNQETCLMRITTGLVEFQIYLDYLQKEYESNKGNVEAVQISTKALIQTLRQKGKNPDKATTPNPTTNAGLLDKLQSQNEWMKNTKIILILRSLEDFLQFSLRAIRIM
LSTPEQTEGLITHIIMEINDLNGKMCSKGIKCENKLHLPRLEDDDGCFETGFNKEECLTRITYGLSGYEKYLAYIEGKFEGDINEAVALDLGTKHLIDVLKQKLSNPTQV-TANPTTDSEVIAELDSQEDWQQYTAIHIILVNLKEYLHKTLRALRHI
LISADKM-EIIKYILGRIFALKKEMCDKYNKCENNLHLPKLAEKDGCFQSGFNQDTCLTRIATGLLEFQVHLKYLQANYEGDKENANSVYFSTKVLLQMLMEKVKNQDEVTTPDPTTDTGLQAILKSQDKWLKQTTIHLILRNLEDFLQFSLRAVRVM
YTTSQ-VGGLITYVLREILEMRKELCNGNSDCMNNLKLPEIQRNDGCFQTGYNQEICLLKICSGLLEFRFYLEFVKNNLQNKKDKARVIQSNTETLVHIFKQEIKDSYKIVLPTPTSNALLMEKLESQKEWLRTKTIQLILKALEEFLKVTMRSTRQT
YTTSQ-VGGLITHVLWEIVEMRKELCNGNSDCMNNLKLPEIQRNDGCYQTGYNQEICLLKISSGLLEYHSYLEYMKNNLKNKKDKARVLQRDTETLIHIFNQEVKDLHKIVLPTPISNALLTDKLESQKEWLRTKTIQFILKSLEEFLKVTLRSTRQT
FTSPDKSEELIKYILGRISAMRKEMCEKYDKCENNLNLPKMTEKDGCFQSGFNQETCLMRITIGLLEFQIYLDYLQNYYEGDKGNTEAVQISTKALIQLLRQKVKQPEEVSTPNPITGSSLLNKLQTENQWMKNTKMILILRSLEDFLQFSLRAVRIM
YTTSQQVGGLVTYVLREIYELRKELCNNNPGCMNNLELPVIQINDGCLQTGYNWEICLLKITSGLLDYQIYLEFVTNNVQNKKDKARVIQSTIKTLSQIFKQEVKGPDKIVTPSPTSKAILMEKLESQKEWPRTKTIKLILKALEEFLEVTMRSTRQN
LTSADKMEDFIRFILGKISALKKEMCEKYNKCENNLNLPKLAEEDKCFQSQFNQETCLTRITTGLQEFQIHLKYLEANYEGNKNNAHSVYISTKHLLQKL--RPMNRVEVTTPDPTTDSSLQALFKSQDKWLKHVTIHLILRSLEDFLQFSLRAIRIM
LTSPDKMEEFIKYILGKISALRKEMCDKYNKCENNLRLPKLAEKDGCFQSGFNQETCLTRITTGLLEFQIHLKYIQANYEGNKEDANSVYISTKLLVQMLMKKVKSQDEVTTPDPTTDTSLQAILKAQDKWLKHTTIHLILRSLEDFLQFSLRAVRIM
QSTAGKAGEHMIHITKKIFELREKMCDYNAKCLPDLNLPKLTEKDKCSPTALNKEICLMRITIELLEFQIYLEYIQNKFRNDEENARAVQISLQVETELLMQKVKKIKVVPTLSPSTKSGLLEKLQSKNKWLN-------LRSLEVFLEHSLRAMHMM
LTSSEQIENLIKSILLEISDVKNKMCDNHESCKNNLNLPKLARKDGCFHSGFNQETCLIRITTGLLEFQVYLEYIQNTFE---GHAQAMKIGTKALVNILRQKMKNPVEETIPDPTTNTGLLEKMHAQKNWLKTTTIHLILRSLEDFLQFTQRAIRM-
LTTADKTKQHIKYILGKISALKNEMCNNFSKCENNLNLPKMAEKDGCFQSGFNQETCLMKITTGLSEFQIYLEYLQNEFKGEKENIKTMQISTKVLVQILMQKMKNP-EVTTPDPTAKSSLLAKLHSQNEWLKNTTTHLILRSLEDFLQFSLRAVRIM
LTTPEKTEALIKHIVDKISAIRKEICEKNDECENKLKLPKMEEKDGCFQSGFNQAICLIKTTAGLLEYQIYLDFLQNEFEGNQETVMELQSSIRTLIQILKEKIAGL----ITTPATHTDMLEKMQSSNEWVKNAKVIIILRSLENFLQFSLRAIRMK
LTSADKMEELIKYILGKISALKKEMCDNYNKCENNLNLPKLAEKDGCFQSGFNQETCLTRITTGLQEFQIYLKFLQDKYEGDEENAKSVYTSTNVLLQMLKRKGKNQDEVTIPVPTVEVGLQAKLQSQEEWLRHTTIHLTLRRLEDFLQFSLRAVRIM
LTSPDKTEALIKYILGKISAMRKEMCEKYDKCENNLNLPKMAEKDGCFQSGFNQETCLMRITTGLLEYQIYLDYLQNEYEGDKGSIEAVQISIKALAQILRQKVKNPDEVTTPDPTTNASLMNNLQSQNDWMRNTKIILILRSLENFLQFSLRAVRIK
LTTPEKTEALIKRMVDKISAMRKEICEKNDECENKLNLPKMEEKDGCFQSGFNQAICLIRTTAGLLEYQIYLDYLQNEYEGNQENVRDLRKNIRTLIQILKQKIADL----ITTPATNTDLLEKMQSSNEWVKNAKIILILRNLENFLQFSLRAIRMK
LTSANKVEELIKYILGKISALRKEMCDKFNKCENNLHLPKLEGKDGCFQSGFNQETCLTRITTGLVEFQLHLNILQNNYEGDKENVKSVHMSTKILVQMLKSKVKNQDEVTTPDPTTDASLQAILQSQDEWLKHTTIHLILQSLEDFLQFSLRAVRIM
PTSPNQTENLIKSIFLEISEVRNKMCGNDDSCKNNLNLPKMVEKDGCFQSGFNQETCLKKITTGLLEFQIYLDYLQNKFE---ENAKAMQMRTKALVQVLKQKVKNPDEITTPEPTTNSSLLAKLQSQSEWLQTTTIHLILRSLEDFLQFTQRSVRIM
LTTPDKTEALIKHIVDKISAMRKEICEKNDKCENNLNLPKMKEKDGCFQSGFNQETCLIRSTVGLLEYQTYLDYLQNEYEGDQENVKDLRSSIRTLLQIMRQKSIDL----VTTATTNPDLLEKMQSSNEWVKNAKIILILRSLENFLQFSLRA----
YTTSQQVRGLITYVLSEILETRKQLCNDNPDCMNNLKLPEIQRDDGCFHTGYNRDVCLLKITSGLLEYQTYLEYVKNNLQNKKDKARAIQSNTKTLIRIFKQEVKDPGQIVF-DPTSEALLLEKLESQTEWL--------------------------
-----------------------QMCEKFTVCQNNLNLPKVTEEDGCLLKRTVEDKCLRKISSGLYTFQTYLKYIQETFISENQNVESLSYSTEHLARTIRQMVINPEEVIIPDAATQESLHTELKSTKDWTEKITIHLILRYFTSFMERTVRAVRY-
-----------------------QLCDKYAVCDNSMNLPKMDDR--CLQSSFNKEKCFTKIASGLYAYHIHLLFVQDIYTNEGKVVKDLRISTEQLVDAVKLMVKDYYEVPELDADAQKVLLAKLYSESDWLQKVTARLILRDFTHFMAKTARAIRR-
------------------------MCESSKEAENNLNLPKMAEKDGCFQSGFNEETCLVKIITGLLEFEVYLEYLQNRFESSEEQARAVQMSTKVLIQFLQKKVG-----------------------------------------------------
------CEPLARVLRDRAVQLQDEMCKKFTVCENNLNLPKVTEEDGCLLAGFDEEKCLTKLSSGLFAFQTYLEFIQETFDSEKQNVESLCYSTKHLAATIRQMVINPDEVVIPDSAAQKSLLANLKSDKDWIEKITMHLILRDFTSFMEKTVRAVRYL
------CEALAWLLHARAARLQEEMCEKFTVCENNLNLPKVTEEDGCLLAGFDEEKCLKKLFSGLFTFQTYLEYVQETFTSEKQKVESLCYSTKHLATTIRQMVINPDEVVIPDSATQKSLLTKLKSDKTWIEKITTHLILRDFTSFMEKTVRAVRYL
------CKSLAKTLYKEADALKEENGENQSMCDNNLVFPNLTEQDGCFYSGFNKETCLINLISKLQEFDGYFQFMENELKEKKSRIEALKMFTTQLAESLKKLMMGADLVPTLNPTASHDLVLKLKSLNEWSRKVALHLSLCRYIKFMEHTIRAIRNM
------IVNHAKYLEKTASDLKEEICRIHNLCDNNLLLPNITERDGCLPSSFNEETCLIKIISGLQDFDIFLNYMETEMED--NRFQTLKLSTTQLANTLKTVIKKTDLVPTTNPTTSSILLSELQSLTAWSRKVGFRLILWHYTRFIQGTVRAVRYL
------------------------------------------EEDGCFPLAANHETCLLRITSGLLEFQMYLEHLQAKFRSEEENTRVILKNMRHLINTLRPKVKNFNEGATLKPAVVASLMENLQQKDQWLKTTTIHFILRGLTDFLQFTLRSVRLM
------------------------------------------EEDGCFPSALNHETCLLRITSGLLEFQMYLEHLQAKFRSDEENTRVMLKNIRYLIKTLRPKVKNLNEGATLKPAIVASLMKNLQQKDQWLKTTTIHFILRNLTDFLQFSLRAVGLM
---------------SKAAKLKDEMCEKFTVCDNDLNLPKITEEDGCLLSGFNEEKCLSGISSGLFTFQTYLEYVQETLITEKQKVESICYGTKHLANTVRQMVKNPGAVIMPDPATQNTLFAKLKSNKKWIEKITTHLILRDFTSFMEKTVRAVRY-
--------SLTKTLWKQANGLKDKICDDYSLCENNMNLPKMTEEDGCFLSGFNEETCLKRIVTGLSEFQIYLKYVQKTFKGEEKILESIQNYTKHLSNILKQ--------------------------------------------------------
---------------------------------------------------FNQETCLIRSTVGLLEYQAYLDYLQNEYEGDEENIKDLQSSVRTLLQIMRQKSIDL----VTTPTANLDLLEKM---------------------------------
------------------------------------------------------------------------------------------------------------ETSTPLPTSKALLMEKLESQKEWSRTKTIQLILKALEQFLKVTMRSTRQN
---------------------------------------------------IHEETCLLKITTGLLEFEVYLEYLQRE--------------------------------------------------------------------------------
---------LAKLIEREAASLQIKLCDNHGVCDNNLQLPKISTEDRCFSLGFQKDKCLNKIHRDLSIFKIYLVHVKETFISEKNTVESLQYKTQILIQIMKR-----TETESDDKGTNIQVLQALKSENLWRQRVTNRLILQAFIECIQKTARAVRY-
---------------------------------------------------IHEETCLLKITTGLLEFEVYLEYSDNR--------------------------------------------------------------------------------
---------RIDWLLSVVAACYEDLCANTGICQFNADMPVVEQGSGCFPPSLNETRCLKDLAIGVWGLVEPFVFLNEHFGEYVEHVGAMHMLARAIAWDMRDHANRDLYEPFELPVPKPKMLPLLRRLTTWNKHIAAFNILRRIERFANDANRALSY-
---------RIGWLLSVVSACYEDLCTNTGICQFNADMPVVKQDEGCFPPSLNKTRCLQELAVGFWGLVDPFVFLNEHFGDYVEHVGAMELLVRAIAWDMRDESNRLSQRPFELPSSDSKMLPRLRGLTTWNKHMAAFKILQRIEKFANDANAALTY-
---------RLNWMLWVIDECFRDLCYRTGICKFHLKLPAINDTDHCGLIGFNETSCLKKLADGFFEFEVLFKFLTTEFGKSVINVDVMELLTKTLGWDIQEELNKLTKTHYSPPKFDRGLLGRLQGLKYWVRHFASFYVLSAMEKFAGQAVRVLDS-
--------------------IHSQMCDYNAKCLPDLNLPKLTEKDKCSPTALNKEANLMKLGEGLSEFQKHLELLQNKS-----RANDVQINSQALTQILKQMCLNPDALITQPPKSDSW---RVHSQGQWLKNMMIPLSFAMLKKLLELHVRAVHLM
-------------------------------AENNLHLPKL-GKRWMLQSGFNQETCLTR--------------------------------------------------------------------------------------------------
----------LESVLGATKLHKNEFLVEFQG-EDRYKIPSLPA--KCPYSNFGKDACLRRLLEGLLIYSVLLKRVEEEFPSSSI-LSEVRFYSNILIKELENKVRDRDQVMRLTSSQEEQLLKDTDYPDTFHRKMTAHGILYNLHYFLVDCRRVINKR
----------WDLIIGVTAHHQKEFEDEFQQ-ENHYKLSSLPA--DCPSANFSKEACLQRLAEGLHTYMVLFKHVEKEYPSSSI-LLHARYHSGALIGLIKEKMRNPGQVTVPTSRQEQQLLQDMDNPSTFHRKMTAHNILRQLHNFLRNGKVAIRKR
----------WHAVLGVTKLHKKEFEDEFEN-VENYKVFSVPE--RCPHSNFSKEACLHRMAHGLLVYTVLLKHVEKEYPGSLI-CSLAKYYSGLLINLCKEKMRNPDQVAALTDRQEAQLLRGLNLPSAFQRKMAAHSVLRQLHYFLLDGRRAIAKR
----------LESVLRATKRHKKEFLAEFQG-EDRYKIPSLPA--KCPYSNFGQDACLRRLLEGLLVYSVLLKHVEQEYPLSRI--SEVRYYSNVLIKEVENKVKERGQVTTLSSSQQEQLLRAVDRSDTFHRRMTAHGILYNLHYFLVDCRRVINKK
----------WGLIISVTARHQQEFEDEFQQ-EDHYKISSLPA--NCPSANFSKEACLHRLGEGLHTYMVLFKHVEKEYPSSPI-LLYARYHGGALIGLIKEKMRNPGQVTVPTSSQEQQLLQDVDSPNTFQRKMTAHNILRHLHDFLRNGRVAIRKK
----------WRSLFDSAQEYEKAFEHHFQTLEDSHTPASIPK--HCNITKFRKDACLQTLAKGLLIYSVLLKHVEKEYRSSLN-FSDAPSNIGTLIGMVKGKMKNRNQVTPLTSSEEEQLLKEVNSPDPYHRKLHAYSILRALKAFLSEGKRAVCRM
----------LKVVLEVIKTHRQEFEAEFH---AQYNIPSLPA--DCPSTNFSMEALLHRLLQGLPVYTALLKYVEKEEPKSQI-PSRFRQNSELLKQRITGKMRHAVQVTPLTSSQEQQLLRDLDSSDTFHRKMTAHSILYQLRSFLVDCKNAINKK
----------WERLIEATKQHQKEFEQEFQG-NEGHKRSAFPV--KCPMSNYSKEACLQRLAQGLLVYTVLLKQVEKEYPDNSI-LREIKPGIPLLVAQIKEKMKRSERVPAPSSSQEEQLLKQLDSPDTFQRRMTAHSILSHLRFFLIDGKRAFRK-
---------------------------------------------------------------------------------------------------FSFQINNTEQMEFLSPTPDATLLEKLETQSQTQMLLIAEIVLQRLEEFLQDSLRAIRK-
-------------------------------------------PAGCP-VTMNKEACLLRLVQGLQKYKVLLTHVKKEYP---DNALLIKYNSDLLIHLIEEKMRHPERLTVLSDTEAQSILQGLENTNTFQRKVTAHSILRKLHLFLIDSSRDL---
---PPKWEKMIKMLVHEVTTLRNQFVEEFQKPVSQHQVPSTPPKTLC--SASNKEACLQEISRGLQVYQLLLQHVKAEYPQSTL-LPSVTHQTTVLIGLVKDQMKVAEVVEDLSASERKRVLGEVSTGTEWERKTSVHAILRELRNFLVDTKRALRRM
---------ALESVLRATKRHKKEFLAEFQG-EDRYKIPSLPA--KCPYSNFGQVGRVPALTTGLFGFFPSFGWISTRFSSSAGSLSEVRYYSNVLIKEVENKVKERGQVTTLSSSQQEQLLRAVDRSDTFHRRMTAHGILYNLHYFLVDCRRVI---
---------------------------------------------TCP-EKSSKEACLRCLAQGLLTYTALLKHVEKESPSSIRSEHSFKSLLLRLTSGIKNKMRHREHVKALTNSQEGHLLRDFDSPDPFQRLMTTFKILYKLRDFLIDGIKNI---
-----QIQEQVRKIQNDIEGLQQRLCADYRLCHLYLGIPRPSV-KNCYSQDLKLVACLNQLYYGLQFYQGLVRALEGISPELAPTLDTLKLDIGDFAASIWQQMEDFQVVPEIVPTQSIL--PTF--SSSFYRRAGGVLIFSQLQSFLDMAYRALRLL
-----KSLEQVRKIQARNTELLEQLCATYKLCHHSLGIPKASL-SSCSSQALQQTKCLSQLHSGLFLYQGLLQALAGISSELAPTLDMLHLDVDNFATTIWQQMESLGVAPTVQPTQSTM--PIF--TSAFQRRAGGVLVTSYLQSFLETAHHALHHL
-----QSQEQVRKIQMDMEELQQRLCADYSLCHQYLGIPRPPL-KNCYSQDLRLAACLNQLHDGLQLYQELLKALQGISPELAPTLDTLQLDVADFTATIWLQMEDLQVTPKIMPILGTL--PSF--SSSFWRRAGGILIFDRLQAFLETAYRALRHL
-----KSLEQVRKIQASGSVLLEQLCATYKLCHHSLGIPKASL-SGCSSQALQQTQCLSQLHSGLCLYQGLLQALAGISPALAPTLDLLQLDVANFATTIWQQMENLGVAPTVQPTQSAM--PAF--TSAFQRRAGGVLAISYLQGFLETARLALHHL
-----KSLEQVRKIQTRNSELLEQLCATYKLCHHSLGIPQAPL-SRCSIQALQLTKCLSQLHSGLFLYQGLLQALTGISPELAPTVDMLQLDVANFATTIWQQMESLGVAPTVQPTQSTL--PTF--TSAFQRRAGGVLAASHLQSFLETAHCTLNHL
-----KCLEQVRKVQGDGTELQEKLCATYKMCHHSLGIPQVSL-SSCPSQALQLTGCLRQLHRGLFLYQGFLQTLEGISPELAPTLDMLRLDITDFATTIWQQMEELGTAPALQPTHGAT--PAL--PSAFQRQVGGLLVASHLQSFLELVHRVLRHL
-----KSSEQARRVQAEAMVMQEKLCATHRLCHHSLGIPWASL-RSCSSRDLQLLGCLRQLQAGLMLYQGLLQALAGISPALSSSVDLLRLDVADFATTVWQQMEDLGVAPAAQPTLGTV--PAF--TSAFQRQAGGVLVASHLQDFLELALRALRYL
-----KCLEQARRVQAEAVEMQQKLCATYRLCHHSLSIPWVPL-KGCSSQDLQPTRCLRQLQAGLVLYRGLLRALVGISPELSPTVDTLQLDVADFATTIWQQMEDLGTAPAVQPTPGTM--LTF--TSAFQRQAVGAMVASQLQCFLELAYRALRYL
-----KCSEQVRKIQADTSMLQEELCATHKLCHHSLGIPRAPL-SSCSSQALQLPGCLSQLHSGLLLYRGLLQALAGISPASAPALDKLQLDVADFATTIWHQMEDLGQAPAVQPTQSTM--PAL--TSAFQRRAGSVLVASHLQSFLELTYRVLRYL
-----KCLEQVRKIQADVVAMQERLCATHKLCHHSLGIPQVPL-GSCSSQALQLTSCLGQLHGGLFLYQGLLQALAGISPELGPTLDMLQLDITDFATNIWQQMEDLGMAPGVQATQGTV--PTF--TSAFQRRAGGVLVASSLQSFLQLALRVLRHL
-----KCLEQMRKVQADGTALQERLCATYKLCHHALGIPQPPL-SSCSLKSLWTTGCLRQLHGGLFLYQGLLQALAGVSPELAPALDTLQLDISDFAVNIWQQMEELGVAPAVPPTQGTM--PTF--TSAFQRRAGGVLVASDLQSFLELAYRALRSF
-----KCLEQVRKIQGDGAALQEKLCATYKLCHHSLGIPWAPL-SSCPSQALQLTGCLSQLHSSLFLYQGLLQALEGISPELSPTLDTLQLDIADFATTIWQQMEDLGMAPALQPTQGAM--PAF--TSAFQRRAGGVLVASHLQRFLELAYRVLRHL
-----KCLEQMRKVQADGTALQETLCATHQLCHHALGIPQPPL-SSCSSQALQLMGCLRQLHSGLFLYQGLLQALAGISPELAPTLDTLQLDTTDFAINIWQQMEDLGMAPAVPPTQGTM--PAF--TSAFQRRAGGVLVASNLQSFLELAYRALRHF
-----KCLEQVRKIQADGAALQDRLCATHKLCHHSLGIPQPLL-SSCSSQALQLTGCLSQLHSGLLLYQGLLQALAGISPELAPTLDMLQLDVTDFATNIWQQMEDLGVAPVVQPTHGPM--PTF--ASAFQRRAGGVLVASNLQRFLELAYRGLRYL
-----QCLEQVRKIRADGAVLQERLCATYKLCHHSLGITQAPL-SSCSSQALEVVDCLHQLHSGLVLYQGLLQALAGTSPEVASTLDLLHLDVADFAINIWQQMEDMGVAPTGKPTQGPM--PTF--TSAFQRRAGGVLVASNLQSFLEQAYRVLRYL
-----KNLEFTRKIRGDVAALQRAVCDTFQLCTPDPHLVQAPL-DQCHKRGFQAEVCFTQIRAGLHAYHDSLGAVLRLLPNHTTLVETLQLDAANLSSNIQQQMEDLGLDTVTLPQRSPP--PTF--SGPFQQQVGGFFILANFQRFLETAYRALRHL
-----KDLEYIRKIKGDVARLQELMCSTFQLCSQKLGITQAPL-DQCHSKTFQVSGCPRSVRASTEAW-GALGLREATCPGQGLGAPGVS-GPGSLPSSAPLQMEDLGLNTVTYPGQGPL--PSF--SSDFEKQASGFIILANFQRFLEMALRALRHL
-----KNKEFVARMKSEISDLKESMRTDFSLGTDFLGIEQADH-SHCQKATCDLGNCFKQLRAGLHTYYGYLSHIKQILPNYTNRVSSLQLDTSNLSTNIQLQFEESSLPVVTYPQAENQ--PNF--LQ--QREIGSYLVLRKFMLFMDVITRALNHC
-----KCLEQVRKIQAQGSVLQEKLCATYQLCHHSLGIPQAPL-SNCSSQGLQLTGCLSQLQSGLFLYQGLLQALAGISPELSPTVDMLQLDVANFATTVWQQMEDLGVAPAVQPTQGTM--PTF--TSAFQRRAVGVLVASRLQRLLELVYRVLRHL
-----KCLEQVRKIQADGVVLQERLCATHNLCHHSLGISQAPL-SSCSSQALQLTGCLRQLHSGLFLYQGFLQALAGISPKLAPTLDMLHLDIADLATNIWQQMEDLGVAPAVQATQGTM--PIF--TSAFQRRAGGVLVAANLQSFLELAYRVLRYL
-----KCLEQVRKIQNEGAALQENLCATYKLCHHSLGIPRTTL-NGCPSQDLQLTSCLSQLHRGLFLYQGLLQALAGISPELAPTLDMLQLDVADLATTIWQQMEDLGVAPALQPTQGAM--PTF--TSAFQRRAGGILVTSHLQSFLELAYRVLRYL
-----KCLEQVRKIQADGAELQERLCATHKLCHHSLGLPQASL-SSCSSQALQLTGCLNQLHGGLVLYQGLLQALAGISPELAPALDILQLDVTDLATNIWLQMEDLRMAPASLPTQGTV--PTF--TSAFQRRAGGVLVVSQLQSFLELAYRVLRYL
-----KCLEQVRKIQADGAELQERLCAAHKLCHHSLGIPQAPL-SSCSSQSLQLTSCLNQLHGGLFLYQGLLQALAGISPELAPTLDTLQLDVTDFATNIWLQMEDLGAAPAVQPTQGAM--PTF--TSAFQRRAGGVLVASQLHRFLELAYRGLRYL
-----KGLEQVKKVQGDGTELQEKLCATYKMCHHSV-IPQVSV-SRCPSQALQLTGCLRQLH-SLFLYQGLLQTLEGISPELAPALDMLCLHIADFATTIWQQMEELGTAPVLQPTRGAT--PAL--PSAFQRRVGGLLVASHLQSILELVHRVLRHL
-----KNLEFTRKIRGDVAVLQRVVCDTFQLCTPDPPLLQAPL-DQCHKRGFQAEMCFGQIRAGLHAYQDSLGAVLQLLPEHTALVETLQLDAANLSTNIQQQMEDLGLDTVTLPHRSPP--PTF--STDFQQQVGG---------------------
------------DLYKDVKTLRDEFERDFREMVVRISTPLLKPSDRCLSKNFSTERCLTRIYSVLTWYKDNWNYIEKENLTSVL-VNDIKHSTKRLLEAINSQLQVRDGEMDQTSST------SLSFKSAWTRKTTVHSILFNFSSVMIDACRAINYM
--------EFSRKITRDAEKLKNLMCDNHGLCEEHLQLPDVPLG-QCQAGSFSQEGCFSQLSNGLQELQRRVVAMPGHLPEA--DLQRLEKDISDLLVNIQEEMEAQGITAQSSPSQA---LP--TYSKGFHKKVAMFLILSDLAS------------
-------------------------CDTLQLCKQELDIAQAPLE-QCHRRTFQAETCFSQIRAGLRIYGGSLATIQALLPGHAGLVETLQLDMANLSSNIQQQMEDLGLATVTYP-QDPV--PT--FSSHFHHQVGGFFILANFQRFLETAYRALRHL
-------------------------CTKHSLCHHYLGVRQASVN-RCPSRDLELGPCLKQLARGLKLYQAQLEALEGISPQLAPALDTLQLDVRDFAINIWQQLEDLRLTATPLSPQASV--PT--FTSAFQRRAGAVLVLDNLQGFLEVVARVLNQL
---------LAEKIHPEIKNLQDLICKQHHICNDYLPHPRLEE---CRSNNLQLDTCMNKMEAGLQVYGDYLNLIKKVIVGSSL-AETVQADIMDLQQHIRQKVIDVAILVLTLPNFAA----------SFDEQAGGHIVLSHFKRFMETVLRVLQYM
------------------------------------DVPRIQCGDGCDPQGLNSQFCLQRIRQGLVFYKHLLD--SDIFTGEDSPVDQLHTSLLGLSQLLQPEDHHWETQQMPRLSPS----------QQWQRSLLRSKILRSLQAFLAIAARVF---
------------------------------------NVPRIQCEDGCDPQGLNSQFCLQRIRQGLAFYKHLLD--SDIFKGEDSPMEQLHTSLLGLSQLLQPEDHPRETQQMPSLSSS----------QQWQRPLLRSKILRSLQAFLAIAARVF---
------------------------------------DVPHIQCGDGCDHQSLNSQLCLQQIHRGLIFYGHLLN--SDIFKGEDGPVGQLHSSLLGLSQLLQPKDHSWKKQQVP---PS----------EPWQRLLLRPKILQRLRAFAAIAARVF---
------------------------------------DVPRIQCEDGCDPQGLNSQSCLQRIHRGLVFYEKLLG--SDIFTGENGPVDQLHASILGLRELLQPKGHHWETEQTPSPIPS----------QPWQRLLLRLKILRSLQAFVAVAARVF---
------------------------------------DVPHIQCEDGCDPQGLNTQFCLQRIRQGLFFYEKLLG--SDIFTGEDGPVGQLHASLLGLSQLLQPEGHHRETPQTPSLHPS----------QPWQRRLLRFKILRSLQAFLAVAARVF---
------------------------------------EVPRIQCGDGCDPQGLNSQFCLQRIHQGLVFYEKLLG--SDIFTGEDGPVGQLHASLLGLRQLLQPEGHHRETEQTPSPSPS----------QPWQRLLLRFKILRSLQAFVAVAARVF---
------------------------------------DVPHIQCEDGCDPQGLNSQSCLQRIHQGLVFYERLLG--SDIFTGESGPVIQLHASLLGLRQLLQPEEHHWETEQTPSPSPS----------QPWQRLLLRLKILRSLQAFVAIAARVF---
------------------------------------DVPHIQCEDGCDPQGLNSQFCLQRIHQGLLFYEKLLG--SDIFTGEDGPVGQLHASLLGLSQLLQPEGHHWEPQHTASPIPS----------QPWQRLLLRFKILRSLQAFVAVAARVF---
------------------------------------DVPHIQCGDGCDPQGLNSQFCLQRIYQGLIFYQKLLG--SDIFTGEDGPVGQLHASLLGLSQLLQPEDHQQETQ-TPSLSPS----------QPWQRLLLRIKILRSLQAFVAVAARVF---
------------------------------------YVPHILCGDGCDPQGLNSQFCLQRIYQGLVFYQNLLG--SDIFTGEDGPVSQLHASLLGLSQLLQPEVHQWEPQ-IPSLSPN----------QPWQRLLLRIKILRSFQAFVAVAARVF---
-----------------------------------------------VSENYSLETSLRHMSEGFQLHRDLLSAVSPRLAN-KDNVTGLVADIRDLVLRIN-EMLKMAQTDAVVQPSPTPVALNLPG--NYEVQVAAHMILVQLQAFGQDTARCLRS-
-----------------------------------------------LSERFTMDMCVSRMSVGCLLYQGLLGVLADRL----SGLTNLRADLRDLLTHIN-KMKEAAQFGAESPDQNQDLASRLHG--NYEVQVAVHVTLTQLRSFCHDLIRSLRA-
-----------------------------------------------LSEHFTLDVCVSRMSSGILLYQRLLGVLSDRV----SGLSDLQADLRDLQTHVA-TVREVAQLRDPELDENQDLASRLHG--NYKVQIATHLTLTHLRSFCHDLIRSLRG-
-----------------------------------------------LSEQFTADMCVSRMLAGGRLYQGLLVDLSGRL----GGLEALSAELRDLVTHIN-QMKDAAQLSSDSSDQSSDLTLNLHG--NYDVQVAAHLTLCQLLEFCHDLIRSLRN-
-----------------------------------------------LSENFTMGTCLRRISEGLQLHRTLLAVIADHLKN-KDRVLALQADIRDLNIQIN-KMLKMVGEETVVPP---AVTLNLPA--DYEVQVAAHLTLQQLQTFGRDVDRHLKS-
-----------------------------------------------VCRDFTLEVCLSSMSAGLQLYQDVLGVLKERVT--TEKVTGLLADIRDLLAQVN-KMQEPGQMSSVAQYEASGLASRLPG--DYEVQVATHFILLQLRDFTQNLKRSLRN-
-----------------------------------------------PSDHFTLDMCVSRMLVGCQMFQRLLAVLSEKL----DGLMDLKVTLRDLVTHIT-KMKETLGLDVDGSEALTDVASRLHG--DYEAQMAAHLALVQLRSFCHDLTRSLRA-
-----------------------------------------------PSEQFTLDICVSRMLVGCQMFQKLLGVLSERV----DGLMDLKVTLRDLVTHIT-KMTETVRLNGDTPEAPSDAASRLPG--NYEAQMAAHLTLIQLRSFCHDLTRSLRA-
-----------------------------------------------LSDNFTLNTMLRHMLEGLQLHKDLLNHVLPRLEV-KDMVIDLTHDLNDLSVQVL-KMLKLAQREGVSKPNPTGLALDLRG--SYEVQVAIHLILVQLQGFEQDMDRCLRS-
-----------------------------------------------LPHEDTLDVCLSRMLAGVQLYQGLLGDLSGRL----SGLSDLKADLRDLLNHIT-EIKKAAQLGGEAVQNQSDLASRLQG--DYEVQVAANATLKQLLSFCHDLMRSLKK-
-----------------------------------------------PSENVTLEDSLRQLYEGLQLYRALLGSLGNRLAN-KDKVTGLMADVKDLAIQIK-EMLKIVRPQGSPHPTKPSVSLNLPA--EYEFQVAAHLTLVDLQSYIQDLVRFLRT-
---------------------------------MSLQIPGPPV-LKPFSDSFTLDTCVSRMLAGVQLHQNLLAVLSGRLA----GLEELKADVRDLLAHIM-KLKEAALMEGSALERGSDLDARL--TDSYTVQVAAHTVLTQLRSFCHDLTRSFRAL
---------------------------------SIIGIPAAPV-LKALSPNVTLETSLALVSKGLQLYEDLLGIIVNHLEQKKE-LSDLKAHISDLKKLITRMLKVAGGQAEDLPKPT----LNL--PGDYEVQVAAHLTLLQLQSFGQDVGRCLES-
-----------------------------------------------------LESSLILAHEGLQQHQALLSSISPHLENQ-QRATDLMNTVRDLAVQIS-KMLQGLQTDYVLQTTPSPVALRLHG--DFEVQVAAHLTLVQLQSLGQDVHRFL---
-----------------------------------HYIPSAPVL-QNITDISSLETCLDKIVRGLQLHLNLLKDLIETTLSQTDQVTELQADIQELVLLIE-ELQNQSGFTSEEQSQSFNLTQHL--KSDFQTEAAAHLILHQLRDFSCDIL------
------ALSLAKKILNDIPAVQ-ELCAPNTGLSSEFQIPTLPL---LKSEDLTLEERVQRMLIGLELHRVLLKLLEPCRPE------NLLNDLAELRTLLLLQVPSAG--PQVLPHTHTL-SAP---LSQFKLQVRVRATLRQLRSFMQDVFRSLRC-
---------------------------------NHHRFKSLPAISSSDFATLEVKPTLSQLHANLKSFQHHFEWLNNITHKSIPKLTDMVSHIGGLVNSLQRQMNHIGAPRLPVPSP--SLPPIP--AFHWEMVQTSQELLEQFSLFCDWAARVLGR-
---------------------------------EQHRFKSLPLMNNSDINSLEMRPTLSQLHADLKSFEHHFAWLSRASRKALPKLGQMMSLIKSLTSMLEHQMMRVDAQRLSPPSP--SMPPPP--PSQFDVLQSSQELLLQFRLFCDWAQRVFLS-
-----------------------------------NKLPDLPHMQHHFFNSLKMNESLSELYLLAQAFRLHVDWLKTE---S----EDASTHL---SNLLNMSLHQ-SA--PQPPAP--SLP--V--SSAFDLLQFSIEISERLKVFCNWSKRVLRS-
---------------------------------EPHKFPSLPEMSNNDLNNLELKPTLSQLHAELKLYEHHFEWLNRVSKKALPKLVEMIKELKSLISLLHHQMLRVEAPRLNLTTP--SLPPQL--PYQFDVLQSSHELLQHFKLFCDWAYRAFIS-
---------------------------------ESHRFRSLPEMSNTDLHNLELKPTLSQLHADLKLYKDHFDWLSNVSKKAVPKLEKMITEMKSVITMLHHQMQRVEAPVLTPATP--SLPSHL--PYHFEVLQSTHELLQHFNLFCDWAIRAFIG-
---------------------------------EPHRFRSLPEMSNNDLNNLELKPTLSQLHADLKLYEHHFEWLNKVSKKALPKLVEMIREMKSLINLLHRQMLRVEAPKLTQATP--SLPPHL--PYQFDVLQSSHELLQHFKLFCDWAYRAFIS-
---------------------------------DHQRFKSLPAISSSDLTTLEFKPTLSQLYADLKSFEHHFEWLNRTTRKSVPKLTDMISHIKSLINSLQRQMTRAEAPRIPVPSP--SLPPNP--AFHWEVVQSSQELLQQFRLFCDWASRVFLT-
-----------------------------------DPTPQIQCSDRCDPDGLDGELCLRRLQEALTFYGHLLG--SDLFSGQPGPVGQLRSDLLDLGRLLQQPL------------------------------------------------------
-----------------------YLCHHSTLCRSFVHFPILMSNVECQRREFRGAECMNAMVRGLRAYESYLTRLRDDAPGDADAATVVLSALDSLIEELP----VNNKIGGAE--SNEKTVRALGGQSPRDVVLSAFRILEYLQMFLRDGRRAIAMM
------MVNLAKNILHDTKIAFRQFKLKFPS-EHRLTLPVLSG-SASDLGAIQVTNALSKIDSDLRSYEEHFEWLKKAEVISKQSITKIHSKTQNLANHIEHLMNRHNIPKNFSPPQL---PP--QHATHWSAVHAGHAIFHHFKLFMDWTVRALV--
------LLNMARHLLRDTKQSYHNFKAKYPS-EHKLSLPVLSM-NAVEISNVQIHTGLLKLSTDLFIYQKHFDWLRKAAHAIRHEFNGIHNRIEKLLKKIDLLMIKHHMPR-VTEPVV---PP--NSTTPWGVIQSGHAIFHHFHLFLDYATRVLV--
