DVRQEELGAVVDKE-AATSAAIEDAVRRIED-NQARHASSGVKLEVNERILNSCTDL-KAIRLLVTTSTSLQKEIVESGRGAATQQEFYAKNSRWTEGLISASKAVGWGATQLVEAADKVVLHTGKYEELIVCSHEIAASTAQLVAASKVKANKHSPHLSRLQECSRTVNERAANVVASTKSGQEQIEDRDT
DIKQEELGDMVEKEMASTSEAIEDAVRRIEEMNQARNESSGVKLEVNERILNSCTDLMKAIRLLVTTSTNLQKEIVESGRGAATTQEFYAKNSRWTEGLISASKAVGWGATQLVESADRVVLHTGKYEELIVCSHEIAASTAQLVAASKVKAEKSSRNLARLQECSRNVNEMAANVVASTKSGQEQIEEKDT
DVRQEELADMVDKEMSATSSAIEDAVLRMEELNQTRRETTGVKLEVNQSILGSCSDLMKAIHMLVTASTDLQRDIVESGRGSGSAKDFYAKNSRWTEGLISASKAVGWGATQMLDSADKVVTDRGKYEELIVCSHEIAASTAQLVAASKVKADRGNKKLHTLQQASRHVNDMAAVVVTSTKSGQMQIEDKNS
DLDAEKVGLLLEDEMKRMDEAIKKAVQVIEDQKKSRATDSGIRLEVNEKILDSCNALMSAIIVLVSKSRAMQEEIVAAGRGTASPKEFYKRNHQWTEGLISAAKAVGVAATVLVQSADGAITGKGKLEHLIVASQEIGASTAQLFVSSRVKADRGSQKLAELLTASRAVNSCTASVVATVKSGQQKLSDNET
DVRQEELGAMVDKEMAATSAAIEDAVRRIEDLHRTDEGAPGPGRAPSGPPESHTHLGLQAIRLLVTTSTSLQKEIVDSGR---------TPNSRWTEGLISASKAVGWGASQLVESADRVVLHTGNDEDLFVCCHEIAGSTAQLVAASKVKADKHSPCLSRLQECSRAVNERAAGVVASTKSGQEQIEDRDT
DVKEKEIGDLLENEMHSTSSAIELAAKKIEELHQTRKDMSGVELTVNESILDSCTGLMQAIKVLLVKSQDLQKEIVLQGRGTASVKEFYKKNHRWTEGLLSAAKAVGMGATSLLEAADKVVKGEGKFEELIVCSNEIAASTAQLVVASKVKADRKSKKLSELSEASRGVTSCTGKVVGSAREASQIIEEQNL
DVRQEELGDLVDKEMAATSAAIEEAVRRIDEMNQARKDTTGVKLEVNERILFSCTDLMKAIRMLVIASTDLQKEIVEGGRGAATIREFYAKNSRWTEGLISAAKAVGWGATEMVESADKVVLHTGKYEELIVCSHEIAASTAQLVAASKVKADRHSKKLTKLQQASRQVNEMAANVVASTKTGQEHLEDKDT
DIRQEELGDLVDKEMAATSAAIEEAVRRIDEMKQAQKDTSGVKLEVNERILNNCTGLMKAIRMLVKASTDLQKEIVESGKGAATIKEFYARNSRWTEGLISAAKAVGWGATEMVESADKVVLHTGKYEELIVCSHEIAASTAQLVAASKVKADRHSQKLPVLQQASRHVNEMAANLVASTKSGREHLEDKDP
DVRQDELGDLVDKEMAATSAAIEEAVRRIDEMDQARKDTSGVKLEVNERILNSCTDLMKAIRLLIISSTDLQKEIVEGSRGAASIKEFYARNSRWTEGLISAAKAVGWGATEMVESADKVVLHTGKYEELIVCSHEIAASTAQLVAASKVKAERNSKRLSVLQQASRHVNEMAANVVASTRTGQEHLEEKET
DIDKEVVGSELEQEMRRMDEAIRRAVQEIEAQRRARESSDGIRLEVNESILANCQALMSVIMQLVAASRELQMEIVAAGKQGGSPAEFYKRNHQWTEGLLSAAKAVGVAARVLVESADGVVTGKGKFEHLIVAAQEIAASTAQLFVSSRVKADKDSKKLEALSIASKAVNQNTAQVVAAVKTGQTTLNDGDS
ELQQGELGDLVEQEMAATSAAVESAAARIEDLNKSRAVDTGVKMEVNERILASCTDLMQAIKELVLSSKHLQRDIVESGRGAASMKEFYAKNSRWTEGLISASKAVGWGATVMVDAADLVVQGKGKFEELMVCSHEIAASTAQLVAASKVKADKDSGNLQRLQQASRGVTQATAKLVASTKSGKSQIEETDT
DVRGGDLHDLVEQEMASTTQAVEQAAERIEALNEARQRDTGVNLEVNERILDSCTELMKFIQILISSSKDLQREIVEQGRGASSTTEFYMKNSRWTEGLISAAKAVGWGATMLTDAADLVIQGDGKFEELMVCSHEISASTAQLVAASRVKAGKNSTKLKTLQSASRNVNGATAKVVASTNAGRRQLDEESL
DIKQEELGDMVDKEMASTSAAIEDAVRRIEEMNQARNKSSGVKLEVNERILNSCTDLMKAIRLLVMTSTHLQKEIVESGRGAATTQEFYAKNSRWTEGLISASKAVGWGATQLVESADRVVLHTGKYEELIVCSHEIAASTAQLVAASKVKGEKNTPHRSASQLISRVDSHSSAPTVERLLLGRESVTPPDT
DVRIEELGFVVDKEMIATSTAIEEAVLRMDEMTKAKKDTSGIHLEVNQSILGSCSDLMKAVHMLVTAATDLQKDIVEGGRGAATVTEFYAKNSRWTEGLISASKAVGWGATQLLDSADRVVNENGQYEELIASSHEIAASTAQLVAASKVKADRSNKKLNTLQQASRHVNDMAAVVVTSTKHGQQQISDQGV
DIKQEELGDMVDKEMAATSAAIEDAVRRIEEMNQARKGSSGVKLEVNERILNSCTDLMKGIRLLVMASTDLQREIVDSGRGAATPKEFYAKNSRWTEGLISASKAVGWGATQLVESADRVVLHTGKYEELIVGSHEIAASTAQLVAASKVKAAQSSKNLSRLQHASRHVNEMAANVVASTKSGQEQIEDKDT
DVLREELGTMVDKEMVATSTAIEEAVLRMDELTQAKKDTTGLKLEVNQSILGSCSDLMKAVHILVTAATDLQKDIVEGGRGAASTTEFYAKNSRWTEGLISASKAVGWGATQLLDSADRVVCEKGTYEELIACSHEIAGSTAQLVAASKVKADRSNKKLNTLQQASRHVNDMAAVVVTSTKHGQKQISDHGI
DVEQEELGDLVDKEMAATSAAIETAAARIEELSKARAGDTGVKLEVNERILGSCTGLMQAIQVLVLASKDLQREIVESGRGAASPKEFYAKNSRWTEGLISASKAVGWGATVMVDAADLVVQGKGKFEELMVCSHEIAASTAQLVAASKVKADKDSSNLSKLQQASRGVNQATAGVVASTKSGKSQIEEKDS
DVGKDELGDMVDKEMAATSAAIEEAVRRIDEMSQTRKDATGVQLEVNERILNSCTDLMQAIRLLVLASTDLQKEIVEGGRGAASVREFYARNSRWTEGLISASKAVGWGATQMVDSADKVVLHTGKYEELIVCSHEIAASTAQLVAASKVKADRGSKRLGNLQQASRHVNEMAAKVVASTKTGQDQVEDKDT
GHQGKEAAEVVQGSWQDAGISRVLKWADIRARGQHRQGAVGGVSPPPTHILNR--NFLLSPPRAGIWGCSVTNCAFTNGR-AASPKEFYAKNSRWTEGLISASKAVGWGATVMVDAADLVVQGKGTFEELMVCSREIAASTAQLVAASKVKADKDSANLCKLQQASRGVTQATASVVASTKAGKSQVEEKGN
DIKQEELGDLVDKELAATSAAIETATARIEELSKSRAGDTGVKLEVNERILGSCTSLMQAIQVLIVASKELQREIVESGRGTASPKEFYAKNSRWTEGLISASKAVGWGATVMVDAADLVVQGRGKFEELMVCSREIAASTAQLVAASKVKADKDSPNLAQLQQASRGVNQATAAVVASTVSGKLQIEDTDN
HVFYEGMGS---KKLEASKMGIEPATLQSEETGPPSLGLTLCILLFGARILASCTELMEAIKALVLSSKDLQRDIVESGRGAASMKEFYAKNSRWTEGLISASKAVGWGATVLVDAADLVVQGKGKFEELMVCSHEIAASTAQLVAASKVKADKDSTNLQRLQRASRGVTQATAVVVASTKSGKSQIEDTET
DIRQEELGDLVDKEMAATSAAIETATARIEELHRSRAGDTGVKLEVNERILGSCTSLMQAIQILILASKDLQKEIVESGRGAASPKEFYAKNSRWTEGLISASKAVGWGATVMVDAADLVFQGKGKFEELMVCSREIAASTAQLVAASKVKADKDSANLARLQQASRGVNQATAEVVASTMAGKSQIEETES
EADTQKLGDLLEDELAQMDRAIDEAAQRIQDLNKSREGDSGIKLEVNGKILDACTGLMQAIRELVKSSKHLQEEIVAKEKGSASKKEFYSRNHRWAEGLISAAKVVGLGAKFLVDAADRVVSGGGKFEELVVASQEIAGATAQLVVASRVKADRGSTRLAALGAASKGVSQATGNVVATAKACAHLIEDSEV
DIREEELGDLVEKEMAATAAAVETAATRIEELNKSRTDDTGVKLQVNERILGSCTDLMQGIQALILASKDLQKEIVESGRGAASQKEFYARNSRWTEGLISASKAVGWGATVMVDAADLVVQGQGKFEDLIVCSHEIAASTAQLVAASKVKAEKDNANLSRLQQASKVVNQNAAGVVASTKCVMSQIEEKDT
TDATDMAEDLVEGELIAMEKAIEEATQKMGELNESRKSHSGVKLEVNEKILDSCTNLMTAIKILIKRARLLQAEIVSQGKGTNSSKEFYKRNHQWSEGLISAAKAVAVGAKFLLTCADKVVSGQGKFEQLIVASQEVAASTAQLVVASRVKADKKSENLKELSSASKGVAQSTAGVVATAKSCSQLIEESED
EEESAQMGDIVDEEMASTSIAVNEAARKIEELKRSREEQSGVNLEVNSRILDSCTDLMKAIQVLIVRSKELQQEVVAEGPGS-NAKEFYKKNHNWSEGLISAAKQVGHGASRLVDCADKVVSGNGKFEALIVNSKDIAACTAQLVAASMVKARARSERLKALKASSRAVSEATGSVVASAQSGADKAEAANK
DLKQEELGDLVDTEMAATSAAIETATARIEELSKSRAGDTGVKLEVNERILGSCTSLMQAICIITLVSHSFAGATVCSSQGTASPKEFYAKNSRWTEGLISASKAVGWGATVMVDAADLVVQGRGKFEELMVCSHEIAASTAQLVAASKVKADKDSPNLAQLQQASRGVNQATAGVVASTISGKSQIEETDN
EADTRKLGDMVEEELSQMDKAIEEAAKRIQDLEKSREGDSGIKLEVNSKILDACTGIMQAIRELVKSSKHLQEEIVGKEKGSASKKEFYSRNHRWTEGLISAAKVVGIGANFLVDAADKVVGGRAKFEELVVASLEIAGATAQLVVASKVKAARGSERLSALSKASRGVSEATGNVVATAKACAQMVEDSEV
GIKAENLADLLESEMMEMDKAIEEAANRIQDLTKSRTADSGIKLEVNSKLLDSCTTLMQAIRVLVQKSRLLQAEIVAQGKGTASAKEFYKRNHQWTDGFISAAKAVAVAAKFLLTAADKVVSSNGKLEQMVVAAQEIAASTAQLVVASRVKANRNSANLQQLTQASKGVTTATGTVVATVKDCSQLIDENEE
TA--EVIGAELEQEMKRMDEAIQKAVELIDEQKKSRATDSGIRLKVNEGILDSCNQLMAAIVTLVAKSRALQEEIVAAGHDTANPNEFYKRNHQWTEGLLSAARAVGVAATVLVQKADDVVSCRGKLEYLIVASQEIAASTAQLFVSSRVKADRESQRLKELSSASCSVNTCTANIVATVKNAQITLNEQRD
NV--EIIGTLVESELHSMDKAIEETVAKIQDLDKSRAADSGVKLKVNEQILDSCTDLMKCIRKLVQKSRLLQKEIVEQGKGTVSATEFYKRNHQWSEGLISAAKAIAMGANFLLEAADKVVLGNGKFEQLVVASQGIAASTAQLVVASRVKADRNSNNLTELSKASREVTQATANVVATAKSCNHLVEENED
NV--EVIGSLVETELLSMDKAIEEAANRIQDLEKSRAADSGLKLEVNGKILDSCTELMKCIRKLVKKSRLLQAEIVEQGKGTASATEFYKRNHQWSEGLISAAKAVAMGANLLLEAADKVVAGNGKFEQLVVASQGIAASTAQLVVASRVKANRNSNNLTALSEASRDVTQATGSVVATAKNCSQLIEENDD
NS--VDLDKLIEIELKEMDAAIEEAASKLIDLSKSRENDNKIKLEVNEKILEACTALMENIKILNIKSRTLQKEIVSSQKGNATANEFYKRNSQWSDGLISASKSVAKAANCLVEAANNAVSDSGQNFDLIVAAQEISACTVQLVMASKVKANRNSENLTNLTNASRNVTKATGVVVATVKDGNSRQEQQND
AN--ESLGDLVEDELSIMDRAIEEAAKRIQEITESRAAHSGIKLEVNEQILDSCTSLMASIRVLVHKSRKLQAEIVANQGSNGSAKEFYKRNHQWTEGLISAAKSIGLGAKGLLDAANEAVSGEGKLERLIVASHCVAAGTAQLVVASRVKASRNSDNLAELSQASRNVTNATATVVATAKSCAQLVQQTEE
QK--IDLDKLLEIELREMDAAIDDAASKITDLAKAREKDNQTNLEVNGKIVDACTTLMECVKALIQKSRLLQHEIVASQKGNASANEFYRRNSQWSDGLISASKSVAKAANYLVEAANKAISESGKNFELIVAAQEIAACTTQMVIASKVKAERNSQKLSDLTKASRSVTQATGTLVATVKDCNSQLEQQSE
----EVVGSELEEEMRRMDAAIRQAVEKIEEQRKARENNDGLRLEVNERILGTCQALMAAIAILVQRSRELQAEIVAAGRGQGPPQEFYKRNHQWTEGLLSAAKAVGVAARVLVAAADGVVTGSGKFEQLIVAAQEIAASTAQLFVSSRVKADRDSEKMAALSVASKSVNQATAQVVA-------------S
---TEEIGDLVEAEMQSTTDLVDQAASRIEEMKRSREADTGVKLEVNERILDSCTQLMICIKELITKSKSLQQEIVASGRGSATAKEFYKRHHRWTEGLLSAAKLVGVGASHLVDSSDKLMKGKGKFEELMVCSHEIAASTAQLVAASKVKADKGSPNLKGLQVASRNVATATAGVVASAKTGAQMIEEGD-
---ADAISNAVDNEINATAELVADAAARIEEMNKTREKYTGVQLEVNGRILDSCTSLMQAIKVLILKSKDLQEEIVGVGKGTATAKEFYKRHHRWTEGLISAAKAVGWGAKVLVDSADKVVLGKGKFEELVVASNEITASTAQLVAASRVKAHRGSNKLQALHGASKDVVESAAGVVASAKTGAEMIEDKT-
----SELGDLVETEMSETTEAVDKAAAKIEELNKSRQGSSGVRLEVNEQILDSCTELMKAIKQLIIKSKDLQNDIVAQGRGTATAQEFYNRNPRWTEGLVSAAKTVGWGATVLTDAADKVIQGTGKFEELVVCSHEIAASSAQLVAASKVKAEKNSATLKELRAASKGVANATANVVATTKSGQAQIEESDT
---TENLGDLVENELSSMDKAIEEAAAQIEEMSKSRASDSGIKLEVNEKILDACTNLMQAIRVLVQKSRLLQSEIVALGKGSASAKEFYKRNHQWTEGLISAAKSVAQGANFLVTAANKTVAGGARHQDLIVAAQEIAACTAQLVVASRVKAPRGSQNLNALGSASKQVTQATGIVVATAKDCSQRLEDS--
---KIDFDKLLEIELREMDSAIDEAASKIINLAKAREKDDKTNLEVNGKIVDACTTLMECVKVLILKSRVLQQEIVASQKGNASANEFYRRNSQWSDGLISASKSVAKAANYLVDAANNAIESESGKNELIVAAQEIAACTAQMVIASKVKANRNSQNLTDLTKASRNVTQATGTLVATVKDCNSQLEEL--
---SVDLDKLIEVELKEMDAAIEEAASKIIDLSKSRENDNKIKLEVNEKILEACTALMECIKVLILKSRVLQQEIVSSQKGNATANEFYKRNSQWSDGLISASKSVAKAANYLVEAANTAVNNDSGQNELIVAAQEIAACTAQLVIASKVKANRNSQNLDNLTAASRNVTKATGVVVATVKDGNSRQEQQ--
---VEVIGDLVENELTSMDKAIEEAAARIQEMDKSRAADSGVKLEVNGKILDSCTELMMAIRNLVKKSRLLQMEIVAQGKGTASATEFYKRNHQWSEGLISAAKAIAMGANFLLEASDKVVSGNGKFEQLVVASQGIAASTAQLVVASRVKADRNSSNLSALSEASRLVTQATGGVVATAKSCSQLVEDN--
---KVDFDKLVEKELQEMDLAIEDAAAKITDLEKAREKDDKVNLEVNGKIVNACTTLMECIKILIIKSRVLQKEIVSSQKGNASVSEFYRRNTQWSDGLISASKSVAKAANFLVDAANKAIESESGQNELIVAAQEIAACTTQLVIASKVKANRESTNLADLTKASRNVTKATGTVVATVKDCNSQLEQR--
--NAVEKGDIVGEEMAYTTEAIDVATAKIEALRRARENETGTKLEVNERILDSSTKLMNAIRLLIIRASILQKEIVRIKKGISSGKDFYKRNNRWTEGLLSAAKAVGASASNFVEVANEMMEGTCGLEELVVASREISSSTVQLVTASRVKSNPRSENLLQLESASKLVTASTTEVIAAANAGRKVLAEDD-
--NEEQLGTALVAELDVAAKLVEESAARIAALQDARERMTGTELAVHEAILDSCTNLMDFIKQLIITSNKLQKEIVESGRGAGSAEDFYCRNSRWTDGLLSASKAVGLRAQTLTDLADLCVKGQGNFDELIVASREITASTAQLMAASRVKAT-RGENLTKLEVLSKQVNKAAGQVVGASQSGKTSTEKSS-
-LQQGELGDLVEQEMAATSAAVESAAARIEDMNKSRAVDTGIKMEVNERILASCTELMQTIKELILSSKDLQRDIVESGRGAASMKEFYAKNSRWTEGLISASKAVGWGATVMVSKR---------------------TKTAQICTACSR-----------RPEASRRLRR---SSWASTKSGKSQIEETDT
----GDLGDLVNQEMENAARAISAATERLAARARPKSKFDMLDVSVHDSILDATQQIANAIGRLIQAATESQEEIVREGKGSSSVQQFYKRNNRWTEGLISAAKAVAYATSLLIESADGVISGTHSFEQLIVAANEVAAATAQLVAASRVKASLMSKAQQNLELASKAVTDACKALVK--------------
----GDLGDLVESEMQAAAKAIEAATQRLQAMARPKSRFSATDLQVHDAILASALAITNAIGRLIKAATDSQQEIVAQGKGSSTSTAFYKRNHRWTEGLISAAKAVARATSFLIETADGVISGTKTLEQLIVASNEVASATAQLVQASRVKSELMSRTQENLELAAKAVTDACKALVR--------------
----GDLGDLVDKELSKAADAIEAAAARLAKKTKPREGFSTYELRINDVILAAAIAVTNAIAELIKAATESQQEIVREGRGSSSRTAFYKKNNRWTEGLISAAKAVATSTNTLIETADGVISGRNSPEQLIVASNDVAASTAQLVAASRVKATFMSKTQDRLEAASKAVGAACRALVR--------------
----GDLGDLVDSELSKAADAIAAAAARLAKKNKPRDGYSTYELKVHDSILDAAMAITNAITRLIKAATVTQQEIVQAGRGSSSRTAFYKKNNRWTEGLISAAKAVASSTNTLIETSDGVISDRNSPEQLIVASNDVAASTAQLVAASRVKAGFRSQSQENLEQASKAVGAACRALVR--------------
----GDLGDLVDDELTKAANAIDAATERLSKMKKPRDHYSTYELKIHDSILAAAVAVTNAIAQLIKAATASQQEIVREGRGSMSKTQFYKKHNRWTEGLISAAKAVATSTNTLIETADGVISGRNSPEQLIVASNDVAASTAQLVAASRVKASFMSKTQDRLESASKTVTAACRALVR--------------
----GNIEELVDNQLAETAQAIQQAILRLQNAAKPKDDSSPSELQVHDSLLSASIAITEAIARLIKAATASQAEIVAQGRGSSSRGAFYKKHNRWTEGLISAAKAVARATTTLIETADGVVNGTSSFEHLIVACNGVSAATAQLVAASRVKANFASKVQDHLEDAAKAVTEACKALVR--------------
----GEIGELVDREMTMAARAIEEATKRLEAMSRPRQRHTAIEIQVHDAILAASLAITNAIARLIAAATESQSEIVAQGKGSSTAQAFYKRNNRWTEGLISAAKQVAFTTTYLIETADGVINGTHTLEQLIVASNEVAAATAQLVQSSRVKAELMSKTQMKLELAAKAVTEACKALVK--------------
----GNVEELVDQQLRDTAKAIQNAIEKLNALAQPKNMNPPSELQVHDSLLNASISITQAIARLIEASTAAQAEIVAQGKGSSSRAAFYKKNNRWTEGLISAAQAVARATNTLIETADGIINGNYDLEHLIVACNGVAAATAQLVAASRVKANFASKTQDHLEQAAKAVGDACKALVR--------------
-------GISVDSALQGAANAIMAASRKLQEMSRSRTQFSASEIKVHDDILAAALAITAAIAELIRRATASQKEIVAQGRGSSSAQAFYKANSRWTEGLISAARAVADSTTLLIRTSDGLIQGTASLEEVIVASNEVAAATAQLVAASRVKADLRSPTQLALEQAARAVTDACKALVR--------------
----SNLDAMVEDEMERAANTVNSASQFLTNLQNP--NIKGVDVEVHEAILSAAMAVTKAVSILITAATESQREIVSKGRGNQTRTEYYKKNNRWTEGLISASKAVAGATNVLIQTADGVIRDDCQQEQLIVASNEVAASTAQLVAASRVKANFVSKTQDNLETASGSVSRACKELVG--------------
----GDLGDLVDNELSKAADAIAAAAARLAKKSKPKDQYSTYKLEIHDSILDAAIAVTNAIARLIKAATVTQQEIVQAGRGSSSKTAFYKKNNRWTEGLISAAKAVATSTNTLIETADGVLSGRNSPEQLIVASNDVAASTAQLVAASRVKAGFMSKSQESLEQASKAVGSACRTLVR--------------
----GDIGDIVTQEMQNAAKAIEEATQRIQVMARPKSKYDSLDIQVHDAILQATLAITNAIGRLIQAATESQEEIVKEGKGTSTTQQFYKRNNRWTEGLISAAKAVAYATGLLIESADGVISGTHSLEQLIVASNEVSAATAQLVAASRVKASLMSKSQQLLELAAKAVTDACKALVK--------------
----GDLGDLVEQEMLGTAKAIEQATERLKQMARPRSRFSAVDLQVHDSILAAALAITNAIARLIQAATESQQEIVAQGKGSSTSQQFYKKNNRWTEGLISAARSVAFATNLLIESADGVISGTHTMEQLIVASNEVAAATAQLVAASRVKANLMSKTQERLELAAKAVTEACKALVK--------------
----GNIGDIVEQEMLGAAQAIEAATQRLQQMSRPRSRFSAIDLQVHDSILSAAMAITNAIARLIQAATDSQKEIVAQGKGSSSVQQFYKRNNRWTEGLISAAKSVAFATNLLIESADGVLSGTHSLEQLIVASNEVAGATAQLVAASRVKANLMSKTQERLELAAKAVTEACKALVK--------------
----GEIGDIVEKEMLAAANAIQAATERIQAMQADISSRSGLDKNVHDSILEASLAITNAIARLIQAATESQKEIVAQGRGSTSAQQFYKRNNRWTEGLISAAKAVAFATNLLIEAADGVLNGSHSLEQLIVASNEVAAATAQLVAASRVKANFMSKTQERLEIAAKAVTEACKALVR--------------
----GDLGDIVDRELTNAANAIEAAAQRLAKKKKPREGYSTYELRIHDSILEASIAVTTAIAELIKAATASQQEIVREGRGSSSRTAFYKKNNRWTEGLISAAKAVATSTNTLIETADGVISGRNSPEQLIVASNEVAASTAQLVASSRVKATFMSKTQDRLETASRAVGAACRSLVR--------------
----GDIGDIVEQEMLSAARAIEAATQRLQEIAKPRSKYSAVEVQVHDSILAAALAITNAIGRLIQAATASQQEIVAQGKGSSTTQQFYKRNNRWTEGLISAAKAVAFATTLLIQSADGVLSGTHSLEQLIVASNEVAAATAQLVAASRVKASLMSKTQEQLELAAKAVTEACKALVR--------------
----DDIGDIVEQEMMGAAKAIEIATQRLQQMNRPHSRFSAVDIQVHDSILASAMAITNAIARLIQAATESQQEIVAQGKGSSSIQQFYKRNNRWTEGLISAAKSVAFATNLLIESADGVLSGTHSLEQLIVASNEVAASTAQLVAASRVKANLRSKTQERLEAAAKAVTEACKALVR--------------
----DNLEELVNDEMEQTVETINLASKFLNDMKNP--NIYGDNLEIHEMLLTCAKSITDAVAQLIKSSIQSQQEIILK--GGTTKNDFYKKNSRWTEGLISASKAVAGATNVLIHIADGVLKQSNSHEELIVASNEVAASTAQLVAASRVKANFVSQTQDNLEIASSNVSKACKSLVS--------------
----GNLEDIVDRELGAAAAAIEAAAARLSKANRNRD-YDTFELEIHDSILNSAVAITTAITALIKAATDSQREIVAQGKGGGSRTAFYKRNNRWTEGLISAAKAVATSTNLLIETADGVIVGRNKLEQLIVACNDVTASTAQLVAASRVKATFMSKTQDRLEGASKAVNSACRSLVK--------------
----GDLGDVVDAEMNKAAAAIEAATARLAQMKKPKNGYSTFEISIHDSILDAAIAVTNAIAQLIKAATVSQQEIVAQGRGSSSRTAFYKRNNRWTEGLISAAKAVAMSTNMLIETADGVISGHKKLEQLIVVANDVAASTAQLVAASRVKASFMSKSQERLEAASKAVTSACKSLVR--------------
----GDLGELVDREMSNAAKAIEAAAERLAKQNKNRDQYSTYELKIHDEILEAAIAVTNAIAQLIKAATASQQEIVNQGRGSSSKTQFYKKNNRWTEGLISAAKAVATSTNMLIETADGVISGRNSPEQLIVASNDVAASTAQLVAASRVKASFMSKTQERLETCSKAVTNACRSLVR--------------
----GDLGDLVDSELTKAANAIDAAAQRLAKKNKPRDGFSTYELKINDSILDAAVAVTNAIAQLIKAATDSQNEIVREGRGSSSRTAFYKKNNRWTEGLISAAKAVATSTNTLIETADGVISGRNSHEQLIVASNDVAASTAQLVAASRVKASFMSKTQDRLEQASRAVGSACRALVR--------------
----GDLGDIVDQELSKAADAIAAAVARLQKKNKPRDGYTTYELKVHDSILDAAMAITTAIAQLIKAATATQQEIVQAGRGSSSRTAFYKKNNRWTEGLISAAKAVADSTSTLIETADGVLSNRNSPEQLIVASNNVAASTAQLVAASRVKAGFMSKNQENLEQASKAVGAACRALVR--------------
----GDLGDLVEREMAKAADAIAAASAKLGDLKFNQSDPSTTDLQLHEAAIQAAQAVINAIAALIRAATDAQNEIVAQGRGTSSRAQFYKKNNKWTEGLISAAKSVAASTNILIEKADGTLRRTSGLEELIVASNNVAASTAQLVAASRVKATFMSKTQDKLEECSKVVTSACRNLVK--------------
----GDLGEVVDNELHSAAQAIANASSHLNSLSKPD-SLSQIDFEINKAILSAAIAVTNAIALLISAAIETQEEIVSQNKGSGTRAQYYKKHNRWTEGLISAAKAVAGSTKILISTADGVLGNKNSHEELIVASNEVAASTAQLVAASRVKATFMSKSQEKLEVASKTVTSACRVLVN--------------
----GDLGDLVDREMANAANAILAATERLSKKDRSRDSYSTYELRIHDAILGAAIEVTNAIARLIKAATESQQEIVREGKGTSSRTAFYKKNNRWTEGLISAAKAVANSTNLLIETADGVISGRNSPEQLIVASNDVAASTAQLVAASRVKASFMSKTQDRLEAASKAVTQACRALVR--------------
----GNLEDLVKNEMEQTANTVDLASKFLKDLQNP--NIKSGNFEIHETLLSAAMAVTNAVALLIQAATESQREIVSKGMGSQSRTEFYKKNNRWTEGLISASKAIAGATNVLIQTADGVLKEKNSHEQLIVASNEVAASTAQLVAASRVKANFVSHTQDNLEVASSKVSNACKLLVA--------------
----GNLEDLLDRELSQTVKTVDHASQFLTNMKDV--TIYGGDVKVHEALLACAKAITDAVARLIKASVSSQKEIVDRGMGKQSRTEFYKKNNRWTEGLISAAKAVAGATNILIHTSDGVLRQKNSHEELIVASNEVAASTAQLVAASRVKADFVSKSQNTLEDASTSVTSACKALVE--------------
----GNLEDMLRNEMQETAKAVDKASDFLQKLLNP--NIKNGNFEVHEALVNASIAVTNAVALLITAATESQREIVSKTKGSQTRTEFYKKNNRWTEGLISASKAVAGATNILIQTADGVLRSTNSHEQLIVASNEVAASTAQLVAASRVKANFVSKTQDNLESASTKVSSACKALVS--------------
-----NLEDLVNDEMAQTAQTISLASQFLNDMSNA--NIYGGNLEVHEMLLASAKLVTEAVEGLIKASVESQKEIVAK--GKGTQSDFYKKNSRWTEGLISASKAVAGATNVLIHTADGVLKQSNSHEELIVASKEVAASTAQLVAASRVKANFVSKAQDNLEDSSGNVTRACKSLVD--------------
----DNVGELVDREMAHAAETIAKASDHLTDLTNAQ-DISTIDMKINNSILSAAIAVTKAIILLIKTATDCQNEIVNHGRGSSSRTSFYKKHNKWTEGLISASKSVAYSTNSLIQIADGVLSSTSSPEQLIVACNDVAASTAQLVAASRVKASFMSKTQDKLELASKSVTSACRSLVR--------------
----GDIGDIVEREMMNAASAIDAATAKLQALSRPRNKYSAVDLQVHDAILEASLAITRAIAGLIKAATESQQEIVAKGRGSSTNQQFYKKNNRWTEGLISAARAVAFATTMLIEAADGVIMGTHSLEQLIVASNEVSAATAQVVAASRVKAEFMSKTQDRLERAAKAVTEACRALVK--------------
----GNLEDLLNNEMEHTAQTVDSASKFLDKLQNP--NIHGGNLEVHEALLSAAKAVTLAVTLLISAATESQREIVNKGRGSQSRIEFYKKNSRWTEGLISASKAVAGATNVLIQTADGVLSESNSHEQLIVASNEVAASTAQLVAASRVKANFTSKTQENLEVASSKVSTACKSLVS--------------
----GDLESLVNKEMQQTVDAVNSASKYLTDLKNP--NIYGGNIEIHETLISCAKAIIDAVARLIQASVESQREIVEKGKGTQSNTEFYRKNSRWTEGLISAAKAVAGATNILIQISDGVLQKKNTHEELIVASNEVAASTAQLVAASRVKANFMSKSQDALESASGDVTRACKSLVN--------------
----GNLEELVNDEMEQTAAIVSKASNFLNDMANP--SIHGGNLEVHEMILAAAKLVTDAVVNLISASVESQNEIVSKGKGTQTRTEFYKKNNRWTEGLISASKAVAGATNILISTADGVLRDVNSHEQLIVASNEVAASTAQLVAASRVKANFVSKTQDNLEHASSDVTSACKALVS--------------
----GDLGDLVDSELTKAADAIAAAAARLAKKNKPRNGYSTYELKVSDSILDAATAITSAIAELIRATTATQQEIVQAGKGSSSRTAFYKKNNRWTEGLISAAKAVASSTNTLIETADGVLSKRNSPEQLIVASNDVAASTAQLVAASRVKAGFMSKNQDKLEQASKAVGGACRSLVR--------------
----GDPGEIVEQELAKAMKAVEAAAARLVARNKPRDPFAAYEVKVHEAILDAAAAVTSAVAELVRAATAAQNDIVQAGRGASSRTAFYKKNNRWTEGLISAAKAVAAATNTLIETADGVLSGRNSPEQLIVASNDVAASTAQLVAASRVRAGIASRTQEGLETASKAVGAACRALVR--------------
----GKIEDAVDNEMDETAKAVDSATEFLNKLLSAS-Q--SSNFEIHESIVSAALSITKAIQLLIIAATKCQREIVDRNKGNGTRRDFYKKNNRWTEGLVSAAKAVAGATNILIQTADGVLKNENSHEQLIVACNEVAASTAQLVAAARVKANFMSKSQDDLEGSSRTVNTACKSLVA--------------
----GDIGDIVEQEMSSAARAIEMATERLQQMSRPKSRFSAVDLQVHDSILAAAMAITNAISRLIKAATDSQQEIVSQGKGSSTIQQFYKRNNRWTEGLISAAKAVAFATNLLIESADGVLLGTHSLEQLIVASNEVAAATAQLVAASRVKANLMSKTQERLEAAAKAVTEACKALVR--------------
----GEVGDLVEQEMTRAARAIEDATAKIQEMRRPN-SFSATDIQVHQLILNSVLALTNAIGNLIKCATASQQEIVAQGRGSSSKAAFYKKHNRWTEGLITAAKAVAVATNLLVEAADGVISKTHSLEQLIVASNEVSAATAQLVSASRVKSSFMSRTQERLEMAAKAVKDAAAELVK--------------
----MNTSNNAHHELISTAETITKSSQQLR-------------ADVPKPLLSLALAIIDSVLGLIQAAIACQNEIG--SNSTTQLNQFYKKNSRWTEGLISAAKAVGATTNMLITTAGKLTGGEASPEEFIVASKEVAASTVQLVAASRVKTHAHSKAQVKLEDCSKDVGNACKSLVSHVMKG---------
----SETQNDPHAQLVATEKKIVKSSAKLR-------------VKVPQPLVSLSLSIIDAIVALVRAAIECQNEIS--ETTNVSLTQFYKKNSRWTEGLISAARAVGSATNILISTASNL-DKTGSPEEIIVASKEVASSTIQLVAASRVKTLPHSKAQSGLEECSKAVTTACRMLVAHVME----------
----DDEMQQTVDTISLASKFLNDLMANPQLYNG-----NGGNIEVDEMLLACAKAITDAVAELIKASIKSQQEII--SKGGTTKSDFYKKNSRWTEGLISASKAVAGATNVLIHTADGVLKQSNSHEELIVASNEVAASTAQLVAASRVKANFVSQAQDNLEIASSNVSKA--------------------
----SNKETNPHSELVATADKIVKSSEHLR-------------VDVPKPLLSLALMIIDAVVALVKAAIQCQNEIA--TTTSIPLNQFYLKNSRWTEGLISAAKAVAGATNVLITTASKLINENTSPEQFIVASKEVAASTIQLVAASRVKTSIHSKAQDKLEHCSKDVTDACRSLGNHVMGM---------
----DQELSKAADAIDAAVQRLAKLKSKPRDGFS------TYELKINDVILEAAIAVTGAIAELIKAATASQQEIVREGRGSASKTAFYKKNNRWTEGLISAAKAVASSTNTLIETADGVISGRNSPEQLIVASNDVAASTAQLVAASRVKATFMSKTQDRLETASKAVGAACRALVRQVQDI---------
----KSNGTNAHQELVATAQNIVKSSQQLR-------------VNVPKPLLALALAIIDAVVALVQAAIQCQNEIE--STTKTPLSQFYKKNSRWTEGLISAAKAVGTATNMLIGTATKLTGGDTSPEEFIVASKEVAASTAQLVAASRVKVNAHSKSQEALESSSKDVNSACKQLVSHVMEG---------
----TGAPTNAHAELVATAEKIVRSSQKLR-------------VNVPKPLLNLALSIIDAVVALVKAAIECQNEIA--QTERTPITEFYKKNSRWTEGLVSAAKAVATATNLLISTAGNLLDSATSPEEFIVASKEVAASTVQLVAASRVKSVAHSKTQDQLEGCSKVVGDACKSLVSHVMGI---------
-----DLDDVAERELLAAAKTIEDAANS--LLAAKSKKKEGDS---AEAILEAAMAITGATSTLVGAATLAQRE--RVEKGRTS-GPLYRKDPTWAEGLISAAKSVAQATKALVDNANK-----GTEEALIATSMAVTAATSQLVSACKSKSDINSPSQHKLSNAAKSVSNATNLLVAAAKAV---------
----DRELTNAANAIEAAAQRLAKLKKKPRDGYS------TYELRIHDSILEASIAVTNAIAELIKAATASQQEIVKEGRGSSSRTAFYKKNNRWTEGLISAAKAVATSTNTLIETADGVISGRNSPEQLIVASNDVAASTAQLVAASRVKATFMSKTQDRLETASKAVGSACRSLVRQVQDI---------
----KNKEQDPHSQLIATADAIVKSSQPLR-------------ITVPKPLLTLALAIIDAVVALVQAAVQCQNEIA--TTTNTPLSQFYKKNSRWTEGLISAAKAVGGATDILISTAGELVDKEGSPERFAVAAKEVAASTIQLVAASRVKSGVHSKAQEKLEECSRAVTEACKELGHTVLVK---------
-----DLDDLAERELMAAAKAIEDAANS--LLAAKAKKKEGEM---QEAILEAAMAITSATSTLVGAATLAQRE--RVEKGRAG-GPLYRRDPTWAEGLISAAKAVAATTRALVDTANK-----GTEEALIATSKAVTAATSQLVAACKSKSDINSPSQHKLSNAAKSVQHATNLLVAAAKAV---------
----GELGDMIEQEMMSAANTIEQATLRLRYSA--------TELKVHDTILEAAMAIMRAIGGLIRASTESQEEIVARGRGTSSAHQFYKKNNRWTEGLISAARAVAFASTMLIETADGVIMSTHSLEQLIVASNEVSSATVQLVAASRVKSEFMSQTQERLERAAKAVTDACRSLVRQVQMI---------
-----DLDDLAERELMAAAKAIEDAAKS--LMVSKNKKKDGDA---AEAILEAAMAITNATSSLVGAATLAQKE--RVEKGRAG-GPLYKKDPTWAEGLISAAKNVAAATRALVDTANK-----GVEEALIATSKAVTAATSQLVSACKSKSDINSPSQHKLSNCAKSVQNATNLLVAAAKAV---------
----DSLGDAVEKQMSEAAMAINDAARRIKTQS-------GIQLDVNESILASAQQLMDLIRILIQKSTVVQQEVVAASRGSASATEFYKKNHRWTEGLISAAKAVGWGATLLVDNADKVVNNMGKFEELMVAAGEVAASTAQLVASSRVKSAANSKSQEELMGASRDVTGATRNLVSAAKNA---------
------VGEELESELGAMERAIEEAAARIAKLWSSKQSHTGVKLEVSEKVLDSCTALMKAIVDLIRKAKVLQEEIVARGKGSATARDFYKRNHRWTEGLLSAAKAVGFGAKLLTDAADNVVKGQAKFEQLTVASQEIAASTAQLVFASRVKAELQSKNLQALAESSKAVSLATGGVVATAKHCAELVEE---
-VSSDELGYFLDKELANTQAAVEKAADLLESM-STRHKLEGGSLEVNEAIISCASAIINAVKLLLEACIEAQEEIVSRGKGSLSRALFYKKNNRWTEGLISASKQVAYATGILIRMADGVLAGTNSSEELIVASNEVASATAQLVSSSRVKSDLMSQSHLNLEEASRKVTSSCKMLVT--------------
-LGQQELGEVVDKEMENASATIDAASELLKAN-VSGPSVD---VQVNNSILGSALAIIDAVKLLINASISSQEEIVNKGRGSHSRSSFYKKNNRWTEGLISAAKAIAYSSNVLIQIADGTLQGNNSNEELIVASREVAASTAQLVAAARVKSDLMSKTESNLEGASKKVNAACRKLVA--------------
-------ARTVDDELDDMDAAIEHAASQIEEMLAARAGDSGVKLEVNGKILDACTTLMAAVKVLVQDARQLQNELGD----QHSRQKMYRKNPQWSEGLISAAKAVVFAAKLLVSSADEAVGGSGRMEGVSAAAHEVAGSTAQLVAASRARAPPQTPALSRLTQASRGVAAVTGALVGAVR-----------
-------TRGVDDELADMDRAIEVAAKQIEDMLAARAGDTGVKLEVNGKILDACTTLMAAVKVLVQDSRKLQNELGD----PKTRQNMYRRNPQWSEGLISASKAVVFAAKLLVSSADEAVGAAGRVEGVSAAAHEVAGSTAQLVAASRAKAPPATPALARLTAASRAVAAATGAVVAAVR-----------
------------------------------------------------------------------------------------------RNSRWVEGLISAARAVGSASSFLVDTSDKALHGDAKLEEIMVCGQEIAASTAQLVSASRVKARLDSPNKAELENDSKSVQV---------------------
----GDLGDLVDNELSKAADAISAAAARLAKLKSPKDGYSTYELRIHDSILDAAIAVTNAIARLIKAATVSQAEIVQAGKGTGDKTAFYKKNNRWTEGLISAAKAVASSTNTLIETADGVLSGRNSPEQLIVASNNVAGSTAQLVAASRVKAGFMSKSQESLEQASKAVGAAC-------------------
---DGEIGELVEQEMTLAARAIEEATKKLEMMSRPKNRQSAMEVQVHDAILSASLAIANAIARLIAAATESQAEIVGQGKGSSSAQAFYKRNNRWTEGLISAAKQVAYSTTLLIETADGVISGTHSLEQLIVASNEVAGATAQLVQASRVKAQLMSRTQVKLEVAAKAVT----------------------
----KDIGDLVNRELENALSAIEQAAMRLDKLKDRPNHSMSPDLLIHDTILEAAIVITNAVSQLIKAATDSQNEIVSQGKGSNTRAAFYKKNNRWTEGLISAAKAVAGSTNILIETADGMINGGNSPEQLIVASNEMAAATAQLVSASRVKAKFMSKTQQTLEDASKAVSQ---------------------
---DGDLGDAVEREMGQAAQAIEAAARRLQELLA---LQSGPDLQVHSAILQSAMAITTAIANLIRCATASQQEIVAQGKGTSSLVAFYKKNNKWTEGLISAAQAVAQATTYLVEMADGLVQGTKSWEQLVVAAQEVGVATTQLVAAARVKAIAYSKTQDRLEAAAQAVREA--------------------
---------------------------------------------------------------------IIQQEFKDS----STSIEFYKPYNRWTDGLLSAAKFVGAGANVMVEIANSIVCGKGKLERLIVISQEIAASTTQLVYATRVKTNSQPEMFNKLQVTSKEVCKCTGNLIACVKKAIE-------
-------TDVIQKEMETTFEAITVAEQKFQGLISKKNSMNNENLQVNSLILDCCSELLLCVSDLVKQSKLIQQEFDDS----PTSVEFYKPYNRWTKGLLSAAKFVGASANVMVEIADSIVCGDVKLERLIVISQEIAASTTQLVYATRVKTTSQSEMFNKLQLTSKEVCKCTGNLIACVKKAIE-------
-----EWLSLVDKEMCETAKSMTSAEERFRQLATTFSTLPGTPLRVHQLILDRCSALIAAIRDLVLKSKMAQQSINEDTVN----SQNYKRHGRWTQGILSAAKSVGACANVLVEVSDQLVSNRHVFERLLVVAQEVSASTTQLFVASRVKLPPDSLHLSELQTAVRLVSQTTGDLVGSVKSAIET------
----NDPNVIAERKLLSAAIAIEQAAKRLAEFKPAEGPRKADELKFDEQIFEAAKAIAAATSALIRSATGAQREIIAKGRAGKT-DAMYHSDGTWNDGLVSAAKQVAASTSELCEAANEVVKGSAQHDRVIVCARNVSSFTVQLLTAAAVRSDSSSQTQ-RLRAAGKAVTDATEQLVEAAKGNN--------
---GESLEETAEKELKAAAAIIEEATAALLNAKKKREQNRIDEAGIDESILEAARAITSATGVLVQCATNVQHELVLAGKVGNK-GNMYRRDPTWARGLISAAQAVAGSVQGLVHSANSSSQGKVDEEQLVASAKGVAAATARLVTASRAKADLNSASNSQLAQAAKQVSNATAALVEAAKQVN--------
---GENLEEIAEKELKAAAQVIEEATAALLRAKKKREESRQDEAGIDEAILEAARAITSATGILVNCATAVQHELVVQGKSKGGSGSVYRRDPTWANGLISAAKAVAGSVQGLVVSANDSTQGKAEEETLVASARGVAAATARLVSASRAKADLNSATNSQLSQAAKQVSNATSQLVEAAKSVG--------
---GENLEEIAEKELKEAAKVIEEATAALLRAKKKREETRAEDAGIDEAILEAARAITSATGILVKCATEVQHELVQQGKAKTG-GAVYRRDPTWARGLISAAQAVAGSVQTLVVSANDSTQGKAEEETLVAAARGVAATTARLVSASRAKADLNSQSNSQLSQAAKQVSNATAQLVEAAKSVG--------
---GENLEEMAEKELKAAAAIIEEATASLLRAKKKREENRLDEAGIDEAILEAARAITSATGILVQCATAVQHELVMAGKVGNKTGNVYRRDPTWARGLISAAQSVAGSVQGLVVSANNTTQGKVEEETLIASARGVAAATARLVTASRAKADLNSQSNSQLAQAAKSVSNATTHLVEAAKSVN--------
----EDPNVIAENELLAAAAAIE-AAARKLADLKPRETPRAEDLNFEEQILEAAKAIATATSALVKAAGAAQKELVSTGKIDFTKGTAYHENAMWSEGLVSAAKGVAAATGSLCDAANTAVQGEASQERLVSSAKQVASSTAQLVVACRVKADANSKTQ-RLNQAASMVKSATDELVKSASEAA--------
---VEDLEALAEDELSACARSIEEATAKLIAARPQSKSKNGDAEGVAATIVDASSAIAKAVAKLVNSAAVAQSKRREDQIAS---GSVYKADPTWSNGLISAAKGVGAATHRLVEAAMKSATGKAEEEELIATARSVAAATALLVSASRAKSGDDYQSQAHLSTAARQVASATSDLVAAAKAAT--------
---QEDLELTAENELNSLAKTIEAATQSLLAARPKTTKKSADSSDIAGIIVDASGSIAQAVAKLVQNAAVSQSRRREEQKSQ---GTYYKQDPTWSNGLISAAKSVGGAVQMMIQAAMKAAQGKAEEEELIATAREVAASTARLVSASRAKSGDDQQSQNQLTLAAKAVTQAISKLLDAAKTAT--------
---LEDLEALAEDELMACARSIEEATARLLASRPESKAKHGDAEGIAATIVDASGSIAKAVAKLVHSAAVAQSKRREDQIAT---GSVYKQDPTWSNGLISAAKSVGAATHRLVEAAIKSAKGNAEEEELIATARAVAAATALLVSASRAKSGDNYQQQSHLSQAAKQVALATQDLVAAAKAAT--------
--------VIAENELLGAAASIDAAAKKLASLRPRKSIDLDESLNFDEMILEAAKSITSATSALVKAASAAQRELIDSGKVSRTPT-SSSDDGQWSEGLISAARLVAAATHSLVESANALVQGLGSEDKLISAAKQVASSTAQLLVACKVKAEGDTTATRRLHQAGTAVIRSTDNLVRAAQQ----------
--------VIAENELLGAAASIEAAAKKLASLRPRRQAETDENLNFDEMILEAAKGIMAASSALVRAANAAQRELVDQGKVARRPL-TSSDDGQWSEGLISAARLVAAATHSLVEAAQHLVQGTGTEETLISTAKQVASSTAQLLIACKVKSDPNSETGRRLQAAGNAVIKSTDKLVQAAQQ----------
--------VIAETELLGAASSIEAAARKLANLQPRRSIEADESLNFDEMILEAAKSITAATSALVRAASAAQRELVAAGKLGERPL-YTSDDGQWSEGLVSAARLVAAATHSLVESANWLVQGQASEEKLISSAKQVASSTAQLLVACKVKAEPDSSSMRGLQVAGNAVKQATDHLVRAAQQ----------
--------VIAENELLGAANSIDAAAKKLASLRPRRSIETDESLNFDEMILEAAKSIAAATSALVKAASAAQRELIDLGKVSRRPL-TSSDDGQWSEGLISAARLVAAATHSLVESANSLVQGVSSEEKLISAAKQVASSTAQLLVACKVKADPDSDSTKRLQAAGNAVKRATDNLVRAAQQ----------
--------VIAENELLGAAAAIEAAAKKLEQLKPRAKPEADESLNFEEQILEAAKSIAAATSALVKAASAAQRELVAQGKVGAIPA-NALDDGQWSQGLISAARMVAAATNNLCEAANAAVQGHASQEKLISSAKQVAASTAQLLVACKVKADQDSEAMKRLQAAGNAVKRASDNLVKAAQK----------
--------VIAENELLSAANAIEAAARKLALLKPRQGAKADDSLNFEEQILEAAKAIAAATSALVKAAGYAQRELVQQGRVRANPS-PYSEDGQWSMGLISAARKVAAATQGLCESANSVVQGHASQERLIGSAKQVASSTAQLLVACKVKADTFSESMRRLQVAGNAVKRASDNLVKAAKD----------
--------AQAENELLGAASSIEAASLKLAELRPRQTVHEIVEAEFDENILTSAKGIISAVHILVRAATSAQRELAMQGRTESRP--SGSGTYQWSEGLTSASRVVVASVHKLCDAANTLMKGQTTEERLISAAKQVSSSTAQLLVACNVKADPDSQANRRLQAAGQAVRNAAERLVQSAQQ----------
--------VIAEQELLAAASSIEAAAKKLAQLRPRKKPQADESLNFEEQILEAAKSIATATTALVKAASAAQKELVLQGKVGSVPA-MRHDDGQWSQGLISAAQMVARATGNLCEAANQAVQGEASEEKLVTSAKQVASSTAQLLVACKVKADPNSENMKRLQIAGNAVKHASEDLVKAASE----------
--------VIAENELLGAAAAIEAAAKKLEQLKPRAKPEADESLNFEEQILEAAKSIAAATSALVKAASAAQRELVAQGKVSVKYP-Q-----EWGR-----VGTLMSTPTSIANESLSEENGDVCFYERIEGLSQLGSSQGHFLSA---------------QAAGNAVKRASDNLVKAAQK----------
--------AVAENELIGAANSIEAAAVKLSQLRPRQTQKVDDSLTFDEQILAAAKSIATAVQTLVKAASAAQRELVAQGRLEAHPA-FATDDYQWSEGLISASRLVAAAVHQLCEAANALVQGHSSEEKLISAAKQVASSTAHLLVACKVKSDLDSRAMQRLQSAGHAVKTATEHLVMAARS----------
--------IIAENELLGAAASIEAAARKLAELRPKPRPEADDSLNFEEQILEAAKAITAATVALVKSASAAQRELIATGKIFSAGT-DAREDNQWSQGLVSAARTVAAATQTLCEAANAAVQGNASEERLVAGANQVASSTAQLLLACRVKADAHSATQRRLQVAGNAVKRAGENWCEPARP----------
--------TMAENELLNAAASIEMAAKKLEKLKPRQMKSADESLNFEEQIIEAAKSITSATALLIKCATVAQRELVEQGKIKDIKGGSTQEESQWSEGLISAARMVATAVGALCDAANAAVQGNASVERLVSSAKAVAASTAQLLVACQVKADKESKSNARLQAAGNAVKKATDSLVKAAKV----------
--------VIAENELLATCTCNPGLPSTRLAL------QSLGAFTVDGLAWECLKCMHQIPGIKVWGHVLMGPSLWPRGR-GECPR-AAGGIPVGPQRCHVGPKAGTGLSLPCCSCASRVREPMGSQSGAAAPEMVMSSRGCLFCTPAHLGPHQGSSCTLGLQAAGNAVKRASDNLVKAAQK----------
--------NLLEQQMSDAATVVDDASRRIQALHDAQSAAEKETLKVNEKILDQSVQLTALIKQLVATSSVLQQEIVSDE----TSQ------------------GKVW------DTADRTVSGSGKLEEIMVCGQEITASTAQLVSASRVKARPRSEKKAELETCSRQVHT---------------------
--------VVAETELLGAAASIEAAAKKLEQLKPRKPKQADETLDFEEQILEAAKSIAAATSALVKSASAAQRELVAQGKVGSN-LANAADDGQWSQGLISAARRVATATSSLCEAANASVQGHASEEKLISSAKQVAASTAQLLVACKVKADQDSEAMKRLQAAGNAVKRASDNLV---------------
---------IAESELLTAAKSIEAAAQKLSQLKPKKAKEADMSLNFEEQILEAAKSIASATAALVKSASAAQRELVAQGK----------------------AKMVAAATHSLCESANAMVQGHATEERLIASAKEVAGSTAHLLVACRVKANPDSVAMKRLQAAGNAVKRATEVLVKA-------------
---------IAENELLGAAASIDAAAKKLASLRPRRQADVTIELDFDEMILEAAKGIMAASAALVRAANAAQRELIDQGKVARRPL-TSSDDGQWSEGLISAARLVAAATHSLVEAAQNLVRGVGTEEMLISTAKQVAASTAQLLIACKVKSNPNSEAGRRLQAAGNAVIKSTDNLVHAAQQGLEDEEE---
-----------ENELLGAASSIEAAARRLAQLPQAREANE--SLNFDEQILEAAKSIATAVTALVKAASAAQRELVAQGRVDPRP-TLQTDDYQWSEGLISAARMVAAATHSLCESANALVQGHATEEKLISAAKQVAASTAHLLVACKVKADMNSKVMRRLQAAGNAVKTATEHLV---------------
---------IAENELLGAAAAIEAAAKKLEQLKPRAKPKAAPSLS----PVTGPRGLHLCPFSLCLPHSPPLCNLLPTGGR---------------------------------------------------------------------------------------------------------------
---------VAENELLGAASAIEAAAKKLSELKP-RPKAKEETLNFEEQILEAAKSIAAATSALVKAASTAQRELVAQGKVGASRAMAYD-DGQWSQGLISAARMVAAATGSLCEAANEMVQGLASEEKLISSAKQVAASTAQLLVACKVKADPDSEAMRRLQQAGNRVKHA--------------------
------------------------------------------------------------------------------------------DDGQWSQGLISAAQMVARATGNLCEAANQAVQGEASEEKLVTSAKQVASSTAQLLVACKVKADPNSENMKRLQSAGTAVNRATQMLVES-------------
---------------------------------------KGVSMDVNQAIMESAQAIAKATAALVNAAALAQKERVALGKKLATPTKPYAPNQVWSEGLVTAGKAVAEATKEIVTVANNNVTPCEKNEALIASSREVAKATVQLVTAARSKADFDSPTLGKVESASKSVKEAT-------------------
-----------ENELLGAAESIDAAAKKLASLRPRKVEAADASWNFDEVILEAAKSIAAATSALVKAASAAQRELIDSGKVSRRPL-TSSDDGQWSEGLISASRLVAAATHSLVESANALVQGISSEEKLISSAKQVASSTAQLLVSCKVKADPDSASTKRLLAAGNAVKRATDNLV---------------
-----------------------DTRGHIQKMDATKGKDKGRRLEVNTNILGLATKLMDVMEGLISAADAMR-DALENTRGMQSEDEFNMKHSSWFQGLTTAVDAMVEHNPVLTESLRSVVRQQGKHEELQVSARNLSASVAQLAALSRTKSMPHQADINRLSEFKQTVHEILAAV----------------
-----------------------DTLGHIEKLRESRRNDSGRRLEVNTNLLDFAEKMANKLRELMVTAKTVR-KVLEATRGLSSTDEFNAKHQSWFEALTNAVEAMFDGLPLLSEALRSVVRKQGKHEELQVGARNISAMVAQLAALTRTKSMPTQSELNRLAEFKSTVHELLAAV----------------
-----------EQELLAAAASIEAAAKKLGELTLRVAPTADANLSFDAQILEAAKSIAAATKSLVQSARATQRELTEQGKVSLGT---VPADSQWSDGLVSAAKLVAESTSQLCEAANDVVQGEGDAQELIGAAKAVAATTVQLLHAAQVKADAHSENNKRLQRAGQQVKKA--------------------
-------------------SSIEAASAKLAELRPQENTQEIVETEFDDNIIISAKGILHAVHTLMRSASNAQRELAMQGRAAA--GG--TGTYQWSEGLISAARVVVASVHKLCDAANTLMKGQTTEERLISAAKQVSSSTAQLLVACNVRADPDSQANRRLQAAGQAVRNAAERLVQSA------------
-------------------SSIEAASLRLAELRPIQNEEAKEEIEFDENILSAAKSITSACQTLMRAASNAQRELAQQGRADHNRGG--VPDYQWSEGLISAARVVVAAVHQLCEAANALMQGQASEEKLISAAKQVASSTAHLLVACNVKADMDSQAKRRLQAAGQAVKNAAEKLVQSA------------
------------NELLGAAASIDAAAKKLDSLRPRKPQEADETLNFDEMILEAAKSIIAATSALVRAASAAQRELIDQGAVAR-RPAMSSDDGSWSGGLVSAARLVAAATHSLVEAANALVQGAATEERLISSARQVASSTAHLLVACKVKADPGAESTRRLQAAGAEVIRSTDNLV---------------
-------------------------------MLQGKATSNLAPVDVGERLSSNCHATCSALTVLHALKEEDQRAISNLTQGSATAKEFYKRHHRWTEGLLSAAKLVGVGASHLV------------------------------------------------------------------------------
----EDLELSAENELNALTKAIEDATKQLVASRSTTKKAAGVPLDAAGIIVDASSSIAVAVA------KLVQMAAVSQGKRRQEQQRHYKQDPTWSNGLISAAKSVG-GAVQMMINAIKS--TQGKEEELIATAREVAASTARLVSASRAKSG---------------------------------------
---------VAEQELLTAAAAIDSAADKLSVM-QPRAEVQVEDLPFEEQILGAAKSIATATSSLVKAANAAQKEMADT---MPFSGDYYSEEAQWSASMVSAAKLVAFATQNLCEAANATVQGGSADEKLVASAKSVANSTAQLMLACRAKMDAQSPTAKRLLAAGSAVKKAADHLVESAKTALEEAQEA--
DIKQEELGDLVDKEMAATSAAIETATARIEEMLS--------------------------------------------------------------------------------------------------------------------------------------------------------------
---------IAEKELLRAAKAIDAAASKLSNLTPRKEANSDENLNFDEQILDAAKSIALATKLLVQKASAAQKELVLEGRLQVTGV-SKDKTGQFSQGLVSAAQMVARETGSMCEAANDLVKGEASEEKLEAAAQGVSTATGQLLIACKVKAEPDSEAMKRLDQAGAGVRRAADNLVASTAAR---------
----------IEYHLLDSATSIDTAVKKLDTIQPRPYEFDETCWKSDEVILEAVKSIAAATCALVKTASLAQHEIGVD---------------EGVDGLVYAARMVATATHSLVECAREIIEGLSSEDKLISAANQVASCTTQLLVACKVKADPNSDATKRLLASGNVVKRATENVVRAARQAMK-------
----QEPSVAAERELLSAASTIDAVTIKLARLEESAAAAIDESAPVEEQVLHGTQAITAATATLVKAATAAQQELGNGG-GDE---------NSTAADLVAAAKLVAHATQELCDGANASMRGDLSQERLVAAAKSIAKSTAQLIMACKVKMPSNSGPMRRLQAAGSAVKKAAENLVRTVQAAPTDVEDS--
----FDLGDNAERELLAAAKTIEDTANSLLAAKSKEPKKEGDAPDVAEAILEAAMVITSASSTLVGAATLAQRERVEKGRTS-NGGPMYRKYPTWAEGVISAA------TSQLFSASKSYINSQSQ--------HKLTNSTNLLVTAAKVA-----------------------------------------
------------------------------------------------ALIIACRNLVHATLSLMYWAAAAQRELVQQGRLKPIQNPDLESESQWAQGLISAARYVAVGANHLVESAQAFVTMTLKPESLISAAQTTAGYTAQLVIACIAKADPNSQSCLGLRNAGGSVKHAADRLVRIVQM----------
------------------------------------------------AIISSCQHLVHTTLSLMYWAAAAQRELVQQGRMKPVPSGSTESDSQWAQGLISAARFVAMAANCVVESAQAMVSSNPRPEMLISAAETTAGYTAHLVIACMAKADLSSATCQGLRQAGASVRCATEDLVRLVQR----------
-VDPEDPTVIAENELLGATEAIEAAAKKLEQLKPRAPKEADESLNFEEQILEAAKSIAAATSALVKAASAAQRELVAQGKVSTHPQ----------------------------------------------------------------------------------------------------------
--KEEEIQDAATKELVGAANVINNALGKINALEKKKEKQAADQTDISVAILEEAQTLINFTSDLISNATVAQKNITKEETI-KEKKDVYNRDANWEEGLISAAKTVTGCIQYLVKAANDVIHNKPSEAMLVACAKSVAANAMQLQAASMVNLALDDPMRDKLASTIKKITDSQSAFVKTVSQGAD-------
--------------------------------------ETDENLNFDEMILEAAKGIMAASSALVRAANAAQRELVDQGKVARRPL-TSSDDGQWSEGLNFRCPSRQPLLSSLEEAAQHLVQGTGTRRGCISTAKQGASFDRPAVDVRRLSPTPTSKTGRRLQG----------------------------
----------ADSQLRLIAESIAGANRSLADSRPHRNTVVASDSQLQGAVIDAIFPIVISVSSLVESARSRVNELAILGRHFHSAN-FSNRDRSFTDVLVSTARELSTSITALVSAADLVMNGSGSIDTLIRAASGVSSDSARLVAASQVQEHVNSNSQQEVEESSRGVTRATTSLITAAEKI---------
------------------------------------------------AVIIACRNLIHATLSLMYWAAAAQRELVQQGRLKPIPTSDIETESQWAQGLISAARYVAVGANHLVESAQAFVTGSLKPESLISAAQTTAGYTAQLI-----------------------------------------------
---------------------------------------------VTFELLGGAGELGTCVTQLISAAIEAQRAIGPCA-----MSQPYFAVGVWSEGLVSAAREVAEYTSMLRAAAEGLSNGTRPLMDLPASARAVAAATMQLVLAGST-RNPNQAVQLRLHDAGAAVRHVCDKLSAA-------------
----------------------------------------------------------GCSSTLVGAATLAQRERVEKGRTS-NGGPMYRKYPTWAKGLISAAKVVTSSTSQLLSLVNLI------------------------------------------------------------------------
-----NPEVRAKQELLAAATSIEAALMYLSKLLFSLIQTTDAHLNFDAQILEAAKSIAAATKSL--------------------------------------AKLVAELTCQLCEAASDVVQGEGDAQ----------------------------------------------------------------
------------EELNIVLSQINDQINKIEDVRDALK---NCNANENETIGDSTQGLM----EFIQIAGECEKERIREGKEKN--IQTYSNNEVWSEGLLSCAKKIVEWAQYISSA---LMNGE-PDERILAGLKQFKSHCSQLLTAARVSMEDQSPLLQRLEK----------------------------
---------------------------------------------------------------------HSTRELTEQGKSRS-----VLADSLWSDGLVSAAKLAAELACQLCEAA--------------------------------------------SDRAGQQVKKATETLVKAAKDSADN------
------------------------------------------------DLVQATKLLCGAFGDFLTAVNPEQNERRAAGRVGEFSQQPSQHQQQYDDYLVQRAKHVATSTAQLVLCAKTISADCQVQERVIQSATKCAFATSQLVACARVVAPTNNACQQQLTTAATEVSQSVNNLLHDAEHAV--------
------------------------------------------------DLLEATKRLCGAFGDFLTAVNPEQNERRAAGRVGEYSQHPTDSQQQFDDSLVQKAKHVATSTAQLVLCAKTISAECQVQEKVIQSATKCAFATSQLVACARVVVPTNTACQQQLTTAASKVSHSVHNLLQESELAT--------
-----------------------------------------DDVGSGHKLMGAARMLAGAVSDLLRSVEPAQTVLTAAGSIGQASGDLGEIDEKFQETLMNLAKAVANAAAMLVLKAKNVAQDTVLQNQVIAAATQCALSTSQLVACTKVVSPTIPVCQEQLVEAGKLVDRSVETCVKACRSASDD------
-----------------------------------------DEVGSGEDLLRAARTLAGAVSDLLKAVQPTQTVLTAAGSIGQASGDLNETDERFQDVLMSLAKAVANAAAMLVLKAKNVAQDTVLQNRVIAAATQCALSTSQLVACAKVVSPTIPVCQEQLIEAGKLVDRSVENCVRACQAATDD------
------------------------------------------------PLLQAAKGLAGAVSELLRSAQPASNLLQAAGNVGQASGELSDTDPHFQDVLMQLAKAVASAAAALVLKAKSVAQRTGLQTQVIAAATQCALSTSQLVACTKVVAPTIPVCQEQLVEAGRLVAKAVEGCVSASQAATE-------
----------------------------------------------SEKLLGAARDLMDAFSDLLKFAQPDSQEPGAANRVGETQNATGDVDPRFQDALLALAKAVANATAAMVLKAKTVASKTEDQERVIGSATQCALNTSQLVACTKVIAPTIPACQEQLVEAAKLVAKSVENVVDASRQATD-------
-----------------------------------------------GDLVGAARKLCGAFSDFLNTVNPEHEELAAAGRVGDFSQEPTNEVRTFHDHLVQRAKNVATSTAQLVLRAKTISAECALQDKVIHSATQCAYATSQLVACARVVAPTDPACQEQLTSAAKQVARAVEDLLVDAQE----------
------------------------------------------------KLLGAAKRLCGAFSDLLNAAEPGQKEGKAAKTVGEASQEADDMDRMYQETLLALAKAVANATAQLVLKAKSVASTQGLQNKVISSATSCALATSQLVACCKVVAPTSPACQEQLIDAAKGVAHSVEGIVEAAQQ----------
----------------------------------------------GERLLDAARKLCSAFSDLLKATEPETKEPRAASRVGEAIGEEDDSNRELQDMLLALAKAVANTTAALVLKAKNIAADSATQNRVISAATQCALATSQLVACAKVVAPTHSACQTQLMNAVREVTKAVERLVQV-------------
------------------------------------------------SSIRAASS---RLEDMVVTAQPLHRKEGDVGMLGETS---------NQSLLLRDA-----------LRPKDARQGTGLETSVFASFLQ-TLHTEWLLLCLRQLL-------------------AG-------------------
----------------------------------------------------------------------------------------VDTDPRFQDALMALAKAVANATATLVLKAKNVASKAEMQNKVIASATQTALSTSQLVACTKVVAPTHPACQEQLIDAAKLVAKSVEKTVDSAQMATQ-------
----------------------------------------------------------------------------------------------YQDILLSLAKAVANTTAALVLKAKNVAAQCKDQNTIIAAATHCALATSQLVACAKVVAPTNPACREQLTSAARLVAQAVEKLVSSCQQAP--------
----------------------------------------------------------------------------------------------LTDILLSLAKAVANTTAALVLKAKNVAAQCRDQNNIIAAATHCALATSQLVACAKVVAPTSAACREQLTSAARQVAQAVDRLVEACAPAP--------
-----------------------------------------DAPDVTETILEAALATTSATSTLVGAATLSQRERVEKGRTSD-GGPMY-------------------------------------------------------------------------------------------------------
--EPSELELLAEEELLAASADLEAVVKHLNAVD------DYEEPGFGEQLLQAARAIAESTSMLVSQAAST-----------------RRTNVGWSEDVADKAKAMHTAAAQLAEQIKTAEGGQVSEEQLIATAKAVEASDALLQ-----------------------------------------------
----------------------------------------------------SRKAIIDAASRVGEASNDLLRHVMHEGDGEEDSLMLTEEERLYKDQLLSLAKAVANTTASLVVKAKNLATQTLDPEHVIAAATQTGLCTSQLVACTKVLAPTQPSCQQQLSEAAREVSWAVDGVVQASRAA---------
