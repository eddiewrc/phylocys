KPLTPSTYIRNLNVGILRKLSDFIDPQEGWKKLAVAIKKPSGDDRYNQFHIRRFEALLQTGLSPTCELLFDWGTTNCTVGDLVDLLVQIELFAPATLLLPDAVPQ
KTVTPSTYVRCLNFGLIRQLADFIDPQEGWKKLAVDIKKPSGESKYNQLHIRRFEGLIQMGKSPTCELLFDWGTSNCTVGDLVDLLIRNQFLAPASLLLPDAVPK
KPITPSTYVRCLNVGLIRKLSDFIDPQEGWKKLAVAIKKPSGDDRYNQFHIRRFEALVQTGKSPTSELLFDWGTTNCTVGDLVDILIQNELFAPASLLLPGTTP-
NTITSATFIRKLNYGVRRKLSDFLDPQDSWKYVIVSIHKPTGEPRYSQQHLRRFEAVVLQGRSPTMELLNDWGTSNSTVGELVDILMSHRLMAAASILLPDAI-Q
NSVTSASYIRNLSYSLRRRLSDFLDPQDRWMEVLTSIRKPSGEPRYSQLHVRRFEGLAAQGKSPTVELLVDWGTTNSTVGELVCILKSCKLLAAASLLLPGRIET
NSVTSATYIRNLSYSIRRQLSGILDPQERWKEIIISIRKPNGDFRYSQHNVRRFAGLVAQGKSPTEELLADWGTSNCTVGELVDILKSHKLLAAAAIVLPGMIQT
D-VTPNTPVRKLRYSALRALADLLDPQDTWRSIMADISRPCGEPRYTQMHMRRFEACVLQGKSPTMELLFDWGTSDCTVGDLVEILIRHQLFAAVTVLLPDHVEE
KQVTAQTYIRNLPYSVLKSIADDLDTATNWRDFVLKIPRGPNDPRYQLQDIRKFEMIAGRGDSPTTAAIDAWSTSNATVQDLINVLTSLGLISMVNMLKSQVM--
SSVSSSTYVRSLSYSVRRRLSYFLDPQDRWREVIASIRKPSGELRYSQLHVRRFEGLVAQGKSPTLELLVDWGTSNSTVGELVDILRSHKLLAAASVLLPEE-VP
NSVTSATYLRNIGYSLRRQLSDFLDPQERWREVCVSIRKPSGEMRYTQLHLRRFEGVVAQRKSPTMELLNDWGTTNSTVGDLVDILRSLKLFAAASLLLPDTVGS
NSVTSATYIRNLSYSLRRQLSDILDPQERWKEIIVSIRKPNGDFRYSQHHVRRFAGLPAQGKSPTEELLADWGTTNCTVGELVDMLKNNSLLAAVTLLLPDAIPA
KPITPSTYVRCLGYGMVKQLADFIDPQEGWKKLAVDIKNPSGDNRYNQMHLRRFEARQQVGKSPTCELLHDWGTLNCTVSDLVDLLVKNQFLAPARLLLPDAVQA
KSITPSTYLRSIGYKLMMQLADFIDPQEGWKKLAVDIKNPSGENRYNQMHIRRFEGLMKMGKSPTCELLYDWGTSNCTVGDLVELLIKNQFLAPASLLLPDAVRV
EPVTDSTHVRCLSYGLVRRLADFIDPQEGWKKLAVDITNPSGESRYSQVHIRRFEAFVQMGKSPTCELLYDWGTTNCTVADLVDLLIRNQFLAPASLLLPEAVRM
QPVTAATYVRSLRYGLLRQLADLLDPQEGWKRLAAAITDPAGDSRYSQAHIRRFEAFVQMGKSPTCELLYDWGTTNCTVGDLVDLLIRNQFLAPASLLLPEAVGM
HTMTSSTLIRKLNYGIRRSLSDFLDPQDSWKEVLVSIHKPTGEPRYSQQHVRRFEAYVSQGKSPTMELLNDWGTTNCTVGELVDILLSHKFLAPASILLPDRV-Q
KTITSSTYVRCLNVGLIRQLADFIDPQEGWKKLAVDIKKPSGDSRYNQFHIRRFESLFQSGKSPTSELLFDWGTTNCTVGDLVDLLIRNEFLAPARLLLPDSVPE
KPITASTYVRCLSLGLIRKLSDFIDPQEGWKKLAVAIKKPSGDDRYNQFHIRRFEALLQIGKSPTCELLFDWGTTNCTVGDLVDILVQNEFFAPASLLLPDAVPK
KQITSSTYIRKLNFSILRKLSDFLDPDDRWKEVIVLIMKPSGEPRYTQIEVRRFASLAKQGESPTIDFLTNWGTTNATVGELVDILKSNKLLAAAALLLP----E
SSITSETLVRKLSHSTLRNLSDFLDPQDAWKGVLVDIHKDTGEPRYTQLHLRRFERLVTQGKSPTVELLYDWGTTNCTVGELVEILIRHKLLAPARVLLPDIT-R
SSVTSATYVRNISYSLRRKLSDCLDPYETWRDLIVALRKPNGEPRYTQHHVRRFEGLVAQGKSVTVELLNDWGTTNSTVGELVDILRSLNLLAAANLLLPEV-LS
KTVTPSTYVRNLSHGMHRQLADLLDPQEGWKKIAVNIMKPSGDARYSQFHIRRFEGTVQMGKSPTCELLYDWGTTNCTVQDLKDLLQKNGFSAAASLLLPAKNTE
KPITSSMYVRCLNHGLIRQLADFLDPQDAWKTIAVAIKKPTGEPRYNQFHIRRFEGLVHAGKSPTCELLFDWGTTNCKVGDLVDLLIRNQLLAAASLLLPENLPQ
SSVTSATYIRNLGYCLRRKLSDFLDPQDRWKEVLVDIQKPSGEPRYSQLHVRRFEGLVAQGRSPTMELLNDWGTTKSTVGELVDILKSHELLAAANVLLPES-SP
--MDPSVEIRKLPLDVRSHLISLLELQDSWKKLMAIIPQTLSNFKYRSEHFHIIEDSVKKRIFCSEILLREWGTSGPAVGHLKYLLQKAELFRAAAVDILK----
--MYKHVELRKLPVDELCNIVNILEIDNDWKRVMSI------SDKYNNEHIRLIEEAKASNQKCTEILID---TSGPTLDTFKNILIKAQIFR-AADMLNE----
--MERNTELRKLPMGSLYNIINILEINDSWQKVMAW------SDKYNSEHLRMIQDAKISKRTCSEILFD---TSGPTVATLLDVLVKAEIYR-AANILGE----
KSITSNSFIRQLPMSAYRNLENLLDPGEMWTQLMENIPQYLDDSRYTVQDGLNIRTSQLKGRSPTVLLLEDWGTQNPQIRHLIEVLRESELYAAASVNVLK----
NQVTAETYIRKLPYSAILKLVNFLEPDQLWKRLLCHIPKQLDGNRYSTSQAQMIENASKPGGYATKIILEEWGTQNASVRHLIKVLCKARLYAAASVTQLK----
-PLDPKMYIRKMQYTTLKMVAMELDPPKGWKDLASLITKGHGQPRYNVT-KMKYMEYMKRDGSPTIALLQDWGTQNTTVQELVEVLLQGGFYRVANMLIPG----
-----------------------------------LLPRRPGELKYNT------LALQRPGGSPTESVLQDWGTTNARVSDLLRVFGEMHHLAAMDALLPG----
KKATMQTYIRNLPYSVLKSIADDLDVATNWRDFVLKIPRSQPEPRYDLQDIRRFEMIAGRGESPTTAALDAWSTTNATVQDLINLLNSLGLIS------------
--FTMASEVRFLPPWAKSQLAHILEVTHGWREIMGRVPSMEPLRKYTSDDIQLVAECARDRREGFEVLLEEWGTSGPTLQDLVNLLEQAKLYRAVDY--------
--FTRQ-----LPYSAVAKLTSLLDPGKQWEKLIVNIPRKLGDPKYDLTYVSMIDEGKKMGSSSTRLILDDWGTQN--IKHLIKVLVISELYAAADY--------
-----------MPITVRLRLSSMLDTNNSWKIVMGAIPEL--DKKFSSEHVGLIEEAKRQKKSAFDILIDEWGTSGPTVCELVKLLAELELYQAADY--------
--MNENTEIRKLPAAAIYKLGQILDTENAWQKVMAMIPSDNDVAKYTPQHIKLIQTAEKQNKSCTEILFDEWGTSGPNLNTLLQILIKAQFYRAAEFV-------
---TADTELRFLPGRARSLLVNLLDAGDGWKKILYLITH-----LFNADHAWVLE--N-TRRSRTDEILKSWSTTGPKIRDLLCLLKEAEQYRAASII-------
--MLGTTEVRKMDASLKNELVNLLNINDGWKSLMATVTVDCDSLKYTNDHLKLIEMAQTQRRFCSEIFLEEWGTSGPNLKILMDLLFKLKLTRAAECI-------
------MEIRKLKPSEMYRLGIILNISDNWKKLMANVPKQENVPKFSSEHIEMIEQAHKHNRNAAEIFLDEWSTMGPTLRLLLDLLIKAELFRAADY--------
-----DMEIRKLRPAELYTLASILSISDSWKKLMSIVAKKGNVPKFSSDHISIIEQMANSKRNAAQIFLDEWSTMEPTLKLLQELLVKAELFRAAD---------
-----DMEIRKLRPGELYTLGSILIISDAWKKLMSSVPKQGNAPKFNSDHIKIIEEAARTKRNAAQIFLDEWSTMGPTLKLLLELLINAELFRAAD---------
---TLDTEIRKLQPIELCTLAHILNQADAWKTLMAAIPSPDNPSLFNHEHFKMIEQIRQQQRSGAEIFLSEWSTLGPTCRSMLDLLVKAELFRAADYL-------
--VCLDMEIRKLRPGELYILSNILSISDSWKKLMAIVPKQENIKKFNNDHISIIEQAYGDKRNAAEVFLDEWSTMGPTLRLLLELLTKAELFRAAD---------
-PVDPRTPLRRLPFRLVKKMAMELDPPANWKDLAAY------------------AGFSMQEISPTRALLNAWGTRNATVQNLLDALRQIGLLNVVDML-------
---SNFVYIYDIPRMEKKRLAALLEVNNKWYELGEK------QMGYSTVELDSTQRCCRNNRSPAEQLLDKWGNYNHTITELFVVLSREKLYNCMELIKRY----
---EGNKYIYHLPFLERSELCKILNQNDKWEELAGT------WMKYDVLTIQNLRR----EKNPTDELLTLWGHYNHTITELFVLLARMQHYQSMVPLLPF----
---DDTLPIRLLPVAVAVELCHHLDTLDVWEQMSNF------VKLYQIEQIRQQQRR---GKSPTNEFLIIWGQYNHTVQSLFALFKKMKLHNAMRLIKDY----
---DNTMAIRLLPLPVRAQLCAHLDALDVWQQLATA------VKLYQVEQISSQKQR---GRSASNEFLNIWGQYNHTVQTLFALFKKLKLHNAMRLIKDY----
---PEENMISFLRRETLQLICDILDTDNTWETIAPY------MPG-QMRDVDGCR-RFSY-QSPTKMLLRI-CSKGYNATHLYQLFAKTKLIRLMRLMRSE----
---DDTLLIRLLPVVARHQLCVHLDSLDVWQQLATA------VKLYQVEQIRHQKDR---GRSPANEFMNIWGQYNHTVQSLFALFKKLKLHHAMRMIKDY----
---DDSIPIRLLPLSVRHQLCAHLDALDVWQVMATA------AKLYQVDQIHSEKQR---GRSASNEFLNIWGQYNHTVLTLFALFKKLKLHNAMRVIKDY----
---ENIKYIYDLPFTERSEFCKIMNQNDKWEELAGA------WMKYDVLTIQNLRK----ERNPTDELLTLWGHYNHTIAELFVLLSKMQHYQSMVPLLRY----
---ESLKYIYHLPFSERSEFCKIMNQNDKWEELAGN------WMKYDVLTIQSLRK----EKNPTDELLTLWGHHNHTITELFVLLSKMQHYQSMVPLLNF----
---DNSMPIRMLPMSVRQALCEHLDSLNVWQQLAAF------VKLYDVQHIQSENSR---GRSPTNEFLNIWGQYNHSVYSLFALFHKMKLHNAMRLIKDH----
---DNSMPIRLLPEPVRQELCEHLDSLDVWQQLAAF------VKLYDVQHIRNQSTR---GRSPSMEFLNIWGQYNHTVYNLFALFHKMKLHNAMRIIKDH----
---SNFVYIYDIPWTEQKRLTCLLDQDGKWVPLALK------RMGYGAVEVEDIQRCCQSGRSPAEQLLKKWGNLNHTITDLFVVMSQEGLFSAMEIIKRF----
---SNFKYIYDIPQMEKKRLADLLDQDARWVGLALR------QMGYSADDVEGIRRCCRSGRSPAESLLVKWGNLNHTIVELFVVFYRENMTPAMEVIKRY----
---SEVVMIPFMKREVLQQICAILDTDNTWETIAPY------MPG-ELRDVEGCK-RYSY-QSPSELLLRI-SSKGYSTTHLYQLFAKTKLIRLMRMMRSQ----
---KSIKFIYQLPYLERVELCKILNQNNKWEELAGV------WMGFNNLEIQNLRK----EQNPTEELLAIWEKHNHTVLELFMLLARMHHYQAMLPIKSF----
-------MITFMKNDLLQQICDILDTDNTWETIAPY------MPG-QLRDVEVCK-RFSY-KSPTHLLLQI-CGKGYTTNHLYQLFAKTKLVRLMRIMKSE----
------MYVYLLPFTVRKQLAAILDTDNAWELLAFV------MPD-ENADVRACR-DVSF-ESPTENLLAI-GSKGNKVIQLYNCLGRAKLVRAMKAVRHL----
---DKIEYIYQLPFLTRSELCKILNQNDKWEELAGI------YMKYDVLTIQTLRK----EKNPTDELLTLWGHHNHTILELFVLLSRMQHYQSMLPLIPF----
------MFIYDMPFLERKKLCSILDRNNCFEELAAV------HMQFDSETVQKISR----EISPTEKLLDLWSHHNHTVFELFILLYRMKHYQAMTILKPL----
---NKDAYVYLLPFTIRKQLASILDIDNAWELLAFV------MPD-GNIDIRACR-DASF-ESPTENLLAI-GSKGNKVTQLYSCLGRAKLVRAMKAMRHL----
--YTRYTELRRVDDNDIYKLATILDVNGCWRKLMSIIPKRLDGLKYNAQQISLIDGRLTPGQSISQVMIDEWKTSGPTVGVLLQLLVQAEIYSAADF--------
--YLRSTELRRVEDNDIYRLATILDVNACWRKLMSIIPKGLDGLKYSAQDISLIDGRLPPDQSISQVMIDEWKTSGPTVGVLLQLLVQAELYSAADF--------
--YSRSTELRRVEDNDMYRLAKILDENSCWRKLMSIIPKGMDGFKYTAQDMFQIDERLPPDQSKSQMMIDEWKTSGPTVGVLLQLLVQAELFSAADF--------
---HRNMEIRHMA-FHLETLAHILDTSEGWRDFLYLIPRNLDDLKYTGLNESILETE-QKRNLS--KLLEEWGISGPTVDHLLKLLVRANQIRAA----------
--YQRTTEIRKVTPFDLESLTEILE--DDWRTLFYLIPRRIGDVKYNGNQERDLDKHASQERRCALQLLEEWGSSGPTLEHLLKLLVKGNMFRAA----------
--YTRSTEVRKV--FELDELASVLE--DDWRTLFYLIPRKIADVRYNGTHESELDDYGRIQNTARARLLEEWGTSGPTLGHLYTLVDKGGMIRAV----------
-GTEPSTLLFDVPPVLMESFCRLMDSGLGWRTLASRILS-------SQLEVRCIEMYAVAGKSPTKELMWSWAQRNKTVEDLLKVLDEMGHARARSLFQ------
-PFPASLPLFDLPPSLLGAFCSILDSCLGWRGLAERISS-------SWLEVRHIEKYMEQGKSGTRELLWSWAQKNKTIGDLLQILQEMGHERAINLIV------
-PVPAGTALLDLPPLLLGRLCALLDGALGWRDLAERLSS-------NWLEVRHFEKYVQQGRSGTRELLWSWAQKNKTVGDLLQVLEDMGHQRAIDCLT------
-ALSAHTLLFDLPPTLLGELCAVLDSCFKYRDIAERLSN-------SWLDVRHIEKYVDQGKSGTRELLWSWAQKNKTIGDLLQILEDMGHYRAIHLIT------
-RLTSHTLLFDLPPALLGELCGILDASMGWRGLAERLSN-------SWLDVRHIEKYVDQGRSGTRELLWSWAQKNKTLGDLLQVLQEMGHQRAIHLIT------
-AESPHTLLFDLPPTLLGELCAVLDSCMGWRGLAERLSN-------SWLDVRLIEKYVDQGKSGTRELLWSCAQKNKTIGDLLQILQEMGHHRAIHLIT------
-GLTWATCLFDVSAAVMEKFCRLMDSSLGWRGLAEHISA-------DWLEFRNVQEHATRGKSRTGELLWSWAQKNKTVGDLLLILQEMDHQRAMSIFT------
--MDPNTFLYDVPPVVIERLCQILDSGLGWRALAVRVVP-------SSLEVRMLERVEAAGRSPTSELLWSWAQENPRLLDLVKLLQEMGHQRALQLLQ------
------VHVNSLNNGCRRRIAMYLNPENDWCGYAELL-------NFSQPEIENMK----RHKSPTEEMLHLWSTRNPKVGNLISFLCKLERFD------------
------VPLHALNMSVRSKLGTYLDPENDYQGLAEVI-------GFTFQDITNFQ----RHSKPTQEMLYQWGTRPPTVDNLIKHLQTIGRSD------------
------VPLKILRATSTKQLSQRLDVENDWYGLAERT-------GYMTKEMRKFE----QHESPTTALLNDWSTRLPTVDTLLKYLVEIQRPD------------
------MYIYELPFEINKELCRLLDNDDDWKELAGI------HMKYSAFEVNEIEQATRQTSSPTDQLFKKWGQLNHRVEELFILLHRMKHIPALRTLVP-----
------MYIYELPPEINKELCRLLDNFDYWEELAGN------YMMYTAMDVIEIKEARHQGISPTEYLLECWGQKNHRIEELFVLLYRMKHIPAMRRLTG-----
-----NHFIYDLPYSITRQICCNLDVDRAWEKLASEI-------GYTIQEVRVFEMFQHHSGSPCKDVLSDWGSKNATAKDLYLILQRIGRRR------------
------KYIYELPYIPQRDLANILDDNNQWKEL--------GDFQYNVT---RLNEFGRNTQSPTMAMFSDWGQKNHTIVELFVLLSKMQHYRALEILMPY----
-----AAPLVALNYALRRRLSLLLNPGADWASLAEA-------LGYEFLEIRAFEAH----ADPTAQLLHDWRRPPATAGALLDALRRLHRHD------------
-----SLPLAALNVRVRRRLALFLNVRADWSALAEE-------MGFEYLEIRQLETR----PDPTGSLLDAWSRPGASVGRLLELLEALGRDD------------
-----TIPLIALNVSVRKKLGLYLNPRADWMAVAEA-------MDFTYLEIKNYEAC----KSPTKVVLEDWARAEATVGKLLSILAEVERKD------------
-----AVPMVALNYGVRRRLGLYLNPRADWTALAEA-------LGCSFLEIRRLERL----PDPTAALLEEWGRGGATVGQLLAVLRRLGRHD------------
-----SVPMVALNYGVRRRLGLYLNPRADWTALAEK-------LGHDYLEIRRLEAL----PDPTAALLEEWSRGGATVGQLLELLRQLGRHD------------
-----TIPARHLGVQTRKKLALWLNP-TNWEELADE-------MGFEYIEIENFKMK----DSPMYEVLHV-LKENATIGHLI-MLKGIERFD------------
-----VVPLVALNMSVRKKLGLYLNPRADWMAVAEE-------MGFNYLEIKNYEGT----KSPTESVLEDWARTDATVGKLLSILEEVERRD------------
-----AVPLVALNLSVRRWLGLYLNPRADWAVLAEE-------LGYEYLEIKSLEAQ----PDPTARLLDDWARGGATVGRLLALLRALGRHD------------
-----SLPLAALNVRVRRRLSLFLNARADWTVLAEE-------MGFEYLEIRQFEAR----PDPTGSLLDAWGRPGASVGRLLELLSTLGRED------------
-----SLPLAALNVRVRRRLSLFLNARADWTVLAEE-------MGFEYLEIRHLETR----PDPTGSLLDAWGRSGATVGRLIEMLAALGRED------------
-----SLPLTALNVRVRRRLSLFLNAKADWTVLAEE-------MGFDYLEIRQLESR----LDPTGSLLDAWGRPGASVGQLLELLAKLDRND------------
-----SLPLTALNMRVRRRLSLFLNVRADWTTLAEE-------MDFEYLEIRQLETF----ADPTGRLLATWGRPGASVGRLLELLAKVGRED------------
-----DVPLAALNFRVRSRLSLYLNPPAYWPALAEE-------LGFQYLEILNLQTQ----PDPTRRLLDTWTRERATVGRLLELLHKLERLD------------
-----RVPLLALNMGFRKKVGLYLNPRADWMALAEA-------LGFTFLEIKNYESA----INPTVKVLEDWARTDATVGKLLSILSEVERND------------
-----SIPARYLGVQTRKKLALWLNP-TNWEDLADE-------MGFEYIEIENFKLK----NSPMYEVLHV-LQQNATIGHLI-MLKSIERFD------------
-----QLQLSLPHCGRTSPLNPLVFPHAGWFLVHEK-------LGHDYLEIRRLEAL----PDPTAALLEEWSRGGATVGQLLELLRQLGRHD------------
-----WVPLAALNMRVRRRLSLFLNVQADWTTLAEE-------LGFEYLEIRQLEAY----PNPTGRLLDAWGRPDASVSRLLELLAKLGRQD------------
-----SLPLAALNVRVRHRLSLFLNVRADWTGLAEE-------MNFEYLEIRRLETH----PDPTRSLLDDWGRPGASVGRLLELLAKLGRDD------------
-----TIPLIALNMTVRKKLGLHLNPRADWMAVAEA-------MGFTYLEIKNYEAF----KNPTKSVLEDWARTDATVGKLLSILEEVERND------------
-----SIPLTALNFTVRKKLALYLNPKADWTALAEA-------MGFEYLEIKNYEQY----QNPTKMILENWGTTQATVGKLLEMLTAMERVD------------
-----KIPLVALNMSVRKKLGLYLNPKADWMALAEA-------MGFNYLEIKNYEVS----KNPTCTVLEDWARTDTSVGKLLSMLSELDRED------------
-----TVPLVALNVRVRKQLGLYLNPRPDWMAVAEA-------MGCTYLEIKNYESC----VNPTKAVLEDWARSDATVGKLLSILSEVERKD------------
-----TVPLIALNVSVRKKLELYLNPRADWMVVAET-------MDFNYLEIQNYEAT----NNPTRKVLEGWARTDASVGRLLSVLTEVERKD------------
-----TVPLVALNVTVRRKLGLYLNPKADWMTVAEE-------MGFTYLEIKNYEAS----KNPTKTVLEDWARKDASVGKLLSILTEVERKD------------
-----SIPLVALNYNVRHRLSLYLNPNAGWTQLAEE-------MGYDYLEIKNFERF----PDCTMKLLEDWKKFRATVGGLLEMLKKMERND------------
-----TIPLIALNMSVRKKLGLYLNPRSDWMAVAEE-------MGFTYLEIQNYEAS----RSPTKAVLEGWARTDATVGKLLSILTQVERKD------------
-----AIPVTALNCSFRKKLGLYLNPTADWRTVAEM-------MDFTYLEIKNFEKR----EYPFERVLTEWTRPEATVANLLSFLEKAERKD------------
-----NIPLVALNVNVRKKLGLFLNPRADWMSIAEI-------MGFSYLEITNYENC----QNPTRKILEDWARPDAKVGKLLSILEEADRKD------------
-----SVSLVALNLKVRNRLSLFLNVKADWTALAEE-------MGFDYLEIKNYEAQ----NNPTLKLLEDWVKPKVTVGTLLDLLKKIERED------------
-----TVPLTALNVTVRTRLGLFLNPRSDWKALAEK-------VDFGYLEIKNFEAR----LNPTAVVLDEWARKDATVGKLLGILEKLERND------------
-----SVPLTALNVTVRKKLGLYLNPKADWTAVAEA-------MDFTYLEIKNYETV----RSPMAGVLDEWARKDASVGKLLSILLKLERDD------------
-----NIPLRALNINVRKRLGLFLNPRSDWMSVAEN-------MGFSYLEIKNYEDC----LDPTRRILEDWARPGAKVGKLLSILDNVDRKD------------
-----SIPLVALNYTVRHRLCLYLNPNADWTRLAEE-------MGYDYLEIRNFDRY----PDSTMKLLEDWKKFRATVGGLLEMLKKMERND------------
-----SIPAIALNYSVRKRLALYLNPSADWTEIAEK-------MEFTYLEIKNYEKR----ENPTRKLLEEWTRAGATVGKLLSFLEQAERKD------------
--LSAHTLLFDMPPTLLGEFCAVLDSCLGWRGLAERLSS-------SWLDVRHIEKYVDQGKSGTRELLWSWAQKNKTIGDLLQVLQEMGHHRAIHLIANY----
--LSPQLLLFDLPPALLGELCGILDSCLGWRGLAERLSN-------SWLDVRHIEKYVNQGKSGTRELLWSWAQKNKTIGDLLEVLQDMGHQRAIHLIINYGV--
-SSTSSLPLAALNMRVRRRLSLFLNVRADWTALAEE-------MDFEYLEIRQLETQ----ADPTGRLLDAWGRPGASVGRLLELLTKLGRDDVLLELGP-----
-SFMFSIPLVALNVGVRRRLSLFLNPRADWTLLAEE-------MGFEYLEIRELETR----PDPTRSLLDAWGRSGASVGRLLELLALLDREDILKELKS-----
--SLSSLPLAALNVRVRRRLSLFLNVRADWTVLAEA-------MDFEYLEIQQLEKY----ADPTSRLLDDWRRPGASVGRLLELLAKLGRDDVL----------
----ASLPLAALNVRVRRRLSLFLNVRADWTALAEE-------MGFEYLEIRHLEMH----ADPTGKLLDDWGRPGASVGRLLELLTKLGRDDVL----------
-----KIPLVALNMSFRKKLGLYLNPRADWMALAEA-------MGFTYLEIKNYETS----VNPTVMVLEAWARTEATVGKLLSVLSGVERNDVLEDLQP-----
----QARYIYELPFQARDKLCKLLDAGGRWKELGFN------FMNMDHIAVSLMEQAVLRKASPTDELLHKWGEKNGTINQLFVYLYKMKHMQAMLIIKDFVN--
----QVKYIYELPYAQRNKLCRLLDAGGRWKELGFN------YMHMDQISMSLMEQAILRKESPTDELLHKWGEKNGTVNQLFVFLYKMKHMQAMLIIVDFVS--
---ENDVPIYHMPLTIRKHLCGHLDELKVWMQLAEE-------MHLSPSEIQDIKTKLGSGKSPSNELLLIWGHYDHSTTELFNLLHRLKLFPAMRILKQLV---
----QITLISFMKRDILQQICDILDTDNTWETIAAY------MPGIHLKDVEACKKFASFSQSPSKLLLRVWCAKGYTTNHLYQLFAKTKLIRLMRIIKPEVN--
----HSDFVFSMPFAVRKQLSSILDPGNTWETLAAV------MPDINNEDVEACRRA-RDPESPTEILLTIWGSKGNKVIQLYNLLARTRLVRAMEVIHHL----
-------CVFDIPAKTMDEFCRCMDSLWEWMRFASSV------ID-DQTVLRRIRSYERTGVSVTGELMWSWGQKLPTLQELVDRLQKLELYRALGILQQW----
-------RIFDIPAKIMDDFCRCMDCLWEWMRFASSV------ID-DQTVLRRIRSYEKTGVSVTRELMWSWGQKLPTLQKLVERLQKLEMYRALGILQQW----
----TTQYIYQLPSWVLEDFCHKMDSLWDWMQFASYV------MT-DQTQLRKIKSMERQGVSITRELLWWWGMRQATVQQLLDLLYRLELYRAAQVILDW----
----MAGFIYQLPARVLDELCRSLDALCDWMQLASYV------IT-DLTQLQRIKAMERQGVSITRELLWWWGMRQATVQQLLDLLGHLELHRAAHIVRSW----
----MACYIYQLPSWVLDDLCRNIDTLWDWMQFASYV------IT-DLTQLRKIKSMERQGVSITRELLWWWSMRQATVQQLVDLLCHLELYRAAQIVLSW----
----MSCYIYQLPSWVLDDLCRNMDTLWDWMEFASYV------IT-DLTQLRRIKSLERQGASITRELLWWWGMRQATVQQLVDLLCHLELYRAAQIILNW----
----MPCYIHQLPAWVLDDLCRNMDTLWDWMQFASYV------IT-DLTQLRKIKSLERQGVSITRELLWWWSMRQATVPQLVDLLRRLQLYRAAQIVLDW----
----RMMYVRDIPVPARQQLCDLLDDIKNWKHLAGL-------LGFTNDDIAMFQRCMRPGGSPTDEMLTTWDYENHTIEELYLKLYEIKHARCMMFLKDFV---
-------------RSVLQHLCDILDTDNTWETIAPY------MSGIAMRDVEACKRYGSYNQSPSMLLLRIWCSKGFTVVHLYQLFAKTKLIRLMRVMKSEV---
------EYIYEIPSSVRKGLASVLDRSNKWYDLGTK------QMGFTDVELDNLRQCCINNRSTGAALLEKWANYNHSPTELFIVLSREKLYECMDLMKQYV---
---SRALYIHQIPAWVLEDFCQKMDSLNDWMEFASYV------MT-DQTQLRKIKSMEIHGFSITRELLWWWGMRLATVQQLLDLLHRLELYRAARVILD-----
--------------------------AADWRTVAEM-------MDFTYLEIKNFEKR----DYPFEKVLTEWETPEATVANLLSILEKAERKDVISDLK------
------------NAELMVSLSEILDTGDLWIELADM------MPGIERIDVEGCRKRSEHG-SPSAFLLRIWASKGYCVVDLYNLFAMCRLVRCMQLIRNEVD--
----KNVPVHALNMTVISKLGAYLDPEKDYRGLAEVI-------GFTVEDTFHFQKQ----GKPTQDMLYQWGTRPPTVDNLIKHLQTIGRSDDV----------
-----GRFLYELPPSVLWEFCLLMDGLLDWTRFASEVLK-------DQTAVRLAERR----ERRTDWVMNRWENRNGRVWELIDLLERLQLLRPRDVIL------
-----AVLYFNLPASVDCEFCRIMDGLQDWTRFASGILM-------DQNDVRLAERQ----ERRTHWVMVKCSNRNIRVGELIDLLEDLQLLRPRDVIL------
-----QRFLYEVPAWVMCRFYQVMDSLADWHKFASLIVQ-------DQTELRLGERF----HDRTARIMWSWINRNARVADLLHILTSLQL--------------
-----DQFLYDLPPRVLWEFCRVMDALLDWTRFASMVLD-------DQTALRLAQKQ----EKRTDWVMNQWEIRNGRVGELVNLLEGLELLRPRDVIL------
-----QHFLYEVPPWVMCRFYKVMDALADWCQFAALIVR-------DQTELRLCERS----GQRTVSVLWPWINRNGPRDDLGHALSSLNVL----VVQ------
-----REFLYNLPASVMSHFTRVMDSLSDWILFASHAIS-------DQTELRLLEQS----PRRTDILMHKWGCRNGTVGELVQILEGLQWFRPRDIIL------
-----ELFLYELPSAVYWDFCRVMDTLLDWTRFASLVLD-------DQTALRLAEKQ----AHAPDWVMNRWGSRNGRVRDLLHLLEGLELLRPRDLIL------
---------------------------------ASLIVR-------DQTELRLHERT----PARTASIMWPWVNRNARVGDLIQILTDLQLLRARDIIT------
-----PRFLYELPASVMCHFYEIMDALEEWQRFASRIVR-------DQVELRLYERM----EAPTSKVMWAWMNRNARVSDLLQILDDLQLYRAHNVIA------
---------------------------------AAMIVQ-------NLTDLRNLERG----ERRTERVMWHWMNKNATVGDLLCVLNDLKLLRARDVLS------
----------------------------------------------------MEMPS----VVRTASVLWPWINRNARVADLVRILTHLQLLRARDIIT------
-----DISLDALRVSARKKLSLYLNVQSDYNGLAEM-------VGFGFLEIKEFERQ----KNPTDELLTEWNRSDPTIGRLWEFLVELGRLDVL----------
-----QHFLYEVPPWVMCRFYKVMDALSDWCQFAALIVR-------DQTELRLCERS----GQRTASVLWPWINRNARVADLVRILTHLQLLRARDIITA-----
-------FLYKLPPPVLWEFCLVMDGLLDWTRFASEVLG-------DQTAVRLAEKR----ERRTDWVMNQWCNRNGSVGELLELLERLEMLRPRDVILR-----
--YVKDRYIEELPSQQLRLLCEHLDEMQVWTQLAEV-------MQLRPADVKFINQQQELGKSPSWQLIRKWGICKHTVTELFMLLWKNEQHHAMRMLKDFV---
----WQKQLKDVPADVLFELGKKLNPANNWKVLAGQ-------LGYTYEDIAHFDLS---PKTATVDLLHDWSTENATLNILYNKLILIKRPDAAEIVRPY----
------------------------------------------HMNFDVLTIQTLRREILRGNSPTDELLTKWGHQNHTILELFVLLSKMQHYQAMLIIKPFVD--
------KLFRDVPYSLTDHLSRHLNPKGRWKSLAGN-------LGFSNLDIENFNL---QPNSATSSMLYDWGQRESTLYTLYETLVKMKWVTEAEIVKKY----
-----STKLRHLRFDSRRQLGLHLDPEQNWKSLADL-------MGFSNLEILNFENE----RHKTRCVLEEYRQPSATIGNLCMMLDAIGRQDVLTDM-------
--IERSMPATGIGWRTTNILSQYLRPPCDWRDMAEE-------MGFSYLHIQNFAL----ESDPVAKVLSAWCTPGSNVGKLLDIIEKIERHDVLH---------
---PPSPYVHSMPAWVLEDFCQKMDSDSDWMRFASYV-------ITDQTELRKIKCMEKTGISITRELMWWWGVRLATVRQLLDLLQALQLYRAAQVILDWI---
--------LYEIPAHVLEDFCKKMDCLRDWMQFASRIIT-------DQTELRLMKSLEKTGRSLTRELIWWWGVRLATVQNLLDLLQELKLYRAAKVLLD-----
---PGSATLNRLPEPLLRRLSESLDRERGWRRLADVAGSR-GRLRLSCLDLEQCSLKVEPEGSPSLCLLKLMGEKGCTVMELSDFLLAMEHTEVLQLLSPPG---
---LGAVNINRLREPALRGLSDALDRDRGWKKLAELAGTR-GRYRLSCLDLEQCSLKVEPEGSPSRCLLKLLGEKGCTVTELSEFLQAMEHTDVLQLLGPPG---
---PAGSTLNRLPEPLLRRLSESLDRERGWRQLAELAGSR-GRLRLSCLDLEQCSLKVEPEGSPSLCLLKLMGEKGCTVPELSDFLQALEHTEVLQLLNPPG---
---PACATLNRLREPLLRRLSERLDRERGWRRLAELAGSR-GRLRLSCLDLEQCSLKVEPEGSPSLCLLKLMGEKGCTVTELSEFLQAMAHTDVLQLLSPPG---
---FNVLNINSLREPILRKLSEVLDKNKGWRKLAEVVGND-KRFKLSSLELEKCSLKVEPEGSPSRSLLRLMGERGCTVKELVDFLQVLGHTEVVQYFKPPG---
---PWQSSSHGLGKPRRRRLPRRVE-PRSSSTASSSSGNR-PSLPAGCLDLEQCSLKVEPEGSPSLCLLKLLGEKGCTVTELSDFLQTMAHTEVLQLLSPPG---
---ESSLNINFLKESVLKRLCESLDKNKGWRKLGEIVKND-KRFKVSLEDMDMCSLKAKADGSPSRTLLKLIGDQGCIQEELMEFLQIMGHTEALQCLKPPG---
---DKNLGLENISEAALSRLGYMLDN-CGWKQLAKAVAEQ-PQFCYSDREMTDCSLKVSPHGSPGRYLLALLADRGCTLAFLLQCLKKIEHREAVGFLTMNE---
---GCHMCIDALKEDVLAKLSQMLDN-CGWRQLATAAAKH-PRFQCSEKEMTACSLHVNAAGSPARCFLAHLADRRCPLDFLSRCLTEIDHREAAQLISTTE---
---PGSLPLSRLAEPLLRRLSELLDRPKGWRELAQRAGSR-GRVRLSPLDLEQCSLKVEPEGSPSWSLLKLLGDRGCTVVELVEFLQAMEHTEALQCLSYSG---
---FNSLNINSLQEPVLQKLCDLLEQDKGWRKLAHVIASD-SRFKISSQELYQCSLKVMPDGSPARSLLKLMGERGCTAKDLTDFLQTIGQSEAIQLLKPPG---
---FNSLNINLLQEPVLQKVCDLLDQDKGWRKLAGVIASD-SRFKISSQELHQCSLKVLSEGSPSRSLLKLMGERGCTTKDLTEFLQSIGQSNAIQLLKPPG---
------------------------------------MLTV-TYILSSSDDMEMCSLKVELEGSPSLMLLRLMGERGCTTGHLMDYLQTLGNSEALQCLKPPA---
---FNSLNVNSLQEPVLQKLCELLEQDKGWRKLAQAIASD-IRFKISSQELHQCSLKVMPEGSPARSLLKLMGDRGCTTKDLTEFLHGIGQLEAIQLLKPPG---
---DWSLPIGSLGEEVVAQLCELLDT-RGWRKLAEVAGAE-KRFKCSEEELEMCSLKVEPLGSPTQCLLQLLAERECTLRYLQGCLHRMGHTQACQVLSSAD---
---EWKRSIGTLSDEILIKLAKMLDN-CGWRQLANAVSEQ-PKFCCSERELTSCSLQVSPAGSPARTLLALLAERSCSLDFLLHCLRKMEHQEAVQFLTTAE---
---ERSVKVSLLKEPVVRRLCEVLDQNKGWRKLGEIVGND-RRFKVSPDDMEMCSLKVQPEGSPSRTLLKLMGERGCTTGHLMDYLQTVGNAEALQCLKPPA---
--------LSLLKEPLVKKLCEVLDKNKGWRRLGEIVAND-RRFRVSPDDMEMCSLKVQLEGSPSRTLLRLMGDRGCTAGHLLDYLQTLGNSEALQCLKPSA---
--------ISSLKETVLRKLCEVLDKVRGWRRLGEIVNND-RRFEVSSDHMEMCSLRVEVGGSPSLMLLRLMGDRGCTVGHLSDYLQTLGNTEALQCLKPQA---
---E--KSLVSLGEDIVNKLALMLDR-CGWRQLASMVSEH-PKFRFSENELTSCSLQVRPTGSPGRAVLAVLADRFCSLNVLLDCLRKIDHQEAVQYLTSSA---
---SNSLPLQRLPEPLLALLSDSLDRPKGWRELAQVAGSQ-AGLRLSSQELEQCSLQVEPEGSPSRSLLQLMGERGCTVLELTELLQALKHTEALQYLNPPN---
-----------------------------------------------------------------------MGERGCTVIELIELLQALEHTEALQCLSPPG---
---ERSVKISLLKEPLVRRLCAVLDHDKGWRKLGEIVGND-RRFKVSSDDMEMCSLRVQPEGSPSRALLRLMGERGCTTGHLVDYLQTMGNSEALQCLKPPA---
------------------RLCELLDN-RGWRKLAELAGAE-KRFRCSAEELEMCSLKVEPRGSPTQCLLQLLAERDCSLRYLLGCLERMGHQQACRVLCAAD---
--------IGTLGEAALARLSELLDN-RGWRKLAEIAGAE-KRFKCSEEELEMCSLKVEPCGSPSQSLLQLLAERDCSLKYLLCCLAKLGHTEA------GE---
----IDVGVADLQMASRCELACL---PRDWSILAVKL-----------------------SLSRT--LLNEWAIEQASVGNLCRILVELGRCDARD---------
----TDVHISDVPLLARRKLCRMLDPPKDWCLLAMNLGLTELVAKYSSGNGTS-ELDAALQPSPTAALLQEWSGAGSRVGVLMAKLRELGRRDAADFLLKA----
----MDIHASDLNLLTRRKLSRLLDPPKDWCLLAMNLGLPDLVAKYNTNNGSS-K---DFLPSPVHALLREWTTPESTVGILMSKLRELGRRDAADFLLKA----
----GEVAVAEVSLLARRKLCRLLDPPKDWCLLAMNLGLSELAAKYSSGNGTP--PAPLSLPSPTATLLQEWSQPDSTIGVLVAKLRELGRRDAADFLLRA----
----IDVGVAQLQLASRCELAAL---ARDWSILAVKL-----------------------SLSRT--LLAEWALETATVGRLCSVLEELGRQDARD---------
----MDIHVSDLNLLTRRKLSRLLDPPKDWCLLAMNLGLPDLVAKYNTNNGNH-H---DFTSSPSYALLHEWGNPESTVGILMSKLRELGRRDAADFLLKA----
----LDHHVSDINILVRRKLSRLLDPPKDWCLLAMNLGLPDLVAKYNTNNSVQ-K---DFLPSPMCALLQEWSNPESTVSLLMSKLRELGRRDAADFLLKA----
----TGVPVSELPLLARRKLCRMLDPPKDWCLLAMNLGLTELVAKYSSANGAP-HPEPELQPSPTAALLQEWSRRESTVGVLMTKLRELGRRDAADFLLKA----
----PDTHASQLSLLTRQRLGAA---PRDWCLLAVQL----------------------LGKSPI--VLGEWTL-DSTIGHLARALDELGRGDAA----------
----TFTPLRALRVASRRKLSGFLDLENDYSGLAEL-------AGFDYNSIMNMKRQ----KSPTFELLEKWTGRNGTVGNLWTYLFLMERFDVL----------
------------------------DMNDKWEELGGY------YMKIDTATIFKLSKAPMRNHSSTDELLTLWGEQNHTILELFSILNTMQHYRAMIILKPFV---
----NNLPLLMIPYKTRQMLSCLLNTKRDWRGLASL-------VGISSQEAGSIHQC----SNKTDKVLEIWQRNGPTVGQLLEFLQRLDRFDVHDD--------
------EFLYRVPSQVMYRFFHIMDSLTDWKKFASMIIP-------DMTELRLLEQQ--GSRSRTEEVVWAWSNRNAVVGELLSILRSLNLLRALDVFES-----
-------------VRLRQEVALLLNMDHDYRLLAEL-------LGYEYTFIRAFGAT----SNPTLRLLEDWETRDSTVKKLCDCLEEMERYDVLELIQDNV---
---------QQLSFRARHKLAQVLDQESDWRTLVEL-------MGFTYEMVMLLRA----KNSPTMSLLEEWLRNGATLESLIVMMEELENVSALQVLQ------
------TLLRCLRFDTRRQLGLHLDPEQNWQSLADL-------MGFSNLEIINFKEE----RYRTRQVLSEYRQPTATIGNLCLMLEQIGRQDVLTDMEP-----
--------PFRFSKTLRKQLCQCLDPPNDWRMLAQRL------------QVDRYVDYFATKASPTEHILDLWAREATALADLLNHLRLMGRVDAANVLESR----
--------PFRFTRSLRKQLCQCLDPPNDWRMLAQRL------------QVDSNITTCTTFLG--NHTAGVFVRGTSIISQLLNHLRVMGRTDAATILEAQ----
--------NFRLSRQLRKQLCQCLDPPNDWRMLAHAL------------SVDRYINFFATKPSPTDCILDLWARQSNALTELKQIFSDMERFDAVNILDNC----
--------AFKIPLSIRQRICATFDTAKDWQLLAQKL------------HLDRNLGYFAKQRSPSAVILSLWARDTSDLDSLASALEEIGKIHSKSSPKDQ----
--------AFKIPYSIRQRICATFDTPKDWQMLAQKN------------SINRNLSYFATQSSPSAVILNLWARHDGDLDSLACALEEIGRTHTKLSNISE----
--------AFKIPLSIRQKICNSLDAPNDWRLLAQKL------------SMDRYLNYFATKASPTGVILDLWSLDDGDLNTLASALEEMGKSEMLVVMATD----
--------AFRLPDSIRQKICASLDVPCDWRLLARSL------------GFDRYLNYFATKPSPTGVLLDLWACADADLVSLATALEDMGKSEVLVVMTTD----
--------AFKIPLSIRQRISATFDTAKDWQLLAQKL------------HIDRNLCYFACQESPSSVILTLWAQDSGDLDSLASALEEISKVHSPKAVTLS----
--------AFKIPQSIRQRICATFDTAKDWQLLAQKL------------HIDRNLGYFARQTSPSAVILSLWAQEQGDLDSLASALEEIGKVQSKQSTSAD----
--------AFSIPPPIRQKLCSSLDAPHDWRMLAHKL------------NLDRYLNYFATKSSPTGVILDLWAQPDGNLSVLAAVLEEMGRHETVVSLAAE----
--------AFRIPLSIRQKLCGSLDAPNDWRLLAHKL------------HLDRYLNYFATKSSPTGVILDLWAQPDGDLNELAAVLEEMGRHDGSLASMTT----
--------AFKIPLSIRQKICNSLDAPNDWRLLAQKL------------SMDRYLNYFATKASPTGVILDLWSLDDGDLNTLASALEEMGKKEICDNKKAK----
--------PFRFTRSLRKQLCQCLDPPNDWRMLAQML------------RVDRYINYFATKASPTEHILDLWAREPTAVTDLLNHLRVMGRTDAATILEAQ----
--------GFRLPPHIRQKLCLLLDQPNDWRMLAQAL------------TVDRYVQYFATKSSPTEHILNLWAREESAITDLMNIFRVMGRMDAAAVMEKD----
--------RFRLSKDVKRKLCRCLDPPNDWRMLAAHL------------NVDRYLTYFATRPSPTDQILDLWCRDLNALQQLIEICRTMERPDAVAILEQP----
--------AFKIPSSIRERICATFDTAKDWQLLAQKL------------HLDRNLGYFAKQHSPSAVILSLWVCDADDLDSLANALEEIGKIHSCSSPEDQ----
--------AFRLPPAVRASLRGCLDIPNDWRGLAKRL------------QVDRYINYFATKPSPTDHILDLWARDDRAIEELILVLKDMGRSDACLLIESG----
--------AFKIPLSIRQRICTTFDTAKDWQLLAQKL------------HIQRNLSYFTKQKSPSAVILSLWARDSGDLDSLASALEEIGKIHCKSLPQSP----
--------PFRFGKVLRKQLCQCLDPPNDWRMLAQRL------------AMDRYINYFAVKPSPTDQILDLWAREPTSVTDLLNVLRLMGRTDAASLLERE----
--------AFRLQMSVRRQLCQLLDPPNDWRLLAQRL------------SVDRYINYFATKTSPTEHILDLWARESSAVTDLLNILRVMGRQDAATVLERS----
--------AFKIPFLMRQKIITSLDEPADWRILAQKL------------NLDSHLGFFISKSSPTAILLNFWAQPNGNLTQLAAVVAEIGKQDSLLYMMSE----
--------AFKIPLSIRQRISATFDSAKDWQLLAQKL------------HIDRNLSYFACQESPSSVILSLWAQDTGDLDSLASALEEISKDQSSRTAALG----
--------MFSIPEILTAGKLAGISGS---KHLGQNT------------TGTSHLNFFASKPSPTAMILNLWARPNGNLNQLAAAVAEVGKQDSSLFMVSE----
--------AFKIPFLIRQKIISSLDPPADWRTLAQKL------------NLDSHLSIFASKPSHTAMILNLWARPNGNLSQLAAAVAEVGKQDGALF--SE----
--------AFQIPYLIRQKICSSLDAPADWRLLAQRL------------KLDRHMSFFASKSSPTSVILDLWTQHSGNLNQLAAVMAEIGKQEAMIFLVSD----
--------AFKIPFLIRQKICNSLDIPADWRMLAQKL------------NLDRHLNYFASKASPTSVILDLWAQRHGNLNQLAAIMVDIGKQEAMLFLVSE----
--------AFKIPHSIRQRICATFDTPKDWQLLAQKL------------SINRNLSYFATKNSPSAVILDLWARYDGNLDSLACALEEIGRTQNKLSLVTE----
--------AFKIPYSIRQRICATFDTPKDWQLLAQKL------------NIDRNLSYFATQCSPSAVILDLWALHDGDLDSLACALEEIGRTHSTFTT-TE----
---------NEMSYGSQRKISAFLDSSLNWKDLLSM------MPEYNPLQMQQLSQQEARGKSPTISLLSHMASRGRTVGQLYDFCKTLGMNRICELIVQ-----
------IPMTAVGLKCRAILAMRLDVPRDWRGLALL-------AGFTCDDVELLKER---EKSPTASLLELWTRIVATVGHLVQHLMCLERYDVLDDIEAE----
------IPLAVIGLRSLSILSSRLDVTRDWRGLAAL-------AGFDRDSVELMGQR---GESPTEALIDAWAQSRVTLHLLIQNLELLERFDVIDDVMPH----
----SMTFIYQLPFRSMEILVNCLDVDNGWEEVAGM-------LNFTFQEVCFLRQARSRNNSPSAEMLQILSGRGVSLKILKEALQQVGNFRALELLDS-----
---DPGLHLRDLDYETFENLCNLLNPEKNWKTLAGL-------LRLPMQQIRVFEID---KSQATQKLLGYWQTQEASVYKLYHLLLEIHRDDCASLLKK-----
------------------------------------------GFKYSAQNVSQIDEAASPDQSKSQMMIDEWKTSGPTVGVLLQLLVQAELFSAADFVAL-----
-----DMKIISVPTGARRELASLLDPMRDWSILAAKL-------GLG-PWLAEIESACEPGESKTDRVLGEWARSDATVGALLRCLKEMGRDDAIDLLCRS----
-------NAFHLSKPVRTALCQCLDIPNDWKLLATKL------------GVDGYLNFFGIKPSPTDQILDMWAREEGALMRLAGAFQEMGRHDAVTIVQK-----
-------TVFRIPRPTRNKLCICLDRPNDWRLLARKL------------KVDRYINFFATKPSPTEHILDLWARGERALSDLLRIFKEMGRNDVVDLIEK-----
-------DFVQLPHATKRLICGALDPPRDWRLLAKKL------------NTDRYIAYFATKASPTEQILNLWCRPGSSSSSVSHTLKEMGRQD-VLDIIV-----
-------SPFKLSVRVKRQLCAILDPPNDWRALAARL------------GVDRYTTWFATKSSPTEAILELWCRPGATASLAAA-LRQMERHD-AADLLH-----
-------QAFKLPYSIRQKLCSSLDSPNDWRLLAHRL------------GVNSCLEYFEMRGSPTGVVLDLWAQEDGDLDSLASVLEEMGRREAPHPALA-----
-------RPFRFTRSLRKQLCQCLDPPNDWRMLAQRL------------QVDRYINYFATKASPTEHILDLWARPTAVTDLLNH-LRVMGRTD-AATILE-----
-------NAFRIPVSIRQKLCGSLDAPNDWRMLAHKL------------NLDRYLNYFATKSSPTGVILDLWAQPDGNLSRLAQVLEEMGRHDSLVPVVT-----
-------NAFRIPLSIRQKLCGSLDAPNDWRMLAHKL------------NLDRYLNYFATKSSPTGVILDLWAQPDGDLNELATVLEEMGRHDSLASMTA-----
-------YAFKIPFSIRQKICNSLDAPNDWRLLAEKL------------GVDRYLNYFATKASPTGVILDLWAQDDGDLNTLAGALEEMGKSEMMLVMAT-----
-------SAFQIPYLIRQKICSSLDAPADWRMLAQRL------------KLERHMSFLASKASPTSVILDLWAQHSGNINQLATVMADIGKQEAMIFLVS-----
-------SAFKIPFLIRQKIITSLDLPADWRTLAQKL------------HLDSHLSFFASKPSPTAMILNLWARPNGNLSQLAAAVAGLGQPDGGLFTVS-----
--------FVQLPHATKRLICAALDPPRDWRLLAKKLST------------DRYIAYFATKPSPTEQILNLWCRANNSIMSLLLTLKEMGRQDVLDIIVD-----
--------KFRLSKLVKKKLCKCLDPPNDWRMLAAHLSV------------DRYLAYFATKPSPTDQILDLWCRNRNAVCNLIEICRNMGRHDAVSILQE-----
----SKTTLEDIPWLTAKHLSSSLDTNSKWKQLVEH---I---PLSTSSDIRNLEKS----KSPTEALLHFLGRRQTTVAELLI---------------------
--YELSTNLAELPSDLHWRLASSLNFQNVWMKIIGDSTS----SAFNVTEYDRILRT----KNQGSSLLYVWGNHGQTIKDLLLRLQS-----------------
--NLLSTNLSELPFNVHFQLSNSLNCGDIWLKIIDNDFR----DIYSPDELDRISRE----RNPGSALLRYWGNRGQTVCDLLKRLQT-----------------
----MNTNLAELPVRIFKILSEELQKDNIWIKIVEFDED----PIYKSTEIESFLKR----EKCCEQVLRKWGNRGQTVGSLLARLQF-----------------
----METNLAELPVRVFCVLSQELGKDNLWIKVVDFDQD----PIYKQIDIDTFLKR----DNCCDQVLRRWGNRGQTVGDLLARLQL-----------------
----MNTNLADLPVRIFRVLSQELEKDNLWTKIVDYDQD----PIFKQLEIDSFLKR----ENCCDQVLRRWGNRGQTVGDLLARLQY-----------------
--LRLDTNLAELPLPILNQLSSSLNQNETWLKVLDNDKK----NAFSPEEIDKVTRQ----SNPAEAILQRWGNRGQNVQSLIVRLQW-----------------
--YDLSTNLAELPIGLHWRLASSLNFQTVWMKVIGDSTS----SPFSVSEYDRILRS----ENQGSSLLYLWGNRGQNVRHLLLRLQS-----------------
-----------LPVVTTQRLCKLLDSKSEWQKFAKHI----GMERY--------ISYLKSQLSPTAVLLNIWETDEPGVNDLKTIFCAMDRTDCANLLDI-----
KPITQDTRLREICTAKRREMEAMLNARHDWKALARD-------LGYTSTRIASFEQRAEKQTGPTRHLFGDWKREDSTVENLVSSLCNIGRHDVTLLLMP-----
