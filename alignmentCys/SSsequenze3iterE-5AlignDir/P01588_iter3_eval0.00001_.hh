APPRLICDSRVLERYLLEAKEAEKITTGCAEHCSLNEKITVPDTKVNFYAWKRMEVGQQAVEVWQGLALLSEAVLRGQALLVKSSQPWEPLQLHVDKAVSGLRSLTTLLRALGAQKEAISNSDAASAAPLRTITADTFRKLFRVYSNFLRGKLKLYTGEACRTGDR
SPSRLICDNRVLERYILEAKEAENATMGCAEDCSLSENITVPDTKVNFYAWNRMEVSQQAMEVWQGLAVLSEAILRGQALFVNSSQPSGTLRLHVDKAVSGLRSLTSLLRALGAPKEAISIPDAASAAPLQTFTVDTLWKLFRIYSNFLRGKLKLYTGEACRRGDR
APPRLICDSRVLERYILEAREAENVTMGCAQGCSFSENITVPDTKVNFYTWKRMDVGQQALEVWQGLALLSEAILRGQALLANASQPSETPQLHVDKAVSSLRSLTSLLRALGAQKEAMSLPEEASPAPLRTFTVDTLCKLFRIYSNFLRGKLTLYTGEACRRGDR
APARLICDSRVLERYILEAKEAENVTMGCAEGCSLGENITVPDTKVNFHHWKKSEAGRHAVEVWQGLALLSEAMLRSQALLANSSQLPETLQVHVDKAVSGLRSLTSLLRALGVQKEAVSPPEASSAAPLRTVAADTLCKLFRIYSNFLRGKLKLYTGEACRRGDR
ALPRLICDGRVLQRYILEAKEAENITMGCAEVCSLSENITVPDTKVNFYTWKRMEVSQQAAEVWQGLALLSEAVLRGQALLANSSQPSESLQLHLDKAISGLRSLTSLLRVLGAQKEAVSPPDAASAAPLRTFTVDTLSKLFRIYSNFLRGKLKLYTGEACRRGDR
APLHLICDSGVLERYVLEGKEAENVTMGCAESCSLNENITVPDTKVNFYAWKKMEFGQQAVDIWQGLTLLSEAVLRGQALLANSSQPREPLQLHMDRAVSGLRSLTTLLRALGAQKEATSPPDAASAVPLQTITADTFSKLFQVYSNFIRGKLKLYAGEACRTEDR
APPRLICDSRVLERYILEAREAENATMGCAEGCSFSENITVPDTKVNFYAWKRMEVQQQALEVWQGLALLSEAIFRGQALLANASQPCEALRLHVDKAVSGLRSLTSLLRALGAQKEAIPLPDAPSAAPLRIFTVDALSKLFRIYSNFLRGKLTLYTGEACRRGDR
APPRLICDSRVLERYILEAKEGENATMGCAESCSFSENITVPDTKVNFYAWKRMEVQQQAMEVWQGLALLSEAILQGQALLANSSQPSEALQLHVDKAVSGLRSLTSLLRALGAQKEAIPLPDASSATPLRTFAVDTLCKLFRNYSNFLRGKLTLYTGEACRRRDR
SPLRPICDLRVLNHFMKEARDAEVAMKSCGEGCTMSEPVTVPQTRVDFDVWENKNGLEQAEEVQCGLWLLQQGL----------SVTDTALHSHIDNCIRNLLSINAVLRSLNIQ--EY------ATGLEGTWSVTSATDLLQVHVNFLRGKVRLL--QACQQD--
APPRLICDSRVLERYILEAKEAENVTMGCAEGPRLSENITVPDTKVNFYAWKRMEVEEQAIEVWQGLSLLSEAILQAQALLANSSQPPETLQLHIDKAISGLRSLTSLLRVLGAQKELMSPPDTTPPAPLRTLTVDTFCKLFRVYANFLRGKLKLYTGEVCRRGDR
TPARLICDSRVLERYILEAKEAENVTMGCADGCSLSENITVPDTKVNFYAWRRMEVGQQAVEVWQGLALLSEAILRSQALLANSSQPSDTLRLHVDKAVSGLRSLTSLLRAMGVQREAVLPPDAASAAPLRTFTVDTLCKLFRIYSNFLRGKLKLYTGEACRRGDR
GPLRLICDSTVLERYIVEAKDAENVTMSCTEGCSLGENITVPDTKVNFCAWEKMGVGQQAAEVWQGLVLLSEAMLQGQALLANSSQQSETLQLHLDKAVSGLRSLTSLLHLLGAQKAPILPPDAASPAPLRTFPVDTLCKLFQVYSNFLRGKLKLYTGMACRRGDR
APPRLICDSRVLERYILEAREAENVTMGCAEGCSFGENVTVPDTKVNFYSWKRMEVEQQAVEVWQGLALLSEAILQGQALLANSSQPSETLRLHVDKAVSSLRSLTSLLRALGAQKEAISPPDAASAAPLRTFAVDTLCKLFRIYSNFLRGKLKLYTGEACRRGDR
SPQSPFCDSHILERYILEAKEAQNATVRLHPRRNIVLAFSNTSCLINIYFAINSNVGQKAGEVWQGLTLLSEAVLKGQALLANSSQTPAALKLFVDKAVSSLRSLRFLLRGLRTQEETVSLPDAPTAAPFRTFTVGTMDKLFRIYSNFLRGKLKLFLREACQSEER
APPRLICDSRVLERYILEAKEAENITMGCAEGPRFNENFTVPDTKVNFYAWKTMGVEEQAVEVWQGLSLLFEAILRAQAVLANSSQPSEMLQLHVDKAISGLRSLTSLLRALGAQKEAISPPDTTQVIPLRRFTVDTFCKLFRIYSNFLRGKLKLYTGEACRRGDR
SPQSPFCESHILERYILEAKEAQNATVACLEDCSLGENITVPDTRVNFHTWKKMEVGQKAGEVWQGLTLLSEAVLKGQALLANSSQTPVALKLFVDKAVSSLRSLRFLLRGLGTQEEAIFVPNAPTAVPLRTFTVGTMDKLFRIYSNFLRGKLKLFWREACQSQER
SPLRPICDLRVLNHFIQEARDAEVAMRSCREGCGLTQTVSVPQTTVNFEDWEKKNALEQAEEVQTGLWLLQQAL----------SVTNTALNNHIDNTARNLVSINAVLRSLNFQ--EY------ASSLDGTWTVSSATELLQVHVNFLRGKVRLL--PACQQD--
SPLRPICDLRVLNHFIQEARDAEGAMKSCREGCGLSQSVTVPQTTVDFDVWEKKNALEQAQEVQTGLWLLQQSL----------SVTNTAMHSHIDNTIRNLLSINAVLRSLNIQ--EF------ATDLEGTWRVSLATELLQVHVNFLRGKVHLL--QACKPD--
SPLRPICDLRVLNHFIKEAQDAEAAMKTCREGCTLSESVVVPQTTVDFDVWEKKNASAKAEEVQSGLWLLQQAF----------SVTNAALHSHIDNAVRNLLSVNAVLRSLNIQ--EF------GAEIEGTWRASSAAELLQVYVNFLRGKARLL--QACQQD--
SPLRPICDLRVLNHFIKEAQDAEAAMKLCSEGCTLSDSVIVPQTTVEFDVWEKKSALAKAQEVQSGLWLLQEAF----------SVTNTALHSHIDNSVRNLLSVNAVLRSLNIQ--EF------AAEIEGTWRVSTATELLQVHINFLRGKVRLI--QACQQD--
SPLRPICDLRVLNHFIKEARDAEVAMKSCTEGCSLSESVTVPQTRVDFDVWEKKNGLEQAQEVQSGLWLLQQAL----------SVTNTALHSHIDNSIRNLLSINAVLRSLNIQ--EY------TVALEGTWRVSSATDLLQVHVNFLRGKVRLL--QACQQD--
APYKPVCDKTVVDMFIKEAREAETAMDSCNIICQFPEDIMVPETKLNLGEWNKLHVCPQAAEVWNGLILFTKAVPRIADFISDAS-----LKFQVEKIHGDVRSAVHLFKSLNLQDEVQTSQSEA-----KTLPVRTFKKFFSVYTNFLRGKLRLLVMAVCHEASL
SPLRPICDLRVLDHFIKEAWDAEAAMRTCKDDCSIATNVTVPLTRVDFEVWEAMNIEEQAQEVQSGLHMLNEAI----------SNQTEVLQSHIDASIRNIASIRQVLRSLSIP--EY------SGEDKETQKISSISELFQVHVNFLRGKARLL--PVCRQG--
SPLRPICDLRVLDHFIKEAWDAEAAMRACKDACSIATNFTVPLTRVDFDVWEAMNIEERAQEVQSGLHVLNEAI----------SNQTDVLQSHVDASISNIASIRQVLRSLSIP--EY------GGEDKEMQIVSSISELFQVHINFLRGKVRLL--PICHQG--
APYKPVCDKTVLDMFIKEARELETAMDSCNIICQFPEDIMVPETKLNVGEWNKLQTSQQAAEVWNGLVLFTKAVPRITDFISDAS-----LKFQVEKIHSDIRSVVHLFKSLNLQDEAQTSQTEG-----KTLPVRTFKKLFSVYTNFLRGKLRLLVMAVCREASL
SPLRPICDLRFLNHFIQEARDAEVAMKSCREGCDLSESVSVPKTSVNFDEWEKKDATEQAQEVQSGLWLLQQAL----------SMTNSDLTSHIDTTVGNLQSITNVLRSLNFQ--EY------TVGMEGTWTVSSAAELLQVHHNFLRGKV-------------
--------------------------MGCAEGCSFSENITVPDTKVNFYAWKRMAVEQQALEVWQGLALLSEAILRGQALLANSSQTSETLQLHVDRAISGLRSLTSLLRALGSQKEAISPPDATSAIPLRTFTVDTLCK--------------------------
--------SRVLERYLLEAKEAENVTMGCSESCSLNENITVPDTKVNFYAWKRMEVGQQAVEVWQGLALLSEAVLRGQAVLANSSQPFEPLQLHMDKAISGLRSITTLLRALGAQQEAISLPDAASAAPLRTITADTFCKL-------------------------
GPPRLICDSRVLERYILEAKDAENVTMSCMESCSLGENITVPDTKVNFYAWKRMEVGQRAAEVWQGLALLSEAILRGQALLANSSQPSETLRLHVDKAVSGLRSLTSLLRMLGAQKEPILPPDAASPAPLRTFTVDSLCKLFRVYSNFLRGKLKLYTGEACRRGDR
--------PRVLKHFIQEAQDAEVAMRSCREGCGLSQTVSVPQTTVNYDDWEKKDGLEKAQEVQTGLWLLQQAL----DLL-GPSVTNTALNNHIDNTVRNLVSINAVLRSLNFQ--EYTPPTNV-TSLDGTWRVSSATELLQVHVNFLRGKVGLLGAPACQH---
GPARLVCDNRLILKYISEAKDMEKRVSQCQELPPLTQPLPLPMVDFSLLEWKLKTNETKRQEIVCHLALLVDAVTAAQDQV---KQECAAALLG--QLYKKANSFLLLLQTFSWQVSAW-QPDCA---S-WTTPQSHPSVIFLVYRQLVQGKLRFFAKDFCQ----
IPTRLVCDRRLIQKYISDSMELENQVNLCEELPLLQQPVLLPQVGFSLREWMAKTTQVKGQEVLRDLLTLVDGIAATQQEL---KQPCPSVLLQ--QLYGKASFFVLHLRSYGSQ--TS-QPEAA---P-ELTPERNPRKIFQTYRQLLQGKIHFFRRE-C-----
RPMGPICDNRLIQKYIGEAKDMEKRVSECQALPALSCPVVLPLVDFNMRQWKNKSDEIKRREILCNLVLLVGAVTEAQGQV---SQECGAGQLS--QLYRHASSFLLLLQTFGWEEGPW-EPGCS---P-RSIEQTHVTEIFLTYRQLVQGKLRFFAKDSCD----
SPTRLVCDNRLIQKYIVEAKDMEKRVGQCQALPALRCPAVLPLVDFTFLQWKAKSNETKRREILCDLALLVGAAAGAQGQV---SDGCGARQLS--QLYRHANSFFLLLQSFSWEAGHW-EPSCS---P-HSTEQTHISSIFLTYRQLVQGKLRFFAKVLCK----
LTS-PVCDPRLFNKLLRDSAALHSRLSQCSDLSPLPIPVLLPTVDFSLREWRAKTEQTKGQEVLGAVTLLLEGAIAARGQL---NPSCLSSLLG--QLSAQTHSLLGALQGLGTP-----SLPKG-----QTIAHREPTAIFLSFQQLLRGKVRFL----------
SPVAPACDPRLLNKLLRDSHLLHSRLSQCPDVDPLSIPVLLPAVDFSLGEWKTQTEQSKAQDILGAVSLLLEGVMAARGQL---EPSCLSSLLG--QLSGQVRLLLGALQGLGTQ-----LPLQG-----RTTAHKDPNALFLSLQQLLRGKVRFL----------
SPAPPACDPRLLNKLLRDSHVLHGRLSQCPDINPLSTPVLLPAVDFTLGEWKTQTEQTKAQDVLGATTLLLEAVMTARGQV---GPPCLSSLLV--QLSGQVRLLLGALQDLGMQ-----LPPQG-----RTTAHKDPSAIFLNFQQLLRGKVRFL----------
SPAPPACDLRLLNKLLRDSHVLHSRLSQCPDVNPLSTPVLLPAVDFSLGEWRTQTEQTKAQDVLGATTLLLEGVLAARGQL---GPTCLSSLLG--QLSGQVRLLLGALQGLGTQ-----LPPQG-----RTTAHKDPNAIFLSFQQLLRGKVRFL----------
NPAPPACDPRLLNKLLRDSHALHGRLSQCPEVNALSTPILLPAVDFSLGEWKAQTEQSKAQDILGAVTLLLEGVMAARGQL---GPSCLSSLLG--QLSGQVRLLLGALQGLGTQ-----LPPQG-----RTVAHKDPNAMFLSFQQLLRGKVRFL----------
SPAPPACDPRLLNKLLRDSHILHSRLSQCPDFNPLSTPVPLPTVDFSLGEWKTQTEQNKAQDVLGAVTLLLEGVMAARRQV---GPTCLSSLLE--QLSGQVRLLLGALQGLGTQ-----LPPQG-----RTTAHKDPNAIFPEFPT---AALSFL----------
LTS-PVCDPRLFNKLLRDSAALHSRLSQCSDLGPLPIPVLLPTVDFSLREWRAKTERFKCTQLSPTRNSLTKGG----GSP---LPAWEAALVL--SLSGWAGDMISGLGPLCPQ-----SLPKG-----QTAAHREPTAIFLSFQQLLRGKVRFL----------
SPAPPACDLRVLSKLLRDSHVLHSRLSQCPEVHPLPAPVLLPAVDFSLGEWKTQMEETKAQDILGAVTLLLEAVMAARGQL---GPTCLSSLLG--QLSGQVRLLLGALQSLGTQ-----LPPQG-----RTTAHKDPNAIFLSFQHLLRGKVRFL----------
