DTPANCTYLDLLGTWVFQVGSSGSQRDVNCSVMGPQEKKVVVYLQKLDTAYDDLGNSGHFTIIYNQGFEIVLNDYKWFAFFKYKEEGSKVTTYCNETMTGWVHDVLGRNWACFTGKKV
QRGKNKTAPNLTSEIFWQIGRQETKQNREDKKVESTKENVVIHLKKLDTAYDEFGNSGHFTLIYNQGFEIVLNDYKWFAFFKYETKGSKVISYCHETMTGWVHDVLGRNWACFSGKKV
DTPANCSFEEIEGRWKFLMGPSTENSRVNCS---KPVKTWIVSLRFPDLAIDETGRTGFWTLIYNQGYEAVIRNRKFFGFSDFEKSKKKVISYCNVTI-GWSHNDDGGDWACFRGMRF
DTVANCTHEDLLGSWIFHMSDKT----IDCS-KPPVHKVITISLSYNTAIVESDHEIGTYNLIYNQGFEVEVGFHTLFAFFAWDT----HTKYCNKTMYGWAHDNLGNNWACFIAMKQ
DTPANCTYADMLGTWMFSMSQGGNDRTLDCTDPGPVTNNITV--QFSHFSTSSKGYTGYWTLIYNQGFEFRVLNRKFFAFFKWKGEGENATSMCNEIMPGWSHDVLGRDWACFQGKP-
DTPANCTYPDLLGTWVFQVGPVGSQRDINCSVMEPPEKKVVVHLKKLDTAYDDFGNSGHFTIIYNQGFEIVLNDYKWFAFFKYEAKGSRVISYCHETMPGWVHDVLGRNWACFIGKKV
DTPANCTYSDLLGTWVFQVSDGLPDNSINCSVLGPPRKKVIVHLQKLDTATDDFGNSGFFTIIYNQGFEVVLNDYKWFAFFKYKEEGHNVTSYCHETFPGWVHDVLGKNWACFTAKKL
KKNKRKDYYKVLGVDKNAAEDEIKKAYWKRALHRPPEKKVVVHLKKMDTAYDDLGNSGHFTIIYNQGFEIVLNDYKWFAFFKYKQEGSKVTSYCDETMAGWVHDVLGRNWACFTGKKV
DTPANCTYGDSVGTWVLHLGVDGMTKKVDCSKNFKITNNYTVHLLFPDKAVDAAGNVGFWTMVYNQGFEIVLAGRKYFAFSKYVKHGSKVTSMCDQTQLGWAHSTDNSNWQCYYAMH-
DTPSNCTYEDVRGWWTFVEGPRTGTKDVRCDDAGGETRKVRLLLDFPNLATDSDGNVGTWTLIYNQGFEVTLQYRRYFAFSNYTDDGHGVVSYCGSTKVGWSHDVLGHNWACFEGRK-
DTPANCTYEDLLGTWVFQVSKGGHDQTINCSAEATGERSVTVTLEKLSVATDELGNTGFFTLIYNQGFEVVIGGYKWFGFFKYSQKGSEVTSYCDQTLPGWVHDVLGNNWGCFVGKKV
DTPANCTFEDLEGTWVLQVRSGKPTREIDCSDAGPVEKKIIVHLQKLDIAEDDQGNYGFFTIIYNQGFEIVLNSYKWFAFFKYKKQGSTVTSYCNETLPGWVHDVLGRNWACFTGIKV
DTPANCTFEDIAGTWVLYETPRNGDHSIDCNNKENAVNKIQLQLLRPNVAVDQYGNKGHWTLIYNQGFEITVNQRVYFAFSNYAQNGSEVVSYCDKTLAGWSHDTMTHNWACFQGKN-
DTPANCTYEDLVGTWVFQVSKGGHDKTVNCSAEAVGESTVTVTLQKLSVATDELGHTGFFTIIYNQGFEVVINGYKWFGFFKYSQEGPKVTSYCDQTLPGWVHDVLGRNWACFVGKRV
DTPANCTYEDVLGTWVFQVSKGGQDGTVNCSAEATGESTVVVTLEKLSVAKDELGHSGFFTLIYNQGFEVVINGYKWFAFFKYTDDGHKVTSLCDQTLPGWAHDVLGHNWACFVGKKV
DTPANCTYEDLQGSWVFQVGRGQPYRRINCSDPGPVEGEITVTLQKLDVAQDSLGNYGFFTLIYNQGFEVVLNNYKWFAFFKYKEEGSNVTSYCHETLPGWVHDVLGHNWACFTGHKI
DTPANCSYADLLGSWELRVWRAG--RHGNCSA--PVEKTVLVNLQKLDVAQDSLGNFGFFTLIYNQGFEIVLNNYKWFAFFKYKKEGLNVTSYCNETLPGWVHDVLGRNWACFTGQKI
DTPANCTYPDLLGTWVFQVGAGGARSDVNCSALAYTSFKVVVHLKKMDTAYDDFGNSGYFTIIYNQGFEIVLNDYKWFAFFKYETKGIKVISYCHETMIGWVHDVLGRNWACFIGKKM
DTPANCTYPDLLGTWVLEVGPVSSNRDINCSVVGPPEKKVVVHLQKLDTAFDDLGNSGHFTIIYNQGFEIVLNDYKWFAFFKYKQEGSRVTSYCHETMPGWVHDVLGRNWACFTGRKV
DTPANCTYPDLLGTWVFQVGPSGSQRDINCSVMGPPEKKVVVHLKKLDTAYDELGNSGHFTIIYNQGFEIVLKDYKWFAFFKYKEEGRKVTSYCHETMPGWVHDVLGRNWACFVGKKV
DTPANCSFADLEGTWVFQVGRGG--RHINCSEMGPVEKKVKVTLQKLAVAQDDLGNYGFFTIIYNQGFEVVINDYKWFAFFKYKKEGQNVTSYCHETFPGWVHDVLGHNWACFIGQKI
DTPANCSFPDLEGTWEFQIGEGSAARDIDCSQLGKVRTKLTVTMKELYIAEDQYGNLGGFTLIYNQGFEVKILNYLFFGFFKYEQRGSNVTSYCHETFPGWVHDVLGRNWACFVGKKI
DTPANCTYPDLLGTWVFQVGPGGSGRDVNCSAMEPPEKKVAVHLKKMDTAYDDFGNTGYFTIIYNQGFEIVLNDYKWFAFFKYETKGSKVISYCQETMTGWVHDVLGRNWACFVGKKV
DTPANCTFDDLQGTWVFQVSEGRNDNRIDCSKPGPPKDKMVVHLQKLNVAQDDFGNSGIFTIIYNQGFEVVINDYKWFAFFKYKSEAGNVTSYCHETFPGWVHDVLGKNWACFTAKKV
DTPANCTYPDLLGTWVFQVGPRSSRSDINCSVMEATEEKVVVHLKKLDTAYDELGNSGHFTLIYNQGFEIVLNDYKWFAFFKYEVRGHTAISYCHETMTGWVHDVLGRNWACFVGKKV
DTPANCSYEDLVGSWVFQVGKGGQGRNINCSQMGPVQNKVVVHLQKFDLAQDDLGNSGFFTIIYNQGFEVVLNGYKWFAFFKYEKVGGKAVSYCHDTMPGWVHDVLGNNWACFVGKKV
DTPANCTHPELLGTWVFQVGPVGSQRNVNCSVMGPPEKKVVVHLEKLDTAHDDLGNTGHFTIIYNQGFEIVLNDYKWFAFFKYKEEGHTVTSYCNETLPGWVHDVLGRNWACFTGTKV
DTPANCTYEDLLGTWVLQMSKGGHDRSVNCSAEAIGESSWTVTLEKLSVATDQLGNSGFFTLIYNQGFELVINGYKLFAFFKYTEQGSKVISYCDQTLPGWAHDVLGHNWACFTGKKV
DTPANCTYEDLLGTWVFQVSKGGQDKGINCSMMDTIDKSITVRLEKLSVAVDDLGNTGFFTLIYNQGFEVVLNDYKWFGFFKYSEQGSEVTSYCDQTLPGWVHDSLGNNWACFTAKRV
DTPANCSWGQVAGVWEFHIGPGGYDSRLRCS---TAVESFKLSLLFPDVVLDKDGNKGFWTMIYNQGFEVVINQRKYFAFSSYTKEDDVVTSFCNSTFAGWSHDTLGKDWACYYGIQF
DTPANCSWAQVAGLWEFHVGAGGNDNRLKCS---SPVEMFQLNLQFPDVVWDQDGNKGFWTMIYNQGFEVVINQRKYFAFSTFIQDGSVVTSYCNTTFAGWSHDLLGRDWACYYGIRL
DTPANCTYEDLLGTWIFSVSNVGQDKTINCSSTGQAVSTVTVDLQKLSVAVDDLGHTGFFTLIYNQGFEVVINEYKWFGFFKYTQQGSEVVSYCDQTLPGWVHDVLGSNWACFTGKKV
DTPANCTYEDAHGRWKFHIGDYQ----SKCP-EKNSKQSVVISLLYPDIAIDEFGNRGHWTLIYNQGFEVTINHRKWLVIFAYKS----NGEFCHKSMPMWTHDTLIRQWKCFVAEKI
YNPSNCLLNDVQGTWVFQVFEGQPKDNINCSKLGSPKNKVVIHLRGLNVAEDHISSSGFFTIIHKKGFEVVLNDYKWFAFFKYEGNYRHWTVYCNETFPGWVHDVQGKNWACFTAKK-
DTPANCLYEDIRGTWTFQETERSGDSSLSCDELGPVVHTKTFTLSFPDTATDELGNEGTWTMIWNQGFEVNINERSYFAFSYYEGDFTSATSYCDRTFSGWSRDKTIRNWSCFSAQK-
DTPANCTDEDLLGTWVFQVSKGGHDKTVNCSVEATGESTVTVTLEKLSVATDELGNTGFFTLIYNQGFETVLGGYKWFGFFKYTQDGSKVTSYCDQTLPGWVHDVLGRNWACFVGKKV
DTPANCTFDDIRGDWLLYETERTGDAGIDCDDMGPIIHRTEVSLMYPNIAVDEYGNRGTWTMIYNQGFEIKVAGRSYFAFSLWEKTGDTVLSICDKTLTGWSRDITVRNWACYRAQK-
DTPCNCTYDDIIGTWKFFESKHR----VNCS-DTTFRHISIFQLLFPNTVIDSYGNRGTWTLIYNQGFEIMINNRKYFAFSKWEP----DPHHCEQTVPGWSHDKYVRDWSCFVGHKI
N-PANC--EELVGG----A-V-----PY---V-----K--VVHL----LAQ---------TIIYNQGFEVVQNGYK----FK--KVG---IIYCHDTMPGWVHDVLGH---CF-----
DTPANCLFEDIRGTWTFYETERSGDNTLSCDTMGPVVYVKNFTLDFPNTVMDELGHQGTWTLVWNQGFEVNINQRSYFAWSYYEGNFISATSYCDRTFTGWSRDATVRNWSCFFGNK-
--------------------------------MGPPEKNVVVHLQKLDVAHDELGNSGFFTIIYNQGFEVVINDYKWFAFFKYKQEGTNVTSYCHETLPGWVHDVLGRNWACFTGKKI
-----------------------------------------LTLKFPDLATDDEGNEGFWTLIYNQGFEVRIKGRKYFAFSAYEKNGKNVTSHCGKTMPGWTHDVMDRNWACFQGQK-
DTPANCTYPDLLGTWVFQVGPGGSQHEVNCSVMGPPEKKVVVHLKKMDTAYDDLGNSGHFTIIYNQGFEIVLNDYKWFAFFKDVT---------------------------------
HT-AKCTYSELLGTWVFQGSPLGSQHGVNCSVMGPPEKSVVVHLEKVDTAYDNFGNTDHFTIIYSQGFEIVMNDYKWFAFFNNMQ---------------------------------
DTPSNCTYEDIRGWWTFVEGPRTGTKDVRCDDKSAGGETRKVRLLFPNLVTDSDGNVGTWTLIYNQGFEVTLQYRRYFAFSNYTDDGHRVVSYCGSTKVGWSHDVLGHNWACFEGRK-
DIPVSCYFEDVAGTWKFQESGYSTKGPATCENATDFSRQNIIQLLYPNVALDKFGNRGKWTLIYNQGFEVIVNNRKYFAFFKWIKRDNKFISICGKTLPGWQHDILGRHWSCFVGTK-
DTPANCSYMDAIGHWIFHVSRYK----TKCTKQLDVSQTFSMNVQYPNIVTDSYGNMGKWTLIYNQGFEITMNHRKWLIMFAYGPNN---TYTCNKSMPMWTHDTLIRQWHCFTATKV
DTPANCTYEDARGQWIFHVGDYK----SKCSENLNPKQSIIITLLYPDIAVDAFGNRGHWTLIYNQGFEVTISHRKWLVIFAYKENG---EFNCHKSMPTWTHDTLIRQWKCFVAQKI
DLPVHCLYNSTAGVWEFELTD------------IQATRTMKVYLQMPDIAID-KGNTGFFTLVYDQGFEVVVGGNKYWAFFNYTTAGK-IISNCDRTFTGWYHEAKV--KGCYRARRL
DLPVHCMKHQITGKWKMEVSQVPPDSQQAGFDDFKAVDTFEVNLSNDYSVQDKRKTSGQWTMVYDEGFEVEHNGVKYFAFSKYAPNGSDYKSYCGETLIGWYNNLKTGEKGCYRAKKT
DLPVHCVRHQIVGKWQLQFTELTPDNERAGQNTFQQAFTHEVVLTAKNNVVLKKKLQGKWTMVYDEGFEVDFMHYKCFAFSKYKTNYSGAYSYCGETLVGWYQNTKTNERGCYRAQKI
DLPVHCLKHQVAGKWRLYLEKLPPDRAKAYSSNFQPSQDFEVTLGDDYKVKSS-SKSGEWTMVYDEGFEIKLGNMKYFAFSKYEPKGREGVSYCDQTLVGWYTNQDTQERGCYRAEKT
DIPVHCLFHQVFGDWTFYRTS------------LPNVNTIKLRLLAPDVVYD-QGNEGFWTLIYDQGFEVVINKQKYFAFFNYTQQGK-VTSNCGQTFTGWYHDAGI--KGCFRGIRT
DLPVHCLRHQIVGKWKMEVSDVPPDTQHAGWEDFRASDSFELTLGNDYTVTDKRKQSGSWTMVYDEGFEVEHNGVKYFAFSKYEPNGVDYKSLCGETLIGWYNNQKTGEKGCYRAKKA
DIPVHCLYDQIVGTWRFDMGA------------VK---SLHLRFAEPNIVYN-SGQVADWTMIYDEGISVMMDGAKFFAFFPYTVDPI-VTSFCNETVLSTYHDAPY--GGCFKGYKA
------------------------------------------------------------------------------------ESG----YFCNRGRHGWSHDSLLRQWWWFEATKL
-----------------------------------------------------------------------------------EEEGIQVTSYCNETMPGWVHDVLGRNWACFTGSKV
DTPANCTYEDLLGTWVLQVSKGGHDKSVNCSAEGTGESTWIVTLEKLCVAKDNVGNLGFFHPYLQPGF--------------------------------------------------
--------------------------------------------------------RGHFTMVYDQGMEIKVDGLKYFAFFNYTEAHTIVTSHCDQTFVGTYHDNSAKKWGCFKGFKS
--------------------------------------------------------------------------------------EGVSTSNCSRILMGTYHDNSAAKWGCFIGIKV
DTPANCTYEDIQGRWLFQIGDRGHDNTVNCDSVGK-----------------------------------------------------------------------------------
DTPCWCANDQVLGTWKVESTSKN--DRTDCPESLRTKETRMITLLSPNVAVDETGASGTWSQVYSQAIQINIGGVKYLYFLAWEE--STVHSMCYKSQPGWAVKQGMRYRACIRAT--
DTPAWCLGDQVLGVWSLQFTDGNGTDRFECPANIAATTTKRIRLLAPNIAINETGATGTWTMAYTEGLEIRLNGLNYLFYFAWKEEGVMVNSSCWLSAPGWVKKDGIASQACLYAE--
DTPAWCLGDQVLGVWSLQFTDGSGTDRFECPSNITATTVKKIRLVSPNIAINETGATGTWTMAYTQGLEIRLNGFNYLFYFAWQEEGTMVNSSCWLSAPGWVKQDGIAPQACLYAE--
DLPIHCLKNQVTGYWILQRGKIENERKVSCGHLNVVENEEIIK-INPDFTVSGDKGTGKWTLIYDEGWEIDYNNTKYTHFFKYYNQNGNYKTDCDNTLVGWFNSYSVSQRGCSIAKK-
DLPVHCVRSQITGKWLLKEGKVLNNKPVSCGHMGEVEREYEVF-LDEDFTVRGKEGKGRWTLVYDEGWEIEYNGVKYTHFFKYKDDSGQYKSVCSQTLVGWFNHIQKKSRGCSQAIK-
DIPVHCVHDDIANEWKLFITPSIPNRPLNCTRDLTISRVLRVYLEYPNIVRDSLDNSGTWTMVYDQGFEITLKDISLFAFSSFERNTQLIKSKCHETFVGNYRKTSGNSLGCYYAEK-
DLPVHCVKSEIVGKWQIQISHGNSRSMVTCGHEVGVQQTFDIELTKDNKVLGELG-EGTWTMIYDEGWEIDYGGIKFMNFFKYSKKESLFKIDCGKTLVGWYNNYTELNRGCSQG---
DLPAHCLVRHVLGEWEIQEGLLTPDRVDAHGPMTPPNVSASFHALFPDFRIEFDGSSGLWTLVYDEGFEVTPQNRRFFAFFKYQPDGQNAWSYCHTTLVGWWERP-------------
DLPAHCLVRHVLGEWEIQEGLLTPDKIDAHDPMTPPNISPSFHLLHPDFRVDFDGSTGVWTLVYDEGFEVTPQSRRFFAFFKYEPDGRNAWSYCHTTLVGWWDRV-------------
DLPIHATVHDIKGDWTFYLTPAVSGDVSSCGSPSPNKTEVRVSLTDIPGVFDDQRLVGSWTTVYDEGFEVDIGSTRLMGFMKYNPHSHCENSACSRTQIGWYIQQQERVSGCFYAEK-
DLPIHATVHDIKGDWTFYLSPAVPGDVSNCGSPSPNKKEVRVSLTDIPGVFDDRKLVGSWTTVYDEGFEVDVGRMRLMGFMKYNPQNNCENSDCTRTQIGWYIQQAERLSGCFYAEK-
DLPTHVETKNLLGKWKILRTKTSPN-LTTCGSSQPNVSELNVILSDDYAVYDEKRVIGTWTTICDEGFEIKIGNETYAALMHYEPNGKCLDSDCSKIRYGWLDVENEHLHGCFYAER-
DLPTHVEIRNLVGKWKIQRTQTSPK-ITTCGSSQPNTSELYVILSDDFVVYDEKRKIGTWTTICDEGFEIRLGNETYTALMHYEPTGKCTDSECNKIRFGWVDISNTKLYGCFYAER-
DLPVHVEIKDLLGKWKLYKTKTSPE-LLTCGSTQPNDSELNVVLSNDFAVYDERRKIGTWTTIFDQGFEIRIDNETYTAFMHYEPTGKCTDSICDKTRFGWIDIMNKQLHGCFYAEK-
DLPIHVTTADIIGRWEFQYSEPANNWIDGCGSGVPNLEKLDLILTDLLAVKDEGKVVGRWTMVYDEGFEIVINNMTIFGIMKYNNNGNCETTLCSQIQIGWYYLR--KKRGCVSGRK-
DLPIHTLTKDAIGVWKIYETEASSG-FHNCGSSFPNLKEFTVELSDENSVKSKSQPIGNWTLVYDEGLMINIDNKNYFGYFKYSKNNECEDEKCSKIQIGWYSKKVKEF-GCFYAQK-
DQPVHCLREQILGKWEFFITNDANSDTV-CTHKLPTYTKYEANLKDQDHEANGQVIKATWSMLYDQGLIIETDKNRLFTNFKYTLHNDQVSSECDQTMIGVVQDKYNGQLKCFYGHKL
DLPVHCLKHQIIGQWKLQLEN--TTQYIKCGHSQPYLNIFKVILKFKRTQSYQKKLKGNWSVLYDQGIIIQINKIQYFTYFKYYPNENEIHYDCTETLIGWYTKEHQG---CFKAIKI
------------------------------------------------------VPVGTWTMVYDEGFEVRLPNRRFFGFLKYSRKNGNEDSRCSKTHMGWTVEESKHTWGCFYGEKT
------------------------------------------------------I-VGHWTMVYDEGFEVRVPGRRYFALFKYNRINS--KDNCTRTLIGWVLHERVFQWGCFHGRKQ
------------------------------------------------------LQIGTWTMVYDEGFELRIGKKRFFGLMKYSKAGGSEDPKCSKLQMGWTVKETPFKWGCFYAEKE
------------------------------------------------------I-IGHWTMVYDEGFEIRLNGSKYFAFFKYERKSN--KSDCTQTHIGWVLNEKIFYWGCFYAEKK
------------------------------------------------------GVIGKWTMVYDEGLDIELGTTHYFGFFKYNKINSQEDSACSEIGIGWASRKITYLYGCFYAEKI
DLPIHALVSDIAGKWTFSISYNVYGADIACGSDTPISRQVTLTLELPHLAVKSDEVIGTWTAIYDQGFYITLDGLEIFSYVSFSPVGEKYLTNPNQTQIGWVTRREGGNIACASGSR-
DLPIHVLVKDVTGKWQFYLTKPLGGLDVMCGSSMPLDRNFQLSLDYSDLAVKDAGVVGRWTIVNDQGFEVLLDSTRYFFYIRYTQNGD-FETDPNHTQIGWAYR---G---CAFASK-
DLPIHATIGDMIGKWKFDLSKEVKGENVYCGSALPLGNTLELTLDYINLAVKSYGTVGHWTAIYDQGFRITLNGYDLLLYFYFEKIEEKYATDPTRTLIGWASKRDGNNFYCCYGEK-
-------MAETAKKQKFDLSKEVEGENVNCGSTLPLGTTLDLTLDYKNLAVKSHGVVGHWTSIYDQGFRITLNGLDLLVYLYFEKIEEKYITNPNKTLIGWASKRYGNKFYCCYGEK-
------------------MSKEVTGDNANCGSDLPLDKTLILTLDYNNLAVKSDGAVGKWTAVYDQGFKIDLHGTTILTYFYFEEIEDRYATNTNKSSIGWASKKVGNGFFCSYAER-
DLPPNCIHGDVVGTWRIHVGTYKPDRDFAHSMLTPLSFTFDVKTQNLDGVFEKVGTTGNWTVILDQAFTFWTKSYRYTAFFKYINEHEASYCYCHTTLLGWWD---------------
DLPPNCLQGDVIGRWKVYSGIYKPDRDNVHEFLTPVMEKFEVIT------TETVGDIGMWTMLLDQGFSFETKKRQYLGFFKYINEENSAFSYCHTTMLGWWD---------------
DLPVHCLRHEIFGDWIFHLGPLSPERSSKYRERMPPGQTGILRLSNPNRVTMSSMVRGNWTMVYDEGFEVSLNSR-------------------------------------------
-------------------------------------------------------IIGGWTMVYDEGFQIRIGDKSIFGFIKYSLNGDELETLCYETDPGWYYDNANKKRACVFGYK-
-------------------------------------------------VYDEKKKIGTWTTIFDQGFEIRIANETYTGYMHYEPTGRDITTSHDKTRLGWVDIMYKQSHGCFYAEK-
--------------------------------------------------------VGSWTMVYDEGFEIRLRGKRYFDINKINT-------ECYKTRLGWIYEEKIYQWGCFYAEKI
DLPIHGLMHDVLGAWTFTIGTDASDHPTSCGSGSHVEKEIELTLTDKQIRKNSDVPVGTWTMVYDEGFEKEILFNKLF----------------------------------------
DAPTRCLEDQYYGLWRFTLSRQFTTDTLECETSIKDGDKIYLTLLPKGIAYTSGGLAGRWSPVYMAGVDILIGGMRYFFISKTVTGDIKTVSICNESQPGWARQNG------------
NTPVRCLEDQYYGLWSFTLSRQFTTDTLECETSIKDGDKIYLTLLPKGIAYTSGGRAGRWSPVYMAGVDILIEGMRYFFLSKTVTEGAKTVSICSESQPGWARQDG------------
DLPIHTLTKDVIGIWKIYETELY-DKFQNCGSTYPDERFFNRTLLYPRNTWNPLNNLGHWTLVYDEGVMIDINDKNYFGYFKYNNNYGNIKCYSNRLQIGWYSKKINNNYGCFYMEK-
------------------------------------------------------SVIGNWTMVYDEGFEIRLMGKRYFGMY-------------------------------------
--------SDVVGQWVIYETGF-SKSPQTCGGTSPNSNSENLKLSDERFTVKETKPLGRWTMVYDEGIDISIGDKRYFGYLGYKKGTSKYKTNPSEIKIGWVSKKSKHVWGCFYAEKV
DLPIHALTNELVGNWKVYLTNTHSEKFLNCGGSSPNNNSVDHSLVFPRNILDQKHIIGHWTMVYDVGLNIRIIWYTHLGYLRYTKGNECVDSDCYKTHIGWARRDNGT-FECFYGEKI
-------------------------------------------------------YRGSWSVGYDEGLVLVLNNRHLVAFFRYRCSGSGYESLCGQTLIGWWSSRSGAKQGCFWMRK-
DIPVHCLSRHVEGVWEIKLGLLK--YDYECGYRRPDDSDKKIIAFNKDINIIENGYSGYWRIVYDEGLHIEIYKEIYFSFFKFVKNGEVSYSYCNNLVMGVVS---------------
DIPVHCLSRHVEGKWEISLGLLKQDYDYECGYRRPDDSKKQIILFNTDIQIIENGYSGFWRIVYDEGLYMEVYKEIYFSFFKFERRNDASYSYCNRLIMGVVN---------------
DIPVHCLSRHVEGLWEIKLGLLKNKYDYECGYRRPDKQRFEIK-DKKIIAFNKPEYSGYWRIVYDEGLYIEIYKEIYFSFFKFVKNGEFSYSYCNNLIMGVV----------------
-LSARCFSDQYLGLWSFTIAKDRSNDYVGCNSRASLSSLLNVTLMDNGVAQSQGGYTGRWTPIGNHGFELRIATDVFYVISAFSANHYKVSSRCDKIHFGWRTTQGILSFYCLTG---
-------------------------------------------------------------MMYDEGFQIDVDEMSFFTFFKYYPIKEKVTTDCTETIVGWFNVPTKNKKGCMKAYKV
-------------------------------------------------------YSGYWKIIYDEGLYIEVYKEVYFSFFKFKQKGDVSYSYCNNLIMGVMNKY-------------
