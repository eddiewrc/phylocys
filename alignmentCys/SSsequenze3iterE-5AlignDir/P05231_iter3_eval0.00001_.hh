LTSSERIDKQIRYILDGISALRKETCNKSNMCE-NLNLPKMAEKDGCFQSGFNEETCLVKIITGLLEFEVYLEYLQNRFESSEEQARAVQMSTKVLIQFLQKKAKNLDAITTPDPTTNASLLTKLQAQNQWLQDMTTHLILRSFKEFLQSSLRALRQM
LTSTERIDKHIRYILDGISALRKEICNKSNMCENNLNLPKMAEKDGCFQSGFNEETCLLKITTGLLEFEVYLEYLQNRFESSKEQAGAVQMSTKGLIQSLQKKAKNLSAIATPDPATNASLLTKLQAQDQWLQGVTTHLILRSFKDFLQNSLRALRAN
LTPPGRTIESIRSILETIKELRKEMCDHDVNCMVNLHLPRLIEEDGCFPPAVNNETCLLRITSGLMEFRMYLEHLQAKFRSDEENTRVVLKNIQHLIKTLRPKVKNLNEEATLKPAVAVSLMENLQQKNQWLKTTTIHFILRGLTNFLEFTLRAVDLM
TSNSQAF-RLFTLVLHDVQELKSETCKHNVNCLNNLNLPKIKIEDGCFYGGYNWETCHLKIITGLLKFQIYLQYMQNKLQSENEKAEKIYTSVKSLSLFMKAKVSNTEQTVFPSPTANATLLEELESQNETQKLLIVQIVLCSLEEFLQNSLRPIRKA
RASSGKTAGQISYLIKEVFEMRKELCKNDETCMNHLNLPKMTEKDGCFQTGYNRDNCLVRITSGLLEFQVYLRYILNKFQKNKDRAEHVQFISKALIEILKQEVKDPNKIVLPSPTANIHLLAKLASQNDWQKVMTMQLILSNFENFLQFTLRAVRKA
YTTSQQVEGLITRVLKEILQMSKELCNNNPDCMNNLDLPVIERNDGCFQTGYDRENCLLKITSGLLDYQIYLEFLKNNVQNKKERARVIQNSTKALNQILKQEVKDPGKTATPSPTSKVLLMEKLESQKDWPRTKTIQLILKALEEFLKITMRSTRQN
LTSPDKTEELIRFILAEISVLRKKMCDKYDKCENNLKLPQMTEEDGCFHSGFNKETCLMKIITGLSEFQIYLDYLQNKFEGSKANVIVVQNSTKALVQILKQKIKNPEDVTTPDPTANASLLSKLQLQTEWLKNTTINLILRSLDDFMQFSLRAVRIM
FTSPDKTEELIKYILGKISAMRKEMCEKYEKCENNLNLPKMAEKDGCFQSGFNQETCLMRITTGLVEFQIYLDYLQKEYESNKGNVEAVQISTKALIQTLRQKGKNPDKATTPNPTTNAGLLDKLQSQNEWMKNTKIILILRSLEDFLQFSLRAIRIM
LSTPEQTEGLITHIIMEINDLNGKMCSKGIKCENKLHLPRLEDDDGCFETGFNKEECLTRITYGLSGYEKYLAYIEGKFEGDINEAVALDLGTKHLIDVLKQKLSNPTQV-TANPTTDSEVIAELDSQEDWQQYTAIHIILVNLKEYLHKTLRALRHI
LISADKM-EIIKYILGRIFALKKEMCDKYNKCENNLHLPKLAEKDGCFQSGFNQDTCLTRIATGLLEFQVHLKYLQANYEGDKENANSVYFSTKVLLQMLMEKVKNQDEVTTPDPTTDTGLQAILKSQDKWLKQTTIHLILRNLEDFLQFSLRAVRVM
YTTSQ-VGGLITYVLREILEMRKELCNGNSDCMNNLKLPEIQRNDGCFQTGYNQEICLLKICSGLLEFRFYLEFVKNNLQNKKDKARVIQSNTETLVHIFKQEIKDSYKIVLPTPTSNALLMEKLESQKEWLRTKTIQLILKALEEFLKVTMRSTRQT
YTTSQ-VGGLITHVLWEIVEMRKELCNGNSDCMNNLKLPEIQRNDGCYQTGYNQEICLLKISSGLLEYHSYLEYMKNNLKNKKDKARVLQRDTETLIHIFNQEVKDLHKIVLPTPISNALLTDKLESQKEWLRTKTIQFILKSLEEFLKVTLRSTRQT
FTSPDKSEELIKYILGRISAMRKEMCEKYDKCENNLNLPKMTEKDGCFQSGFNQETCLMRITIGLLEFQIYLDYLQNYYEGDKGNTEAVQISTKALIQLLRQKVKQPEEVSTPNPITGSSLLNKLQTENQWMKNTKMILILRSLEDFLQFSLRAVRIM
YTTSQQVGGLVTYVLREIYELRKELCNNNPGCMNNLELPVIQINDGCLQTGYNWEICLLKITSGLLDYQIYLEFVTNNVQNKKDKARVIQSTIKTLSQIFKQEVKGPDKIVTPSPTSKAILMEKLESQKEWPRTKTIKLILKALEEFLEVTMRSTRQN
LTSADKMEDFIRFILGKISALKKEMCEKYNKCENNLNLPKLAEEDKCFQSQFNQETCLTRITTGLQEFQIHLKYLEANYEGNKNNAHSVYISTKHLLQKL--RPMNRVEVTTPDPTTDSSLQALFKSQDKWLKHVTIHLILRSLEDFLQFSLRAIRIM
LTSPDKMEEFIKYILGKISALRKEMCDKYNKCENNLRLPKLAEKDGCFQSGFNQETCLTRITTGLLEFQIHLKYIQANYEGNKEDANSVYISTKLLVQMLMKKVKSQDEVTTPDPTTDTSLQAILKAQDKWLKHTTIHLILRSLEDFLQFSLRAVRIM
QSTAGKAGEHMIHITKKIFELREKMCDYNAKCLPDLNLPKLTEKDKCSPTALNKEICLMRITIELLEFQIYLEYIQNKFRNDEENARAVQISLQVETELLMQKVKKIKVVPTLSPSTKSGLLEKLQSKNKWLN-------LRSLEVFLEHSLRAMHMM
LTSSEQIENLIKSILLEISDVKNKMCDNHESCKNNLNLPKLARKDGCFHSGFNQETCLIRITTGLLEFQVYLEYIQNTFE---GHAQAMKIGTKALVNILRQKMKNPVEETIPDPTTNTGLLEKMHAQKNWLKTTTIHLILRSLEDFLQFTQRAIRM-
LTTADKTKQHIKYILGKISALKNEMCNNFSKCENNLNLPKMAEKDGCFQSGFNQETCLMKITTGLSEFQIYLEYLQNEFKGEKENIKTMQISTKVLVQILMQKMKNP-EVTTPDPTAKSSLLAKLHSQNEWLKNTTTHLILRSLEDFLQFSLRAVRIM
LTTPEKTEALIKHIVDKISAIRKEICEKNDECENKLKLPKMEEKDGCFQSGFNQAICLIKTTAGLLEYQIYLDFLQNEFEGNQETVMELQSSIRTLIQILKEKIAGL----ITTPATHTDMLEKMQSSNEWVKNAKVIIILRSLENFLQFSLRAIRMK
LTSADKMEELIKYILGKISALKKEMCDNYNKCENNLNLPKLAEKDGCFQSGFNQETCLTRITTGLQEFQIYLKFLQDKYEGDEENAKSVYTSTNVLLQMLKRKGKNQDEVTIPVPTVEVGLQAKLQSQEEWLRHTTIHLTLRRLEDFLQFSLRAVRIM
LTSPDKTEALIKYILGKISAMRKEMCEKYDKCENNLNLPKMAEKDGCFQSGFNQETCLMRITTGLLEYQIYLDYLQNEYEGDKGSIEAVQISIKALAQILRQKVKNPDEVTTPDPTTNASLMNNLQSQNDWMRNTKIILILRSLENFLQFSLRAVRIK
LTTPEKTEALIKRMVDKISAMRKEICEKNDECENKLNLPKMEEKDGCFQSGFNQAICLIRTTAGLLEYQIYLDYLQNEYEGNQENVRDLRKNIRTLIQILKQKIADL----ITTPATNTDLLEKMQSSNEWVKNAKIILILRNLENFLQFSLRAIRMK
LTSANKVEELIKYILGKISALRKEMCDKFNKCENNLHLPKLEGKDGCFQSGFNQETCLTRITTGLVEFQLHLNILQNNYEGDKENVKSVHMSTKILVQMLKSKVKNQDEVTTPDPTTDASLQAILQSQDEWLKHTTIHLILQSLEDFLQFSLRAVRIM
PTSPNQTENLIKSIFLEISEVRNKMCGNDDSCKNNLNLPKMVEKDGCFQSGFNQETCLKKITTGLLEFQIYLDYLQNKFE---ENAKAMQMRTKALVQVLKQKVKNPDEITTPEPTTNSSLLAKLQSQSEWLQTTTIHLILRSLEDFLQFTQRSVRIM
LTTPDKTEALIKHIVDKISAMRKEICEKNDKCENNLNLPKMKEKDGCFQSGFNQETCLIRSTVGLLEYQTYLDYLQNEYEGDQENVKDLRSSIRTLLQIMRQKSIDL----VTTATTNPDLLEKMQSSNEWVKNAKIILILRSLENFLQFSLRA----
YTTSQQVRGLITYVLSEILETRKQLCNDNPDCMNNLKLPEIQRDDGCFHTGYNRDVCLLKITSGLLEYQTYLEYVKNNLQNKKDKARAIQSNTKTLIRIFKQEVKDPGQIVF-DPTSEALLLEKLESQTEWL--------------------------
-----------------------QMCEKFTVCQNNLNLPKVTEEDGCLLKRTVEDKCLRKISSGLYTFQTYLKYIQETFISENQNVESLSYSTEHLARTIRQMVINPEEVIIPDAATQESLHTELKSTKDWTEKITIHLILRYFTSFMERTVRAVRY-
-----------------------QLCDKYAVCDNSMNLPKMDDR--CLQSSFNKEKCFTKIASGLYAYHIHLLFVQDIYTNEGKVVKDLRISTEQLVDAVKLMVKDYYEVPELDADAQKVLLAKLYSESDWLQKVTARLILRDFTHFMAKTARAIRR-
------------------------MCESSKEAENNLNLPKMAEKDGCFQSGFNEETCLVKIITGLLEFEVYLEYLQNRFESSEEQARAVQMSTKVLIQFLQKKVG-----------------------------------------------------
------CEPLARVLRDRAVQLQDEMCKKFTVCENNLNLPKVTEEDGCLLAGFDEEKCLTKLSSGLFAFQTYLEFIQETFDSEKQNVESLCYSTKHLAATIRQMVINPDEVVIPDSAAQKSLLANLKSDKDWIEKITMHLILRDFTSFMEKTVRAVRYL
------CEALAWLLHARAARLQEEMCEKFTVCENNLNLPKVTEEDGCLLAGFDEEKCLKKLFSGLFTFQTYLEYVQETFTSEKQKVESLCYSTKHLATTIRQMVINPDEVVIPDSATQKSLLTKLKSDKTWIEKITTHLILRDFTSFMEKTVRAVRYL
------CKSLAKTLYKEADALKEENGENQSMCDNNLVFPNLTEQDGCFYSGFNKETCLINLISKLQEFDGYFQFMENELKEKKSRIEALKMFTTQLAESLKKLMMGADLVPTLNPTASHDLVLKLKSLNEWSRKVALHLSLCRYIKFMEHTIRAIRNM
------IVNHAKYLEKTASDLKEEICRIHNLCDNNLLLPNITERDGCLPSSFNEETCLIKIISGLQDFDIFLNYMETEMED--NRFQTLKLSTTQLANTLKTVIKKTDLVPTTNPTTSSILLSELQSLTAWSRKVGFRLILWHYTRFIQGTVRAVRYL
------------------------------------------EEDGCFPLAANHETCLLRITSGLLEFQMYLEHLQAKFRSEEENTRVILKNMRHLINTLRPKVKNFNEGATLKPAVVASLMENLQQKDQWLKTTTIHFILRGLTDFLQFTLRSVRLM
------------------------------------------EEDGCFPSALNHETCLLRITSGLLEFQMYLEHLQAKFRSDEENTRVMLKNIRYLIKTLRPKVKNLNEGATLKPAIVASLMKNLQQKDQWLKTTTIHFILRNLTDFLQFSLRAVGLM
---------------SKAAKLKDEMCEKFTVCDNDLNLPKITEEDGCLLSGFNEEKCLSGISSGLFTFQTYLEYVQETLITEKQKVESICYGTKHLANTVRQMVKNPGAVIMPDPATQNTLFAKLKSNKKWIEKITTHLILRDFTSFMEKTVRAVRY-
--------SLTKTLWKQANGLKDKICDDYSLCENNMNLPKMTEEDGCFLSGFNEETCLKRIVTGLSEFQIYLKYVQKTFKGEEKILESIQNYTKHLSNILKQ--------------------------------------------------------
---------------------------------------------------FNQETCLIRSTVGLLEYQAYLDYLQNEYEGDEENIKDLQSSVRTLLQIMRQKSIDL----VTTPTANLDLLEKM---------------------------------
------------------------------------------------------------------------------------------------------------ETSTPLPTSKALLMEKLESQKEWSRTKTIQLILKALEQFLKVTMRSTRQN
---------------------------------------------------IHEETCLLKITTGLLEFEVYLEYLQRE--------------------------------------------------------------------------------
---------LAKLIEREAASLQIKLCDNHGVCDNNLQLPKISTEDRCFSLGFQKDKCLNKIHRDLSIFKIYLVHVKETFISEKNTVESLQYKTQILIQIMKR-----TETESDDKGTNIQVLQALKSENLWRQRVTNRLILQAFIECIQKTARAVRY-
---------------------------------------------------IHEETCLLKITTGLLEFEVYLEYSDNR--------------------------------------------------------------------------------
---------RIDWLLSVVAACYEDLCANTGICQFNADMPVVEQGSGCFPPSLNETRCLKDLAIGVWGLVEPFVFLNEHFGEYVEHVGAMHMLARAIAWDMRDHANRDLYEPFELPVPKPKMLPLLRRLTTWNKHIAAFNILRRIERFANDANRALSY-
---------RIGWLLSVVSACYEDLCTNTGICQFNADMPVVKQDEGCFPPSLNKTRCLQELAVGFWGLVDPFVFLNEHFGDYVEHVGAMELLVRAIAWDMRDESNRLSQRPFELPSSDSKMLPRLRGLTTWNKHMAAFKILQRIEKFANDANAALTY-
---------RLNWMLWVIDECFRDLCYRTGICKFHLKLPAINDTDHCGLIGFNETSCLKKLADGFFEFEVLFKFLTTEFGKSVINVDVMELLTKTLGWDIQEELNKLTKTHYSPPKFDRGLLGRLQGLKYWVRHFASFYVLSAMEKFAGQAVRVLDS-
--------------------IHSQMCDYNAKCLPDLNLPKLTEKDKCSPTALNKEANLMKLGEGLSEFQKHLELLQNKS-----RANDVQINSQALTQILKQMCLNPDALITQPPKSDSW---RVHSQGQWLKNMMIPLSFAMLKKLLELHVRAVHLM
-------------------------------AENNLHLPKL-GKRWMLQSGFNQETCLTR--------------------------------------------------------------------------------------------------
-----------ESVLGATKLHKNEFLVEFQGYDDRYKIPSLPAK--CPYSNFGKDACLRRLLEGLLIYSVLLKRVEEEFPSSS-ILSEVRFYSNILIKELENKVRDRDQVMRLTSSQEEQLLKDTDYPDTFHRKMTAHGILYNLHYFLVDCRRVINKR
-----------DLIIGVTAHHQKEFEDEFQQYRNHYKLSSLPAD--CPSANFSKEACLQRLAEGLHTYMVLFKHVEKEYPSSS-ILLHARYHSGALIGLIKEKMRNPGQVTVPTSRQEQQLLQDMDNPSTFHRKMTAHNILRQLHNFLRNGKVAIRKR
-----------HAVLGVTKLHKKEFEDEFENYNENYKVFSVPER--CPHSNFSKEACLHRMAHGLLVYTVLLKHVEKEYPGSL-ICSLAKYYSGLLINLCKEKMRNPDQVAALTDRQEAQLLRGLNLPSAFQRKMAAHSVLRQLHYFLLDGRRAIAKR
-----------ESVLRATKRHKKEFLAEFQGYEDRYKIPSLPAK--CPYSNFGQDACLRRLLEGLLVYSVLLKHVEQEYPLSR-I-SEVRYYSNVLIKEVENKVKERGQVTTLSSSQQEQLLRAVDRSDTFHRRMTAHGILYNLHYFLVDCRRVINKK
-----------GLIISVTARHQQEFEDEFQQYHDHYKISSLPAN--CPSANFSKEACLHRLGEGLHTYMVLFKHVEKEYPSSP-ILLYARYHGGALIGLIKEKMRNPGQVTVPTSSQEQQLLQDVDSPNTFQRKMTAHNILRHLHDFLRNGRVAIRKK
-----------RSLFDSAQEYEKAFEHHFQTDQDSHTPASIPKH--CNITKFRKDACLQTLAKGLLIYSVLLKHVEKEYRSSL-NFSDAPSNIGTLIGMVKGKMKNRNQVTPLTSSEEEQLLKEVNSPDPYHRKLHAYSILRALKAFLSEGKRAVCRM
-----------KVVLEVIKTHRQEFEAEFH-YDAQYNIPSLPAD--CPSTNFSMEALLHRLLQGLPVYTALLKYVEKEEPKSQ-IPSRFRQNSELLKQRITGKMRHAVQVTPLTSSQEQQLLRDLDSSDTFHRKMTAHSILYQLRSFLVDCKNAINKK
-----------ERLIEATKQHQKEFEQEFQGYIEGHKRSAFPVK--CPMSNYSKEACLQRLAQGLLVYTVLLKQVEKEYPDNS-ILREIKPGIPLLVAQIKEKMKRSERVPAPSSSQEEQLLKQLDSPDTFQRRMTAHSILSHLRFFLIDGKRAFRK-
---PPKWEKMIKMLVHEVTTLRNQQFEEFQKPESQHQVPPHLSKTLC--SASNKEACLQEISRGLQVYQLLLQHVKAEYPQS-TLLPSVTHQTTVLIGLVKDQMKVAEVVEDLSASERKRVLGEVSTGTEWERKTSVHAILRELRNFLVDTKRALRRM
------------------------------------------------------------------------------------------------------QINNTEQMEFLSPTPDATLLEKLETQSQTQMLLIAEIVLQRLEEFLQDSLRAIRK-
-------------------------------------------PAGC-PVTMNKEACLLRLVQGLQKYKVLLTHVKKEYPDN-ALLPHIKYNSDLLIHLIEEKMRHPERLTVLSDTEAQSILQGLENTNTFQRKVTAHSILRKLHLFLIDSSRDL---
-----QIQEQVRKIQNDIEGLQQRLCADYRLCHLYLGIPRP-SVKNCYSQDLKLVACLNQLYYGLQFYQGLVRALEGISPELAPTLDTLKLDIGDFAASIWQQMEDFQVVPEIVPTQS--ILPTFS--SSFYRRAGGVLIFSQLQSFLDMAYRALRLL
-----KSLEQVRKIQARNTELLEQLCATYKLCHHSLGIPKA-SLSSCSSQALQQTKCLSQLHSGLFLYQGLLQALAGISSELAPTLDMLHLDVDNFATTIWQQMESLGVAPTVQPTQS--TMPIFT--SAFQRRAGGVLVTSYLQSFLETAHHALHHL
-----QSQEQVRKIQMDMEELQQRLCADYSLCHQYLGIPRP-PLKNCYSQDLRLAACLNQLHDGLQLYQELLKALQGISPELAPTLDTLQLDVADFTATIWLQMEDLQVTPKIMPILG--TLPSFS--SSFWRRAGGILIFDRLQAFLETAYRALRHL
-----KSLEQVRKIQASGSVLLEQLCATYKLCHHSLGIPKA-SLSGCSSQALQQTQCLSQLHSGLCLYQGLLQALAGISPALAPTLDLLQLDVANFATTIWQQMENLGVAPTVQPTQS--AMPAFT--SAFQRRAGGVLAISYLQGFLETARLALHHL
-----KSLEQVRKIQTRNSELLEQLCATYKLCHHSLGIPQA-PLSRCSIQALQLTKCLSQLHSGLFLYQGLLQALTGISPELAPTVDMLQLDVANFATTIWQQMESLGVAPTVQPTQS--TLPTFT--SAFQRRAGGVLAASHLQSFLETAHCTLNHL
-----KCLEQVRKVQGDGTELQEKLCATYKMCHHSLGIPQV-SLSSCPSQALQLTGCLRQLHRGLFLYQGFLQTLEGISPELAPTLDMLRLDITDFATTIWQQMEELGTAPALQPTHG--ATPALP--SAFQRQVGGLLVASHLQSFLELVHRVLRHL
-----KSSEQARRVQAEAMVMQEKLCATHRLCHHSLGIPWA-SLRSCSSRDLQLLGCLRQLQAGLMLYQGLLQALAGISPALSSSVDLLRLDVADFATTVWQQMEDLGVAPAAQPTLG--TVPAFT--SAFQRQAGGVLVASHLQDFLELALRALRYL
-----KCLEQARRVQAEAVEMQQKLCATYRLCHHSLSIPWV-PLKGCSSQDLQPTRCLRQLQAGLVLYRGLLRALVGISPELSPTVDTLQLDVADFATTIWQQMEDLGTAPAVQPTPG--TMLTFT--SAFQRQAVGAMVASQLQCFLELAYRALRYL
-----KCSEQVRKIQADTSMLQEELCATHKLCHHSLGIPRA-PLSSCSSQALQLPGCLSQLHSGLLLYRGLLQALAGISPASAPALDKLQLDVADFATTIWHQMEDLGQAPAVQPTQS--TMPALT--SAFQRRAGSVLVASHLQSFLELTYRVLRYL
-----KCLEQVRKIQADVVAMQERLCATHKLCHHSLGIPQV-PLGSCSSQALQLTSCLGQLHGGLFLYQGLLQALAGISPELGPTLDMLQLDITDFATNIWQQMEDLGMAPGVQATQG--TVPTFT--SAFQRRAGGVLVASSLQSFLQLALRVLRHL
-----KCLEQMRKVQADGTALQERLCATYKLCHHALGIPQP-PLSSCSLKSLWTTGCLRQLHGGLFLYQGLLQALAGVSPELAPALDTLQLDISDFAVNIWQQMEELGVAPAVPPTQG--TMPTFT--SAFQRRAGGVLVASDLQSFLELAYRALRSF
-----KCLEQVRKIQGDGAALQEKLCATYKLCHHSLGIPWA-PLSSCPSQALQLTGCLSQLHSSLFLYQGLLQALEGISPELSPTLDTLQLDIADFATTIWQQMEDLGMAPALQPTQG--AMPAFT--SAFQRRAGGVLVASHLQRFLELAYRVLRHL
-----KCLEQMRKVQADGTALQETLCATHQLCHHALGIPQP-PLSSCSSQALQLMGCLRQLHSGLFLYQGLLQALAGISPELAPTLDTLQLDTTDFAINIWQQMEDLGMAPAVPPTQG--TMPAFT--SAFQRRAGGVLVASNLQSFLELAYRALRHF
-----KCLEQVRKIQADGAALQDRLCATHKLCHHSLGIPQP-LLSSCSSQALQLTGCLSQLHSGLLLYQGLLQALAGISPELAPTLDMLQLDVTDFATNIWQQMEDLGVAPVVQPTHG--PMPTFA--SAFQRRAGGVLVASNLQRFLELAYRGLRYL
-----QCLEQVRKIRADGAVLQERLCATYKLCHHSLGITQA-PLSSCSSQALEVVDCLHQLHSGLVLYQGLLQALAGTSPEVASTLDLLHLDVADFAINIWQQMEDMGVAPTGKPTQG--PMPTFT--SAFQRRAGGVLVASNLQSFLEQAYRVLRYL
-----KNLEFTRKIRGDVAALQRAVCDTFQLCTPDPHLVQA-PLDQCHKRGFQAEVCFTQIRAGLHAYHDSLGAVLRLLPNHTTLVETLQLDAANLSSNIQQQMEDLGLDTVTLPQRS--PPPTFS--GPFQQQVGGFFILANFQRFLETAYRALRHL
-----KDLEYIRKIKGDVARLQELMCSTFQLCSQKLGITQA-PLDQCHSKTFQVSGCPRSVRASTEAW-GALGLREATCPGQGLGAPGVS-GPGSLPSSAPLQMEDLGLNTVTYPGQG--PLPSFS--SDFEKQASGFIILANFQRFLEMALRALRHL
-----KNKEFVARMKSEISDLKESMRTDFSLGTDFLGIEQA-DHSHCQKATCDLGNCFKQLRAGLHTYYGYLSHIKQILPNYTNRVSSLQLDTSNLSTNIQLQFEESSLPVVTYPQAE--NQPNFL--Q--QREIGSYLVLRKFMLFMDVITRALNHC
-----KCLEQVRKIQAQGSVLQEKLCATYQLCHHSLGIPQA-PLSNCSSQGLQLTGCLSQLQSGLFLYQGLLQALAGISPELSPTVDMLQLDVANFATTVWQQMEDLGVAPAVQPTQG--TMPTFT--SAFQRRAVGVLVASRLQRLLELVYRVLRHL
-----KCLEQVRKIQADGVVLQERLCATHNLCHHSLGISQA-PLSSCSSQALQLTGCLRQLHSGLFLYQGFLQALAGISPKLAPTLDMLHLDIADLATNIWQQMEDLGVAPAVQATQG--TMPIFT--SAFQRRAGGVLVAANLQSFLELAYRVLRYL
-----KCLEQVRKIQNEGAALQENLCATYKLCHHSLGIPRT-TLNGCPSQDLQLTSCLSQLHRGLFLYQGLLQALAGISPELAPTLDMLQLDVADLATTIWQQMEDLGVAPALQPTQG--AMPTFT--SAFQRRAGGILVTSHLQSFLELAYRVLRYL
-----KCLEQVRKIQADGAELQERLCATHKLCHHSLGLPQA-SLSSCSSQALQLTGCLNQLHGGLVLYQGLLQALAGISPELAPALDILQLDVTDLATNIWLQMEDLRMAPASLPTQG--TVPTFT--SAFQRRAGGVLVVSQLQSFLELAYRVLRYL
-----KCLEQVRKIQADGAELQERLCAAHKLCHHSLGIPQA-PLSSCSSQSLQLTSCLNQLHGGLFLYQGLLQALAGISPELAPTLDTLQLDVTDFATNIWLQMEDLGAAPAVQPTQG--AMPTFT--SAFQRRAGGVLVASQLHRFLELAYRGLRYL
-----KGLEQVKKVQGDGTELQEKLCATYKMCHHSV-IPQV-SVSRCPSQALQLTGCLRQLH-SLFLYQGLLQTLEGISPELAPALDMLCLHIADFATTIWQQMEELGTAPVLQPTRG--ATPALP--SAFQRRVGGLLVASHLQSILELVHRVLRHL
-----KNLEFTRKIRGDVAVLQRVVCDTFQLCTPDPPLLQA-PLDQCHKRGFQAEMCFGQIRAGLHAYQDSLGAVLQLLPEHTALVETLQLDAANLSTNIQQQMEDLGLDTVTLPHRS--PPPTFS--TDFQQQVGG---------------------
-------------------------CDTLQLCKQELDIAQA-PLEQCHRRTFQAETCFSQIRAGLRIYGGSLATIQALLPGHAGLVETLQLDMANLSSNIQQQMEDLGLATVTYP-QD--PVPTFS--SHFHHQVGGFFILANFQRFLETAYRALRHL
-------------------------CTKHSLCHHYLGVRQA-SVNRCPSRDLELGPCLKQLARGLKLYQAQLEALEGISPQLAPALDTLQLDVRDFAINIWQQLEDLRLTATPLSPQA--SVPTFT--SAFQRRAGAVLVLDNLQGFLEVVARVLNQL
------NKEFSRKITRDAEKLKNLMCDNHGLCEEHLQLPDV-PLGQCQAGSFSQEGCFSQLSNGLQELQRRVVAMPGHLPEA--DLQRLEKDISDLLVNIQEEMEAQGITAQSSPSQ---ALPTYS--KGFHKKVAMFLILSDLAS------------
--------VLAEKIHPEIKNLQDLICKQHHICNEYLRIPHP-RLEECRSNNLQLDTCMNKMEAGLQVYGDYLNLIKKVIVGSS-LAETVQADIMDLQQHIRQKVIDVAILVLHVGMHPQYTLPNFA--ASFDEQAGGHIVLSHFKRFMETVLRVLQYM
----------LESVLRATKRHKKEFLAEFQGYEDRYKIPSLPAK--CPYSNFGQVGRVPALTTGLFGFFPSFGWISTRFSSSAGSLSEVRYYSNVLIKEVENKVKERGQVTTLSSSQQEQLLRAVDRSDTFHRRMTAHGILYNLHYFLVDCRRVI---
----------------------------------------------CP-EKSSKEACLRCLAQGLLTYTALLKHVEKESPSSI-RSEGFKSLLLRLTSGIKNKMRHREHVKALTNSQEGHLLRDFDSPDPFQRLMTTFKILYKLRDFLIDGIKN----
----QKWHLMARDLYKDVKTLRDEQFRDFREMVVRISTPLLKPSDRCLSKNFSTERCLTRIYSVLTWYKDNWNYIEKENLTSV-LVNDIKHSTKRLLEAINSQLQVRDGEMDQTSSTS----LSFK--SAWTRKTTVHSILFNFSSVMIDACRAINYM
----LMSDSLTQKILLSIPDTHKSCIQSETLYATTIGIRPAPVL-SVVSENYSLETSLRHMSEGFQLHRDLLSAVSPRLAN-KDNVTGLVADIRDLVLRINE-MLKMAAVVQPSPT---PVALNLP--GNYEVQVAAHMILVQLQAFGQDTARCLRSL
----ERAKTLVEKILRELPAVHTATVNTEGLLQTSLGIPATPIL-KPLSERFTMDMCVSRMSVGCLLYQGLLGVLADRL----SGLTNLRADLRDLLTHINK-MKEAAAESPDQNQS-LDLASRLH--GNYEVQVAVHVTLTQLRSFCHDLIRSLRAI
----ERAKTLVDKILRDVSIVHSATVTMEGLLQTSLGIPPAPVL-KPLSEHFTLDVCVSRMSSGILLYQRLLGVLSDRV----SGLSDLQADLRDLQTHVAT-VREVADPELDENQNQPDLASRLH--GNYKVQIATHLTLTHLRSFCHDLIRSLRGV
----ERVMRLVEKIRKDVRSVHGSIINIDGLLEITLGIPDTPVL-KELSEQFTADMCVSRMLAGGRLYQGLLVDLSGRL----GGLEALSAELRDLVTHINQ-MKDAASDSSDQSSL--DLTLNLH--GNYDVQVAAHLTLCQLLEFCHDLIRSLRNM
----QTSRLLLQKVLMAIPETHRSSVQSESLLVSTIGIPPAPVL-KALSENFTMGTCLRRISEGLQLHRTLLAVIADHLKN-KDRVLALQADIRDLNIQINK-MLKMVTVVPP------AVTLNLP--ADYEVQVAAHLTLQQLQTFGRDVDRHLKSL
-----DARSLVEKILGNIPMVHESSVKIPGLYQTSLGIPAAPEL-KTVCRDFTLEVCLSSMSAGLQLYQDVLGVLKERVT--TEKVTGLLADIRDLLAQVNK-MQEPGSVAQYEAS---GLASRLP--GDYEVQVATHFILLQLRDFTQNLKRSLRNV
----EPARTLVQKILKDIPVAHAVAVSSRGLLQESLGLPVAPLL-KLPSDHFTLDMCVSRMLVGCQMFQRLLAVLSEKL----DGLMDLKVTLRDLVTHITK-MKETLVDGSEALTV--DVASRLH--GDYEAQMAAHLALVQLRSFCHDLTRSLRAI
----ERARTLVQKILRDIPVAHGAAISTKGLLQLSLGLPVAPLL-KPPSEQFTLDICVSRMLVGCQMFQKLLGVLSERV----DGLMDLKVTLRDLVTHITK-MTETVGDTPEAPSW--DAASRLP--GNYEAQMAAHLTLIQLRSFCHDLTRSLRAI
----KDSSSLTHKILQSIPGAHRSCVNVETLLVSTIGIPSAPAL-SALSDNFTLNTMLRHMLEGLQLHKDLLNHVLPRLEV-KDMVIDLTHDLNDLSVQVLK-MLKLAGVSKPNPTL--GLALDLR--GSYEVQVAIHLILVQLQGFEQDMDRCLRSL
----QPAMTLVNKILIDVRTVHAAAVTNEGLLLASLRIPAAPVL-K-LPHEDTLDVCLSRMLAGVQLYQGLLGDLSGRL----SGLSDLKADLRDLLNHITE-IKKAAGEAVQNQSV--DLASRLQ--GDYEVQVAANATLKQLLSFCHDLMRSLKKI
----QKCKLLTEKILLSIPETHKSCIKTETVFESGIGIPSAPHI-KAPSENVTLEDSLRQLYEGLQLYRALLGSLGNRLAN-KDKVTGLMADVKDLAIQIKE-MLKIVGSPHPTKP---SVSLNLP--AEYEFQVAAHLTLVDLQSYIQDLVRFLRTL
------------------------------------DVPHIQCGDGCDPQGLNSQFCLQRIHQGLVFYKQLLDS--DIFTGEPSLLPVGQLHTSGLSQLLQPEDHQWESQQMPSLSPS----------QQWQRPLLRSKILRSLQAFVAIAARVFAH-
------------------------------------NVPRIQCEDGCDPQGLNSQFCLQRIRQGLAFYKHLLDS--DIFKGEPALLPMEQLHTSGLSQLLQPEDHPRETQQMPSLSSS----------QQWQRPLLRSKILRSLQAFLAIAARVFAH-
------------------------------------DVPHIQCGDGCDHQSLNSQLCLQQIHRGLIFYGHLLNS--DIFKGEPSLLPVGQLHSSGLSQLLQPKDHSWKKQQVP---PS----------EPWQRLLLRPKILQRLRAFAAIAARVFTH-
------------------------------------DVPRIQCEDGCDPQGLNSQSCLQRIHRGLVFYEKLLGS--DIFTGEPSLLPVDQLHASGLRELLQPKGHHWETEQTPSPIPS----------QPWQRLLLRLKILRSLQAFVAVAARVFAH-
------------------------------------DVPHIQCEDGCDPQGLNTQFCLQRIRQGLFFYEKLLGS--DIFTGEPALLPVGQLHASGLSQLLQPEGHHRETPQTPSLHPS----------QPWQRRLLRFKILRSLQAFLAVAARVFAH-
------------------------------------EVPRIQCGDGCDPQGLNSQFCLQRIHQGLVFYEKLLGS--DIFTGEPFLLPVGQLHASGLRQLLQPEGHHRETEQTPSPSPS----------QPWQRLLLRFKILRSLQAFVAVAARVFAH-
------------------------------------EVPRIQCWDGCDPQGLNSQFCLQRIHQGLIFYEKLLGS--DIFRGEPSLLPVGQLHASGLSQLLQPEGHHSETQPTPTPSPS----------LPWQRLLLRLKILRSLQAFVAVAARVFAH-
------------------------------------DVPHIQCEDGCDPQGLNSQFCLQRIHQGLLFYEKLLGS--DIFTGEPPLLPVGQLHASGLSQLLQPEGHHWEPQHTASPIPS----------QPWQRLLLRFKILRSLQAFVAVAARVFAH-
------------------------------------DVPHIQCGDGCDPQGLNSQFCLQRIYQGLIFYQKLLGS--DIFTGEPSLLPVGQLHASGLSQLLQPEDHQQETQ-TPSLSPS----------QPWQRLLLRIKILRSLQAFVAVAARVFAH-
------------------------------------YVPHILCGDGCDPQGLNSQFCLQRIYQGLVFYQNLLGS--DIFTGEPPLFPVSQLHASGLSQLLQPEVHQWEPQ-IPSLSPN----------QPWQRLLLRIKILRSFQAFVAVAARVFGH-
------------------------------------EVPRIQCGDGCDPQGLNSQFCLQRIHQGLVFYEKLLGS--DIFTGEPSPVGQLHASLLGLRQLLQPEVHHWETEQTPSPSPS----------QPWQRLLLRFKILRSLQAFVA---------
