LTSSERIDKQIRYILDGISALRKETCNKSNMCESSKEALAENNLNLPKMAEKDGCFQSGFNEETCLVKIITGLLEFEVYLEYLQNRFESSEEQARAVQMSTKVLIQFLQKKAKNLDAITTPDPTTNASLLTKLQAQNQWLQDMTTHLILRSFKEFLQSSLRALRQM
LTSTERIDKHIRYILDGISALRKEICNKSNMCESSKEALAENNLNLPKMAEKDGCFQSGFNEETCLLKITTGLLEFEVYLEYLQNRFESSKEQAGAVQMSTKGLIQSLQKKAKNLSAIATPDPATNASLLTKLQAQDQWLQGVTTHLILRSFKDFLQNSLRALRAN
LTPPGRTIESIRSILETIKELRKEMCDHDVNCMNRKEALAEVNLHLPRLIEEDGCFPPAVNNETCLLRITSGLMEFRMYLEHLQAKFRSDEENTRVVLKNIQHLIKTLRPKVKNLNEEATLKPAVAVSLMENLQQKNQWLKTTTIHFILRGLTNFLEFTLRAVDLM
TSNSQAF-RLFTLVLHDVQELKSETCKHNVNCLEEEKAML-NNLNLPKIKIEDGCFYGGYNWETCHLKIITGLLKFQIYLQYMQNKLQSENEKAEKIYTSVKSLSLFMKAKVSNTEQTVFPSPTANATLLEELESQNETQKLLIVQIVLCSLEEFLQNSLRPIRKA
RASSGKTAGQISYLIKEVFEMRKELCKNDETCIKSHVAVSENNLNLPKMTEKDGCFQTAYNRDNCLVRITSGLLEFQVYLRYIRNKFQNNRDRAEHVKFSSKALIEILKQEVKEPNKIVFPSPTANINLLAKLESQNDWQKVLTMQLILSNFEDFLKFTLRAVRKA
YTTSQQVEGLITRVLKEILQMSKELCNNNPDCMTHDDALSENNLDLPVIERNDGCFQTGYDRENCLLKITSGLLDYQIYLEFLKNNVQNKKERARVIQNSTKALNQILKQEVKDPGKTATPSPTSKVLLMEKLESQKDWPRTKTIQLILKALEEFLKITMRSTRQN
LTSPDKTEELIRFILAEISVLRKKMCDKYDKCENSREALAGNNLKLPQMTEEDGCFHSGFNKETCLMKIITGLSEFQIYLDYLQNKFEGSKANVIVVQNSTKALVQILKQKIKNPEDVTTPDPTANASLLSKLQLQTEWLKNTTINLILRSLDDFMQFSLRAVRIM
FTSPDKTEELIKYILGKISAMRKEMCEKYEKCENSKEVLAENNLNLPKMAEKDGCFQSGFNQETCLMRITTGLVEFQIYLDYLQKEYESNKGNVEAVQISTKALIQTLRQKGKNPDKATTPNPTTNAGLLDKLQSQNEWMKNTKIILILRSLEDFLQFSLRAIRIM
LSTPEQTEGLITHIIMEINDLNGKMCSKGIKCEGDSHVMENNKLHLPRLEDDDGCFETGFNKEECLTRITYGLSGYEKYLAYIEGKFEGDINEAVALDLGTKHLIDVLKQKLSNPTQV-TANPTTDSEVIAELDSQEDWQQYTAIHIILVNLKEYLHKTLRALRHI
LISADKM-EIIKYILGRIFALKKEMCDKYNKCEDSKEALAENNLHLPKLAEKDGCFQSGFNQDTCLTRIATGLLEFQVHLKYLQANYEGDKENANSVYFSTKVLLQMLMEKVKNQDEVTTPDPTTDTGLQAILKSQDKWLKQTTIHLILRNLEDFLQFSLRAVRVM
YTTSQ-VGGLITYVLREILEMRKELCNGNSDCMNSDDALSENNLKLPEIQRNDGCFQTGYNQEICLLKICSGLLEFRFYLEFVKNNLQNKKDKARVIQSNTETLVHIFKQEIKDSYKIVLPTPTSNALLMEKLESQKEWLRTKTIQLILKALEEFLKVTMRSTRQT
YTTSQ-VGGLITHVLWEIVEMRKELCNGNSDCMNNDDALAENNLKLPEIQRNDGCYQTGYNQEICLLKISSGLLEYHSYLEYMKNNLKNKKDKARVLQRDTETLIHIFNQEVKDLHKIVLPTPISNALLTDKLESQKEWLRTKTIQFILKSLEEFLKVTLRSTRQT
FTSPDKSEELIKYILGRISAMRKEMCEKYDKCENSKEALSENNLNLPKMTEKDGCFQSGFNQETCLMRITIGLLEFQIYLDYLQNYYEGDKGNTEAVQISTKALIQLLRQKVKQPEEVSTPNPITGSSLLNKLQTENQWMKNTKMILILRSLEDFLQFSLRAVRIM
YTTSQQVGGLVTYVLREIYELRKELCNNNPGCMDNDYVLLENNLELPVIQINDGCLQTGYNWEICLLKITSGLLDYQIYLEFVTNNVQNKKDKARVIQSTIKTLSQIFKQEVKGPDKIVTPSPTSKAILMEKLESQKEWPRTKTIKLILKALEEFLEVTMRSTRQN
LTSADKMEDFIRFILGKISALKKEMCEKYNKCEDSKEALAENNLNLPKLAEEDKCFQSQFNQETCLTRITTGLQEFQIHLKYLEANYEGNKNNAHSVYISTKHLLQKL--RPMNRVEVTTPDPTTDSSLQALFKSQDKWLKHVTIHLILRSLEDFLQFSLRAIRIM
LTSPDKMEEFIKYILGKISALRKEMCDKYNKCEDSKEALAENNLRLPKLAEKDGCFQSGFNQETCLTRITTGLLEFQIHLKYIQANYEGNKEDANSVYISTKLLVQMLMKKVKSQDEVTTPDPTTDTSLQAILKAQDKWLKHTTIHLILRSLEDFLQFSLRAVRIM
QSTAGKAGEHMIHITKKIFELREKMCDYNAKCLNRTAALAAPDLNLPKLTEKDKCSPTALNKEICLMRITIELLEFQIYLEYIQNKFRNDEENARAVQISLQVETELLMQKVKKIKVVPTLSPSTKSGLLEKLQSKNKWLN-------LRSLEVFLEHSLRAMHMM
LTSSEQIENLIKSILLEISDVKNKMCDNHESCKNSKEVLTENNLNLPKLARKDGCFHSGFNQETCLIRITTGLLEFQVYLEYIQNTFE---GHAQAMKIGTKALVNILRQKMKNPVEETIPDPTTNTGLLEKMHAQKNWLKTTTIHLILRSLEDFLQFTQRAIRM-
LTTADKTKQHIKYILGKISALKNEMCNNFSKCENSKEVLAENNLNLPKMAEKDGCFQSGFNQETCLMKITTGLSEFQIYLEYLQNEFKGEKENIKTMQISTKVLVQILMQKMKNP-EVTTPDPTAKSSLLAKLHSQNEWLKNTTTHLILRSLEDFLQFSLRAVRIM
LTTPEKTEALIKHIVDKISAIRKEICEKNDECENSKETLAENKLKLPKMEEKDGCFQSGFNQAICLIKTTAGLLEYQIYLDFLQNEFEGNQETVMELQSSIRTLIQILKEKIAGL----ITTPATHTDMLEKMQSSNEWVKNAKVIIILRSLENFLQFSLRAIRMK
LTSADKMEELIKYILGKISALKKEMCDNYNKCEDSKEALAENNLNLPKLAEKDGCFQSGFNQETCLTRITTGLQEFQIYLKFLQDKYEGDEENAKSVYTSTNVLLQMLKRKGKNQDEVTIPVPTVEVGLQAKLQSQEEWLRHTTIHLTLRRLEDFLQFSLRAVRIM
LTSPDKTEALIKYILGKISAMRKEMCEKYDKCENSKEALAENNLNLPKMAEKDGCFQSGFNQETCLMRITTGLLEYQIYLDYLQNEYEGDKGSIEAVQISIKALAQILRQKVKNPDEVTTPDPTTNASLMNNLQSQNDWMRNTKIILILRSLENFLQFSLRAVRIK
LTTPEKTEALIKRMVDKISAMRKEICEKNDECESSKETLAENKLNLPKMEEKDGCFQSGFNQAICLIRTTAGLLEYQIYLDYLQNEYEGNQENVRDLRKNIRTLIQILKQKIADL----ITTPATNTDLLEKMQSSNEWVKNAKIILILRNLENFLQFSLRAIRMK
LTSANKVEELIKYILGKISALRKEMCDKFNKCEDSKEALAENNLHLPKLEGKDGCFQSGFNQETCLTRITTGLVEFQLHLNILQNNYEGDKENVKSVHMSTKILVQMLKSKVKNQDEVTTPDPTTDASLQAILQSQDEWLKHTTIHLILQSLEDFLQFSLRAVRIM
PTSPNQTENLIKSIFLEISEVRNKMCGNDDSCKNSKEALTENNLNLPKMVEKDGCFQSGFNQETCLKKITTGLLEFQIYLDYLQNKFE---ENAKAMQMRTKALVQVLKQKVKNPDEITTPEPTTNSSLLAKLQSQSEWLQTTTIHLILRSLEDFLQFTQRSVRIM
LTTPDKTEALIKHIVDKISAMRKEICEKNDKCENSKETLAENNLNLPKMKEKDGCFQSGFNQETCLIRSTVGLLEYQTYLDYLQNEYEGDQENVKDLRSSIRTLLQIMRQKSIDL----VTTATTNPDLLEKMQSSNEWVKNAKIILILRSLENFLQFSLRA----
YTTSQQVRGLITYVLSEILETRKQLCNDNPDCMNKENALSENNLKLPEIQRDDGCFHTGYNRDVCLLKITSGLLEYQTYLEYVKNNLQNKKDKARAIQSNTKTLIRIFKQEVKDPGQIVF-DPTSEALLLEKLESQTEWL--------------------------
-----------------------QMCEKFTVCQNSMEMLLHNNLNLPKVTEEDGCLLKRTVEDKCLRKISSGLYTFQTYLKYIQETFISENQNVESLSYSTEHLARTIRQMVINPEEVIIPDAATQESLHTELKSTKDWTEKITIHLILRYFTSFMERTVRAVRY-
-----------------------QLCDKYAVCDNSMETFTPNSMNLPKMDDR--CLQSSFNKEKCFTKIASGLYAYHIHLLFVQDIYTNEGKVVKDLRISTEQLVDAVKLMVKDYYEVPELDADAQKVLLAKLYSESDWLQKVTARLILRDFTHFMAKTARAIRR-
------CEPLARVLRDRAVQLQDEMCKKFTVCENSMEMLVRNNLNLPKVTEEDGCLLAGFDEEKCLTKLSSGLFAFQTYLEFIQETFDSEKQNVESLCYSTKHLAATIRQMVINPDEVVIPDSAAQKSLLANLKSDKDWIEKITMHLILRDFTSFMEKTVRAVRYL
------CEALAWLLHARAARLQEEMCEKFTVCENSMEMLVQNNLNLPKVTEEDGCLLAGFDEEKCLKKLFSGLFTFQTYLEYVQETFTSEKQKVESLCYSTKHLATTIRQMVINPDEVVIPDSATQKSLLTKLKSDKTWIEKITTHLILRDFTSFMEKTVRAVRYL
------CKSLAKTLYKEADALKEENGENQSMCDNSSTILAENNLVFPNLTEQDGCFYSGFNKETCLINLISKLQEFDGYFQFMENELKEKKSRIEALKMFTTQLAESLKKLMMGADLVPTLNPTASHDLVLKLKSLNEWSRKVALHLSLCRYIKFMEHTIRAIRNM
------IVNHAKYLEKTASDLKEEICRIHNLCDNSNEALAENNLLLPNITERDGCLPSSFNEETCLIKIISGLQDFDIFLNYMETEMED--NRFQTLKLSTTQLANTLKTVIKKTDLVPTTNPTTSSILLSELQSLTAWSRKVGFRLILWHYTRFIQGTVRAVRYL
--------------------------------------------------EEDGCFPLAANHETCLLRITSGLLEFQMYLEHLQAKFRSEEENTRVILKNMRHLINTLRPKVKNFNEGATLKPAVVASLMENLQQKDQWLKTTTIHFILRGLTDFLQFTLRSVRLM
--------------------------------------------------EEDGCFPSALNHETCLLRITSGLLEFQMYLEHLQAKFRSDEENTRVMLKNIRYLIKTLRPKVKNLNEGATLKPAIVASLMKNLQQKDQWLKTTTIHFILRNLTDFLQFSLRAVGLM
---------------SKAAKLKDEMCEKFTVCDNSMEMLAQNDLNLPKITEEDGCLLSGFNEEKCLSGISSGLFTFQTYLEYVQETLITEKQKVESICYGTKHLANTVRQMVKNPGAVIMPDPATQNTLFAKLKSNKKWIEKITTHLILRDFTSFMEKTVRAVRY-
--------SLTKTLWKQANGLKDKICDDYSLCEENKVVLAENNMNLPKMTEEDGCFLSGFNEETCLKRIVTGLSEFQIYLKYVQKTFKGEEKILESIQNYTKHLSNILKQ--------------------------------------------------------
---------LAKLIEREAASLQIKLCDNHGVCDNSLEMLAENNLQLPKISTEDRCFSLGFQKDKCLNKIHRDLSIFKIYLVHVKETFISEKNTVESLQYKTQIMIQIMKR-----TETESDDKGTNIQVLQALKSENLWRQRVTNRLILQAFIECIQKTARAVRY-
-----------------------------------------------------------FNQETCLIRSTVGLLEYQAYLDYLQNEYEGDEENIKDLQSSVRTLLQIMRQKSIDL----VTTPTANLDLLEKM---------------------------------
--------------------------------------------------------------------------------------------------------------------ETSTPLPTSKALLMEKLESQKEWSRTKTIQLILKALEQFLKVTMRSTRQN
-----------------------------------------------------------IHEETCLLKITTGLLEFEVYLEYLQRE--------------------------------------------------------------------------------
-----------------------------------KEALAENNLHLPKL-GKRWMLQSGFNQETCLTR--------------------------------------------------------------------------------------------------
---------RIDWLLSVVAACYEDLCANTGICQGKLEPMAFFNADMPVVEQGSGCFPPSLNETRCLKDLAIGVWGLVEPFVFLNEHFGEYVEHVGAMHMLARAIAWDMRDHANRDLYEPFELPVPKPKMLPLLRRLTTWNKHIAAFNILRRIERFANDANRALSY-
---------RIGWLLSVVSACYEDLCTNTGICQGKLEPLAFFNADMPVVKQDEGCFPPSLNKTRCLQELAVGFWGLVDPFVFLNEHFGDYVEHVGAMELLVRAIAWDMRDESNRLSQRPFELPSSDSKMLPRLRGLTTWNKHMAAFKILQRIEKFANDANAALTY-
---------RLNWMLWVIDECFRDLCYRTGICKGILEPAAIFHLKLPAINDTDHCGLIGFNETSCLKKLADGFFEFEVLFKFLTTEFGKSVINVDVMELLTKTLGWDIQEELNKLTKTHYSPPKFDRGLLGRLQGLKYWVRHFASFYVLSAMEKFAGQAVRVLDS-
-----------------------------------------------------------IHEETCLLKITTGLLEFEVYLEYSDNR--------------------------------------------------------------------------------
--------------------IHSQMCDYNAKCLNRTAALAAPDLNLPKLTEKDKCSPTALNKEANLMKLGEGLSEFQKHLELLQNKS-----RANDVQINSQALTQILKQMCLNPDALITQPPKSDSW---RVHSQGQWLKNMMIPLSFAMLKKLLELHVRAVHLM
---------ALESVLGATKLHKNEFLVEFQG-EVKYDFLD--RYKIPSLPA--KCPYSNFGKDACLRRLLEGLLIYSVLLKRVEEEFPSSSI-LSEVRFYSNILIKELENKVRDRDQVMRLTSSQEEQLLKDTDYPDTFHRKMTAHGILYNLHYFLVDCRRVINKR
---------VWDLIIGVTAHHQKEFEDEFQQ-EVKYRFLN--HYKLSSLPA--DCPSANFSKEACLQRLAEGLHTYMVLFKHVEKEYPSSSI-LLHARYHSGALIGLIKEKMRNPGQVTVPTSRQEQQLLQDMDNPSTFHRKMTAHNILRQLHNFLRNGKVAIRKR
---------VWHAVLGVTKLHKKEFEDEFEN-VVKYNFLE--NYKVFSVPE--RCPHSNFSKEACLHRMAHGLLVYTVLLKHVEKEYPGSLI-CSLAKYYSGLLINLCKEKMRNPDQVAALTDRQEAQLLRGLNLPSAFQRKMAAHSVLRQLHYFLLDGRRAIAKR
---------ALESVLRATKRHKKEFLAEFQG-EVKYEFLD--RYKIPSLPA--KCPYSNFGQDACLRRLLEGLLVYSVLLKHVEQEYPLSRI--SEVRYYSNVLIKEVENKVKERGQVTTLSSSQQEQLLRAVDRSDTFHRRMTAHGILYNLHYFLVDCRRVINKK
---------IWGLIISVTARHQQEFEDEFQQ-EVKYHFLD--HYKISSLPA--NCPSANFSKEACLHRLGEGLHTYMVLFKHVEKEYPSSPI-LLYARYHGGALIGLIKEKMRNPGQVTVPTSSQEQQLLQDVDSPNTFQRKMTAHNILRHLHDFLRNGRVAIRKK
---------IWRSLFDSAQEYEKAFEHHFQTLENRDQALD--SHTPASIPK--HCNITKFRKDACLQTLAKGLLIYSVLLKHVEKEYRSSLN-FSDAPSNIGTLIGMVKGKMKNRNQVTPLTSSEEEQLLKEVNSPDPYHRKLHAYSILRALKAFLSEGKRAVCRM
---------LLKVVLEVIKTHRQEFEAEFH---IRYDVLA--QYNIPSLPA--DCPSTNFSMEALLHRLLQGLPVYTALLKYVEKEEPKSQI-PSRFRQNSELLKQRITGKMRHAVQVTPLTSSQEQQLLRDLDSSDTFHRKMTAHSILYQLRSFLVDCKNAINKK
---------TWERLIEATKQHQKEFEQEFQG-NVDYILLE--GHKRSAFPV--KCPMSNYSKEACLQRLAQGLLVYTVLLKQVEKEYPDNSI-LREIKPGIPLLVAQIKEKMKRSERVPAPSSSQEEQLLKQLDSPDTFQRRMTAHSILSHLRFFLIDGKRAFRK-
-----------------------------------------------------------------------------------------------------------FSFQINNTEQMEFLSPTPDATLLEKLETQSQTQMLLIAEIVLQRLEEFLQDSLRAIRK-
--------PVWGVFIASTTRHKEQFEDEFSI-EANYDFLE--RTEIPSPPA--GCPVT-MNKEACLLRLVQGLQKYKVLLTHVKKEYPDNAL-LPHIKYNSDLLIHLIEEKMRHPERLTVLSDTEAQSILQGLENTNTFQRKVTAHSILRKLHLFLIDSSRDLC--
---------ALESVLRATKRHKKEFLAEFQG-EVKYEFLD--RYKIPSLPA--KCPYSNFGQVGRVPALTTGLFGFFPSFGWISTRFSSSAGSLSEVRYYSNVLIKEVENKVKERGQVTTLSSSQQEQLLRAVDRSDTFHRRMTAHGILYNLHYFLVDCRRVI---
---PPKWEKMIKMLVHEVTTLRNQFVEEFQK--PVEEISSFSQHQVPSTPPHSKTLCSASNKEACLQEISRGLQVYQLLLQHVKAEYPQSTL-LPSVTHQTTVLIGLVKDQMKVAEVVEDLSASERKRVLGEVSTGTEWERKTSVHAILRELRNFLVDTKRALRRM
-----------------------------------------------------TCPE-KSSKEACLRCLAQGLLTYTALLKHVEKESPSSIRSEHSFKSLLLRLTSGIKNKMRHREHVKALTNSQEGHLLRDFDSPDPFQRLMTTFKILYKLRDFLIDGIKNI---
-----QIQEQVRKIQNDIEGLQQRLCADYRLCHPEELILVGLYLGIPRPSV-KNCYSQDLKLVACLNQLYYGLQFYQGLVRALEGISPELAPTLDTLKLDIGDFAASIWQQMEDFQVVPEIVPTQSIL--PTF--SSSFYRRAGGVLIFSQLQSFLDMAYRALRLL
-----KSLEQVRKIQARNTELLEQLCATYKLCHPEELVLFGHSLGIPKASL-SSCSSQALQQTKCLSQLHSGLFLYQGLLQALAGISSELAPTLDMLHLDVDNFATTIWQQMESLGVAPTVQPTQSTM--PIF--TSAFQRRAGGVLVTSYLQSFLETAHHALHHL
-----QSQEQVRKIQMDMEELQQRLCADYSLCHPEELILVGQYLGIPRPPL-KNCYSQDLRLAACLNQLHDGLQLYQELLKALQGISPELAPTLDTLQLDVADFTATIWLQMEDLQVTPKIMPILGTL--PSF--SSSFWRRAGGILIFDRLQAFLETAYRALRHL
-----KSLEQVRKIQASGSVLLEQLCATYKLCHPEELVLLGHSLGIPKASL-SGCSSQALQQTQCLSQLHSGLCLYQGLLQALAGISPALAPTLDLLQLDVANFATTIWQQMENLGVAPTVQPTQSAM--PAF--TSAFQRRAGGVLAISYLQGFLETARLALHHL
-----KSLEQVRKIQTRNSELLEQLCATYKLCHPEELVLLGHSLGIPQAPL-SRCSIQALQLTKCLSQLHSGLFLYQGLLQALTGISPELAPTVDMLQLDVANFATTIWQQMESLGVAPTVQPTQSTL--PTF--TSAFQRRAGGVLAASHLQSFLETAHCTLNHL
-----KCLEQVRKVQGDGTELQEKLCATYKMCHPEEMVLFRHSLGIPQVSL-SSCPSQALQLTGCLRQLHRGLFLYQGFLQTLEGISPELAPTLDMLRLDITDFATTIWQQMEELGTAPALQPTHGAT--PAL--PSAFQRQVGGLLVASHLQSFLELVHRVLRHL
-----KSSEQARRVQAEAMVMQEKLCATHRLCHPEELALLGHSLGIPWASL-RSCSSRDLQLLGCLRQLQAGLMLYQGLLQALAGISPALSSSVDLLRLDVADFATTVWQQMEDLGVAPAAQPTLGTV--PAF--TSAFQRQAGGVLVASHLQDFLELALRALRYL
-----KCLEQARRVQAEAVEMQQKLCATYRLCHPEELTLLGHSLSIPWVPL-KGCSSQDLQPTRCLRQLQAGLVLYRGLLRALVGISPELSPTVDTLQLDVADFATTIWQQMEDLGTAPAVQPTPGTM--LTF--TSAFQRQAVGAMVASQLQCFLELAYRALRYL
-----KCSEQVRKIQADTSMLQEELCATHKLCHPEELLLLGHSLGIPRAPL-SSCSSQALQLPGCLSQLHSGLLLYRGLLQALAGISPASAPALDKLQLDVADFATTIWHQMEDLGQAPAVQPTQSTM--PAL--TSAFQRRAGSVLVASHLQSFLELTYRVLRYL
-----KCLEQVRKIQADVVAMQERLCATHKLCHPEELVLLRHSLGIPQVPL-GSCSSQALQLTSCLGQLHGGLFLYQGLLQALAGISPELGPTLDMLQLDITDFATNIWQQMEDLGMAPGVQATQGTV--PTF--TSAFQRRAGGVLVASSLQSFLQLALRVLRHL
-----KCLEQMRKVQADGTALQERLCATYKLCHPEELVLLGHALGIPQPPL-SSCSLKSLWTTGCLRQLHGGLFLYQGLLQALAGVSPELAPALDTLQLDISDFAVNIWQQMEELGVAPAVPPTQGTM--PTF--TSAFQRRAGGVLVASDLQSFLELAYRALRSF
-----KCLEQVRKIQGDGAALQEKLCATYKLCHPEELVLLRHSLGIPWAPL-SSCPSQALQLTGCLSQLHSSLFLYQGLLQALEGISPELSPTLDTLQLDIADFATTIWQQMEDLGMAPALQPTQGAM--PAF--TSAFQRRAGGVLVASHLQRFLELAYRVLRHL
-----KCLEQMRKVQADGTALQETLCATHQLCHPEELVLLGHALGIPQPPL-SSCSSQALQLMGCLRQLHSGLFLYQGLLQALAGISPELAPTLDTLQLDTTDFAINIWQQMEDLGMAPAVPPTQGTM--PAF--TSAFQRRAGGVLVASNLQSFLELAYRALRHF
-----KCLEQVRKIQADGAALQDRLCATHKLCHPQELMLLGHSLGIPQPLL-SSCSSQALQLTGCLSQLHSGLLLYQGLLQALAGISPELAPTLDMLQLDVTDFATNIWQQMEDLGVAPVVQPTHGPM--PTF--ASAFQRRAGGVLVASNLQRFLELAYRGLRYL
-----QCLEQVRKIRADGAVLQERLCATYKLCHPEELVLLGHSLGITQAPL-SSCSSQALEVVDCLHQLHSGLVLYQGLLQALAGTSPEVASTLDLLHLDVADFAINIWQQMEDMGVAPTGKPTQGPM--PTF--TSAFQRRAGGVLVASNLQSFLEQAYRVLRYL
-----KNLEFTRKIRGDVAALQRAVCDTFQLCTEEELQLVQPDPHLVQAPL-DQCHKRGFQAEVCFTQIRAGLHAYHDSLGAVLRLLPNHTTLVETLQLDAANLSSNIQQQMEDLGLDTVTLPQRSPP--PTF--SGPFQQQVGGFFILANFQRFLETAYRALRHL
-----KDLEYIRKIKGDVARLQELMCSTFQLCSEDELLLVKQKLGITQAPL-DQCHSKTFQVSGCPRSVRASTEAW-GALGLREATCPGQGLGAPGVS-GPGSLPSSAPLQMEDLGLNTVTYPGQGPL--PSF--SSDFEKQASGFIILANFQRFLEMALRALRHL
-----KNKEFVARMKSEISDLKESMRTDFSLGTDDQFLFMQDFLGIEQADH-SHCQKATCDLGNCFKQLRAGLHTYYGYLSHIKQILPNYTNRVSSLQLDTSNLSTNIQLQFEESSLPVVTYPQAENQ--PNF--LQ--QREIGSYLVLRKFMLFMDVITRALNHC
-----KCLEQVRKIQAQGSVLQEKLCATYQLCHPEELALLGHSLGIPQAPL-SNCSSQGLQLTGCLSQLQSGLFLYQGLLQALAGISPELSPTVDMLQLDVANFATTVWQQMEDLGVAPAVQPTQGTM--PTF--TSAFQRRAVGVLVASRLQRLLELVYRVLRHL
-----KCLEQVRKIQADGVVLQERLCATHNLCHPEELVLLGHSLGISQAPL-SSCSSQALQLTGCLRQLHSGLFLYQGFLQALAGISPKLAPTLDMLHLDIADLATNIWQQMEDLGVAPAVQATQGTM--PIF--TSAFQRRAGGVLVAANLQSFLELAYRVLRYL
-----KCLEQVRKIQNEGAALQENLCATYKLCHPEELVLLGHSLGIPRTTL-NGCPSQDLQLTSCLSQLHRGLFLYQGLLQALAGISPELAPTLDMLQLDVADLATTIWQQMEDLGVAPALQPTQGAM--PTF--TSAFQRRAGGILVTSHLQSFLELAYRVLRYL
-----KCLEQVRKIQADGAELQERLCATHKLCHPQELVLLGHSLGLPQASL-SSCSSQALQLTGCLNQLHGGLVLYQGLLQALAGISPELAPALDILQLDVTDLATNIWLQMEDLRMAPASLPTQGTV--PTF--TSAFQRRAGGVLVVSQLQSFLELAYRVLRYL
-----KCLEQVRKIQADGAELQERLCAAHKLCHPEELMLLRHSLGIPQAPL-SSCSSQSLQLTSCLNQLHGGLFLYQGLLQALAGISPELAPTLDTLQLDVTDFATNIWLQMEDLGAAPAVQPTQGAM--PTF--TSAFQRRAGGVLVASQLHRFLELAYRGLRYL
-----KGLEQVKKVQGDGTELQEKLCATYKMCHPEKMVLFGHSV-IPQVSV-SRCPSQALQLTGCLRQLH-SLFLYQGLLQTLEGISPELAPALDMLCLHIADFATTIWQQMEELGTAPVLQPTRGAT--PAL--PSAFQRRVGGLLVASHLQSILELVHRVLRHL
-----KNLEFTRKIRGDVAVLQRVVCDTFQLCTEEELQLVQPDPPLLQAPL-DQCHKRGFQAEMCFGQIRAGLHAYQDSLGAVLQLLPEHTALVETLQLDAANLSTNIQQQMEDLGLDTVTLPHRSPP--PTF--STDFQQQVGG---------------------
-----------RDLYKDVKTLRDEFERDFRE-MVNMTAFEGVRISTPLLKPSDRCLSKNFSTERCLTRIYSVLTWYKDNWNYIEKENLTSVL-VNDIKHSTKRLLEAINSQLQVRDGEMDQTSSTS------LSFKSAWTRKTTVHSILFNFSSVMIDACRAINYM
---------FSRKITRDAEKLKNLMCDNHGLCEKSELTLIQEHLQLPDVPLG-QCQAGSFSQEGCFSQLSNGLQELQRRVVAMPGHLPEA--DLQRLEKDISDLLVNIQEEMEAQGITAQSSPSQA---LP--TYSKGFHKKVAMFLILSDLAS------------
-------------------------CDTLQLCKEDELLLVRQELDIAQAPLE-QCHRRTFQAETCFSQIRAGLRIYGGSLATIQALLPGHAGLVETLQLDMANLSSNIQQQMEDLGLATVTYP-QDPV--PT--FSSHFHHQVGGFFILANFQRFLETAYRALRHL
-------------------------CTKHSLCHPEELELMRHYLGVRQASVN-RCPSRDLELGPCLKQLARGLKLYQAQLEALEGISPQLAPALDTLQLDVRDFAINIWQQLEDLRLTATPLSPQASV--PT--FTSAFQRRAGAVLVLDNLQGFLEVVARVLNQL
---------LAEKIHPEIKNLQDLICKQHHICNEDELTLKKEYLRIPRLEE---CRSNNLQLDTCMNKMEAGLQVYGDYLNLIKKVIVGSSL-AETVQADIMDLQQHIRQKVIDVAILVLTLPNFAA----------SFDEQAGGHIVLSHFKRFMETVLRVLQYM
-------------------------------------------SDVPHIQCGDGCDPQGLNSQFCLQRIHQGLVFYKQLLD--SDIFTGEDGPVGQLHTSLLGLSQLLQPEDHQWESQQMPSLSPS----------QQWQRPLLRSKILRSLQAFVAIAARVF---
-------------------------------------------NNVPRIQCEDGCDPQGLNSQFCLQRIRQGLAFYKHLLD--SDIFKGEDSPMEQLHTSLLGLSQLLQPEDHPRETQQMPSLSSS----------QQWQRPLLRSKILRSLQAFLAIAARVF---
-------------------------------------------DDVPHIQCGDGCDHQSLNSQLCLQQIHRGLIFYGHLLN--SDIFKGEDGPVGQLHSSLLGLSQLLQPKDHSWKKQQVP---PS----------EPWQRLLLRPKILQRLRAFAAIAARVF---
-------------------------------------------DDVPRIQCEDGCDPQGLNSQSCLQRIHRGLVFYEKLLG--SDIFTGENGPVDQLHASILGLRELLQPKGHHWETEQTPSPIPS----------QPWQRLLLRLKILRSLQAFVAVAARVF---
-------------------------------------------DDVPHIQCEDGCDPQGLNTQFCLQRIRQGLFFYEKLLG--SDIFTGEDGPVGQLHASLLGLSQLLQPEGHHRETPQTPSLHPS----------QPWQRRLLRFKILRSLQAFLAVAARVF---
-------------------------------------------DEVPRIQCGDGCDPQGLNSQFCLQRIHQGLVFYEKLLG--SDIFTGEDGPVGQLHASLLGLRQLLQPEGHHRETEQTPSPSPS----------QPWQRLLLRFKILRSLQAFVAVAARVF---
-------------------------------------------NDVPHIQCEDGCDPEGLNSQPCLQRIHQGLVFYEKLLG--SDIFTGENGPVDQLHASLLGLRQLLQPEGHHWETEQIPSPSPS----------QPWQRLLLRPKILRSLQAFVAVAARVF---
-------------------------------------------SEVPRIQCWDGCDPQGLNSQFCLQRIHQGLIFYEKLLG--SDIFRGEDGPVGQLHASLLGLSQLLQPEGHHSETQPTPTPSPS----------LPWQRLLLRLKILRSLQAFVAVAARVF---
-------------------------------------------NDVPHIQCEDGCDPQGLNSQFCLQRIHQGLLFYEKLLG--SDIFTGEDGPVGQLHASLLGLSQLLQPEGHHWEPQHTASPIPS----------QPWQRLLLRFKILRSLQAFVAVAARVF---
-------------------------------------------DDVPHIQCGDGCDPQGLNSQFCLQRIYQGLIFYQKLLG--SDIFTGEDGPVGQLHASLLGLSQLLQPEDHQQETQ-TPSLSPS----------QPWQRLLLRIKILRSLQAFVAVAARVF---
-------------------------------------------DYVPHILCGDGCDPQGLNSQFCLQRIYQGLVFYQNLLG--SDIFTGEDGPVSQLHASLLGLSQLLQPEVHQWEPQ-IPSLSPN----------QPWQRLLLRIKILRSFQAFVAVAARVF---
-------------------------------------------NDVPHIQCGDGCDPQGLNSQFCLQRIHQGLIFYEKLLG--SDIFTGDDAPVGQLHASLLGLSQLLQPEGHHLETQQTPNPSPS----------QPWQRLLLRFKILRSLQAFIAVAARVF---
------------------------------------------TIGIRPAPVL-SVVSENYSLETSLRHMSEGFQLHRDLLSAVSPRLAN-KDNVTGLVADIRDLVLRIN-EMLKMAQTDAVVQPSPTPVALNLPG--NYEVQVAAHMILVQLQAFGQDTARCLRS-
------------------------------------------SLGIPATPIL-KPLSERFTMDMCVSRMSVGCLLYQGLLGVLADRL----SGLTNLRADLRDLLTHIN-KMKEAAQFGAESPDQNQDLASRLHG--NYEVQVAVHVTLTQLRSFCHDLIRSLRA-
------------------------------------------SLGIPPAPVL-KPLSEHFTLDVCVSRMSSGILLYQRLLGVLSDRV----SGLSDLQADLRDLQTHVA-TVREVAQLRDPELDENQDLASRLHG--NYKVQIATHLTLTHLRSFCHDLIRSLRG-
------------------------------------------TLGIPDTPVL-KELSEQFTADMCVSRMLAGGRLYQGLLVDLSGRL----GGLEALSAELRDLVTHIN-QMKDAAQLSSDSSDQSSDLTLNLHG--NYDVQVAAHLTLCQLLEFCHDLIRSLRN-
------------------------------------------TIGIPPAPVL-KALSENFTMGTCLRRISEGLQLHRTLLAVIADHLKN-KDRVLALQADIRDLNIQIN-KMLKMVGEETVVPP---AVTLNLPA--DYEVQVAAHLTLQQLQTFGRDVDRHLKS-
------------------------------------------SLGIPAAPEL-KTVCRDFTLEVCLSSMSAGLQLYQDVLGVLKERVT--TEKVTGLLADIRDLLAQVN-KMQEPGQMSSVAQYEASGLASRLPG--DYEVQVATHFILLQLRDFTQNLKRSLRN-
------------------------------------------SLGLPVAPLL-KLPSDHFTLDMCVSRMLVGCQMFQRLLAVLSEKL----DGLMDLKVTLRDLVTHIT-KMKETLGLDVDGSEALTDVASRLHG--DYEAQMAAHLALVQLRSFCHDLTRSLRA-
------------------------------------------SLGLPVAPLL-KPPSEQFTLDICVSRMLVGCQMFQKLLGVLSERV----DGLMDLKVTLRDLVTHIT-KMTETVRLNGDTPEAPSDAASRLPG--NYEAQMAAHLTLIQLRSFCHDLTRSLRA-
------------------------------------------TIGIPSAPAL-SALSDNFTLNTMLRHMLEGLQLHKDLLNHVLPRLEV-KDMVIDLTHDLNDLSVQVL-KMLKLAQREGVSKPNPTGLALDLRG--SYEVQVAIHLILVQLQGFEQDMDRCLRS-
------------------------------------------SLRIPAAPVL-K-LPHEDTLDVCLSRMLAGVQLYQGLLGDLSGRL----SGLSDLKADLRDLLNHIT-EIKKAAQLGGEAVQNQSDLASRLQG--DYEVQVAANATLKQLLSFCHDLMRSLKK-
------------------------------------------GIGIPSAPHI-KAPSENVTLEDSLRQLYEGLQLYRALLGSLGNRLAN-KDKVTGLMADVKDLAIQIK-EMLKIVRPQGSPHPTKPSVSLNLPA--EYEFQVAAHLTLVDLQSYIQDLVRFLRT-
------------------------------------LHLMSMSLQIPGPPVL-KPFSDSFTLDTCVSRMLAGVQLHQNLLAVLSGRLAG----LEELKADVRDLLAHIM-KLKEAALMEGSALNRGSDLDARL--TDSYTVQVAAHTVLTQLRSFCHDLTRSFRAL
-----------------------------------------SIIGIPAAPVL-KALSPNVTLETSLALVSKGLQLYEDLLGIIVNHLEQKKE-LSDLKAHISDLKKLITRMLKVAGGQAEDLPKPT----LNL--PGDYEVQVAAHLTLLQLQSFGQDVGRCLES-
------AHSLQSGLLSVLSCLNKSNLFSQSLAGSSTDKLQHLKHYIPSAPVL-QNITDISSLETCLDKIVRGLQLHLNLLKDLIEATLSQTDQVTELQADIQELVLLIE-ELQNQSQQTSEEQSQSFNLTQHL--KSDFQTEAAAHLILHQLRDFSCDIL------
------AHSLISKTLEDIPATHAAWVKSKSLAGSSTDKLQHLKHYIPSAPVL-QNITDISSLETCLDKIVRGLQLHLNLLKDLIEATLSQTDQVTELQADIQELVLLIE-ELQNQSQQTSEEQSQSFNLTQHL--KSDFQTEAAAHLILHQLRDFSCDIL------
------ALSLAKKILNDIPAVQ-ELCAPNTGLSSSTEEFLSSEFQIPTLPLL---KSEDLTLEERVQRMLIGLELHRVLLKLLEPCRPE------NLLNDLAELRTLLLLQV----------PSAGPQVLPHTHTLSQFKLQVRVRATLRQLRSFMQDVFRSLRC-
--------------------------------------------------------------ESSLILAHEGLQQHQALLSSISPHLENQ-QRATDLMNTVRDLAVQIS-KMLQGLQTDYVLQTTPSPVALRLHG--DFEVQVAAHLTLVQLQSLGQDVHRFL---
-------------------------------------------HRFKSLPAISSR-ASDFEVKPTLSQLHANLKSFQHHFEWLNNITHKSIPKLTDMVSHIGGLVNSLQRQMNHIGAPRLPVPSPS--LPPIP--AFHWEMVQTSQELLEQFSLFCDWAARVLGR-
-------------------------------------------HRFKSLPLMNNR-ASDIEMRPTLSQLHADLKSFEHHFAWLSRASRKALPKLGQMMSLIKSLTSMLEHQMMRVDAQRLSPPSPS--MPPPP--PSQFDVLQSSQELLLQFRLFCDWAQRVFLS-
-------------------------------------------NKLPDLPHMQHSAAHFFKMNESLSELYLLAQAFRLHVDWLKTE---S----EDASTHL---SNLLNMSLHQ-SA--PQPPAPS--LP--V--SSAFDLLQFSIEISERLKVFCNWSKRVLRS-
-------------------------------------------HKFPSLPEMSNRSANDLELKPTLSQLHAELKLYEHHFEWLNRVSKKALPKLVEMIKELKSLISLLHHQMLRVEAPRLNLTTPS--LPPQL--PYQFDVLQSSHELLQHFKLFCDWAYRAFIS-
-------------------------------------------HRFRSLPEMSNRLATDLELKPTLSQLHADLKLYKDHFDWLSNVSKKAVPKLEKMITEMKSVITMLHHQMQRVEAPVLTPATPS--LPSHL--PYHFEVLQSTHELLQHFNLFCDWAIRAFIG-
-------------------------------------------HRFRSLPEMSNRSANDLELKPTLSQLHADLKLYEHHFEWLNKVSKKALPKLVEMIREMKSLINLLHRQMLRVEAPKLTQATPS--LPPHL--PYQFDVLQSSHELLQHFKLFCDWAYRAFIS-
-------------------------------------------QRFKSLPAISSR-VSDLEFKPTLSQLYADLKSFEHHFEWLNRTTRKSVPKLTDMISHIKSLINSLQRQMTRAEAPRIPVPSPS--LPPNP--AFHWEVVQSSQELLQQFRLFCDWASRVFLT-
-----------------------YLCHHSTLCRVREYPRIMSFVHFPILMSNVECQRREFRGAECMNAMVRGLRAYESYLTRLRMDAPGDADAATVVLSALDSLIEELP----VNNKIGGAE--SNEKTVRALGGQSPRDVVLSAFRILEYLQMFLRDGRRAIAMM
------------------------------------------EDPTPQIQCSDRCDPDGLDGELCLRRLQEALTFYGHLLG--SDLFSGQPGPVGQLRSDLLDLGRLLQQPL------------------------------------------------------
