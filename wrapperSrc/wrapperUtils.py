#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import time, re
verbosity = 0

def readTrainFile(filename):
	training = {}
	totSeqs = 0
	start = time.time()

	ifp = open(filename, "r")
	if verbosity >= 1:
		print "Reading sequences from %s..." % (filename)			
	i = 0
	line = ifp.readline()
	while len(line) != 0:
		if line[0] == '>':
			#print line
			match = re.search(r"^>(.{4,4}).(\w).(\w{4,6})\n", line)
			pdbCode = line[match.start(1):match.end(1)]
			chain = line[match.start(2):match.end(2)]
			uniProt =  line[match.start(3):match.end(3)]

			if verbosity >= 4:
				print line
				print "pdb: " + pdbCode
				print "chain: " + chain
				print "uniProt: " + uniProt
			line = ifp.readline()
			seq = ""
			while line[0:4] != "FREE" and line[0:4] != "SSBO":
				seq += line
				line = ifp.readline()
			if verbosity >= 4:
				print "Seq: " + seq				
			metaData = []
			while line != "\n":
				metaData.append(parseMetaData(line))
				line = ifp.readline()
			if verbosity >= 4:
				print "Metadata: " + str(metaData)
			#cleansingXs part
			if training.has_key(uniProt):
				print "WARNING: protein: " + uniProt + " is already present in the database!"
			training[uniProt] = [pdbCode + ":" + chain, seq.replace("\n",""), metaData]
			totSeqs += 1
			line = ifp.readline()
			#continue
	fine = time.time()
	
	print " Done: read %d seqs in %.3fs" % (len(training), (fine-start))
	return training # UID:(pdb, seq, [(free, n), (bond, n, n)])

def parseMetaData(mds):
	match = re.search(r"^(\w{4,6}) .*", mds)
	if mds[match.start(1):match.end(1)] == "FREE":		
		match = re.search(r"^(\w{4,6}).*\s+(\d+)\s+.*", mds)	
		metaData = [mds[match.start(1):match.end(1)],
					int(mds[match.start(2):match.end(2)])]
	elif mds[match.start(1):match.end(1)] == "SSBOND":
		match = re.search(r"^(\w{4,6}).*\s+(\d+)\s+.*\s+(\d+)\n", mds)	
		metaData = [mds[match.start(1):match.end(1)],
					int(mds[match.start(2):match.end(2)]),
					int(mds[match.start(3):match.end(3)])]
	else:
		raise Exception("Incorrect parsing!")
	#print "TUPLA:" + str(metaData)
	return metaData

def extractCouplingFromMeta(meta):
	s = []
	for m in meta:
		if m[0] == "SSBOND":
			s.append((m[1],m[2]))
	return sorted(s) #avoid dependence on input format
	
def getBonded(meta):
	cys = []
	for m in meta:
		if m[0] == "SSBOND":
			cys.append(m[1])
			cys.append(m[2])
	return sorted(cys)#avoid dependence on input format



def main():
	
	return 0

if __name__ == '__main__':
	main()

