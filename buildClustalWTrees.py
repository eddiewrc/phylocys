#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  buildClustalWTrees.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from wrapperSrc.wrapperUtils import *
from sources.phyloCysUtils import *
import sys, os, random
verbosity = 1
CLUSTALW_PATH="clustalBins/clustalw2"



def getNumCys(seq, targetCys):
	c = 0
	for i in targetCys:
		if seq[i] == "C":
			c += 1
	return c
	
def filterMSA(msa, targetCys):
	pmsa = {}
	dmsa = {}
	msa1 = {}	
	count = 0
	for seq in msa:		
		if getNumCys(seq, targetCys) % 2 == 0:
			pmsa[count] = seq
		else:
			dmsa[count] = seq
		msa1[count] = seq
		count += 1
	# old fakeroot (farthest leaf)
	'''
	if len(pmsa) > 1:		
		s = list(pmsa[0])
		#assert "-" not in s
		random.shuffle(s)
		pmsa[count+1] = "".join(s)	
	'''
	return pmsa, dmsa, msa1

def writeFasta(msa, fileName, targetCys):
	random.seed()
	ofp = open(fileName,"w")	
	for seq in msa.items():
		ofp.write(">seq|"+str(seq[0])+"|"+getClusterNameString(seq[1], targetCys)+"\n")
		ofp.write(seq[1]+"\n")	
	if len(msa) > 1:	
		#build fakeroot
		s = list(msa[0])
		#assert "-" not in s
		for i in xrange(50): #to be shure shuffling correctly occurs
			random.shuffle(s)
		s1 = "".join(s)	
		ofp.write(">FAKEROOT\n")
		ofp.write(s1+"\n")
	
	ofp.close()	

def main():
	if len(sys.argv) != 5 or "-h" in sys.argv[1]:
		print "\nUSAGE: python buildClustalWTrees.py DATASET algnmentFolderPath alignment_suffixes output_trees_folder trees/SSseq3iterE-2\n"
		print "EXAMPLE: python buildClustalWTrees.py databases/PDBCYS alignmentCys/SSsequenze3iterE-2AlignDir _iter3_eval0.01_.hh trees/SSseq3iterE-2\n"
		exit(0)
	dataBase = readTrainFile(sys.argv[1])
	# UID:(pdb, seq, [(free, n), (bond, n, n)])
	numProts = 0
	TARGET_MSA_DIR=sys.argv[2]
	TARGET_MSA_NAME=sys.argv[3]
	TREE_FOLDER=sys.argv[4]
	predictions = {}
	os.system("mkdir "+TREE_FOLDER)
	count = 0
	for target in dataBase.items(): #iterates among all the proteins with N bonds
		targetCys = getBonded(target[1][2])				
		numProts += 1
		assert (len(targetCys) % 2) == 0 #just in case
		if len(targetCys) <= 10 and len(targetCys) > 2:
			count += 1		
			if verbosity >= 1:
				print "\nBuild tree for protein %s (%d/%d)" % (target[0], numProts, len(dataBase))
			print targetCys
			sys.stdout.flush()
			align=readMSA(TARGET_MSA_DIR+"/"+target[0]+TARGET_MSA_NAME)
			if len(align) == 0:
				print "ERROR: EMPTY ALIGNMENT HERE!"
			assert not len(align[0]) < max(targetCys)				
			pmsa, dmsa, msa = filterMSA(align, targetCys)				
			#assert len(msa) == len(dmsa) + len(pmsa)
			writeFasta(pmsa, TREE_FOLDER+"/"+ target[0]+"_even_", targetCys)
			#writeFasta(dmsa, TREE_FOLDER+"/"+ target[0]+"_odd_", targetCys)
			#writeFasta(msa, TREE_FOLDER+"/"+ target[0]+"_all_", targetCys)
			os.system(CLUSTALW_PATH +" "+TREE_FOLDER+"/"+ target[0]+"_even_" +  " -tree -clustering=nj > /dev/null")
			#os.system(CLUSTALW_PATH +" "+TREE_FOLDER+"/"+ target[0]+"_odd_" +  " -tree -clustering=nj > /dev/null")
			#os.system(CLUSTALW_PATH +" "+TREE_FOLDER+"/"+ target[0]+"_all_" +  " -tree -clustering=nj > /dev/null")
			
			#raw_input()
	#os.system("rm "+TREE_FOLDER+"/*_all_ " +TREE_FOLDER+"/*_pari_ "+TREE_FOLDER+"/*_disp_")
	print "Tot num of prots processed: %d" % count
	return 0

if __name__ == '__main__':
	main()

