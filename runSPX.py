#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  sephWrapper.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from wrapperSrc.wrapperUtils import *
from phyloCys import *
import sys, marshal, os
verbosity = 0

"""
This script iteratively runs and calculates predictions of phyloCys over SPX dataset.
It considers only SPX_IND sequences by removing all the sequences in common between SPX and PDBCYS at row 74
and SPX_INDnoFrag by removing all the sequences shorter than 40aas at row 72

USAGE: python phWrapper.py database_path numBondsChosen TREES_dir_path [DUMP]
	database_path = path to the dataset containing cysteine annotations in the PDBCYS-like format
	numBondsChosen = numbers of bonds for which the predictions are needed, separed by ':'s . e.g.: 2:3:4:5
	TREES_dir_path = directory containing the NEWICK-formatted trees built by buildClustalWTrees.py
	[DUMP] = optional; write 'dump' to store the serialized predictions in python marshal form.
"""

def main():
	if (len(sys.argv) != 4 and len(sys.argv) != 5 )or "-h" in sys.argv[1]:
		print "\nUSAGE: python phWrapper.py database_path numBondsChosen TREES_dir_path [DUMP]"
		print "\nEXAMPLE: python phWrapperTOTAL.py databases/PDBCYS 2:3:4:5 trees/FAKEROOT_SS3iterE-2\n"
		exit(0)
	dataBase = readTrainFile(sys.argv[1])
	bonds =sys.argv[2]
	bondList = []
	DUMP = False
	if len(sys.argv) == 5 and sys.argv[4].lower() == "dump":
		DUMP = True
		os.system("mkdir -p PREDICTIONS")
	for i in bonds.split(":"):
		bondList.append(int(i))
	print "Calculating %s bonds!" % (bondList)
	TARGET_TREE_DIR=sys.argv[3]
	#TARGET_TREE_NAME=sys.argv[4]
		
	for numBonds in bondList:
		predictions = {}
		#########PREDICTION PERFORMANCES EVALUATION PARAMETERS
		Qprot = 0
		numProts = 0
		Rb = 0
		edgeTot = 0
		############
		
		for target in dataBase.items(): #iterates among all the proteins with N bonds
			targetCys = getBonded(target[1][2])		
			assert (len(targetCys) % 2) == 0 #just in case
						
			if len(target[1][1]) < 40 : # remove short sequences, obtaining SPX NOFRAG
				continue
			if  target[0].upper() in ['1K3IA', '1HX1B', '1ETEA', '1MWPA', '1RHFA', '1VCAA', '1PGUA', '1JB9A', '1KZYC', '1UX6A', '1GMLA', '1FJRA', '1NSTA', '1IKOP', '1QNXA', \
			 '1LN1A', '1EXTA', '1H4PA', '1JQPA', '1M4UA', '1GP0A', '1R1ZA', '1N1FA', '2AAAA', '1AX8A', '1QUPA', '1EVSA', '1M8NA', '1DYKA', '1J8EA', '1KL9A', '1HM6A', '1GXYA', \
			 '1I1RA', '2SASA', '1IARB', '1OLZA', '1S4NA', '1BY2A', '1LR5A', '1JERA', '1VMOA', '1RMGA', '1FUSA', '1OLRA', '1HE7A', '1OOHA', '1OHTA', '2GMFA', '1KZQA', '1GL4A',\
			  '1HYPA', '1SKZA', '1POCA', '1IVYA', '1CFBA', '1DQGA', '1NPEB', '1JQGA', '1QZ1A', '1QWTA', '1L9LA', '1MN1A', '1EWFA', '1EQFA', '1I8NA', '1QFTA', '1HNFA', '1G8QA', \
			  '1H30A']: # ignore proteins in common between SPX and PDBCYS
				continue
			
			if len(targetCys) == numBonds*2 :
				numProts += 1
				assert not predictions.has_key(target[0])
				if verbosity >= 1:
					print "\nPredicting protein %s" % target[0]
				#predictedEdges = sephiroot2bootstrap(TARGET_TREE_DIR+"/"+target[0]+"_pari_.ph", targetCys, 30) #calculate predictions
				predictedEdges = phyloCys(TARGET_TREE_DIR+"/"+target[0]+"_even_.ph", targetCys)
				predictions[target[0]] = predictedEdges
				groundTruth = extractCouplingFromMeta(target[1][2])
				########## assessing results			
				if verbosity >= 1:
					print "Prediction: ", predictedEdges
					print "real      : ", groundTruth
				if predictedEdges == groundTruth:
					if verbosity > 0:
						print "CORRECT\n"
					Qprot += 1
				else:
					if verbosity > 0:
						print "WRONG!!!\n"
				for i in predictedEdges:
					if i in groundTruth:
						Rb += 1
				edgeTot += len(groundTruth)	
				###############
		if DUMP:	
			print "Dumping marshalled predictions in in " + "PREDICTIONS/PhyloCys-"+str(numBonds)+"_"+TARGET_TREE_DIR.replace("/","_")+".m ...",
			marshal.dump(predictions, open("PREDICTIONS/PhyloCys-"+str(numBonds)+"_"+TARGET_TREE_DIR.replace("/","_")+".m","wb"))
			print "Done."
		print "*******************SCORES**********************"
		print "Num of proteins with %d bonds: %d" % (numBonds, numProts)
		print "Qp = %3.3f" % (Qprot/float(numProts))
		print "Rb = %3.3f" % (Rb/float(edgeTot))
		print "***********************************************"	
	
	return 0




if __name__ == '__main__':
	main()

